# Value plus

## Installation

To use it, just clone this repo and install the npm dependencies:

```shell
$ git clone git@gitlab.com:itunu/valueplusdashboardadmin.git folderName
$ cd folderName
$ yarn install
```

## Scripts

* `yarn build` - Generate a minified build.
* `yarn start:dev` - Start development server, try it by opening `http://localhost:3000/`.
* `yarn test` - Run tests
* `yarn eslint` - Test coding style.
* `yarn bump x.x.x` - Update package version.
* `yarn deploy -- -r {version}` - Make zip file from build folder. Make sure you called `yarn build` before that!

See what each script does by looking at the `scripts` section in [package.json](./package.json).
