module.exports = {
  presets: ['@babel/preset-env', '@babel/preset-react'],
  plugins: [
    'transform-imports',
    'lodash',
    'date-fns',
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-transform-runtime',
    [
      'module-resolver',
      {
        root: ['./'],
        alias: {
          src: './src',
          jest: './tasks/jest',
        },
      },
    ],
  ],
};
