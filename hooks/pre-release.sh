#!/usr/bin/env bash

set -e

VERSION=`ps x | grep 'git-release' | head -1 | grep -o -E '[0-9]+\.[0-9]+\.[0-9]+'`
node ./tasks/bump.js ${VERSION}

set +e

git commit package.json -m "Bump Version to $VERSION"
git push
