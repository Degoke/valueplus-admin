import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { Auth, AuthProps } from './Auth';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: jest.fn(),
}));

describe('<Auth />', () => {
  let defaultProps: AuthProps;
  let component: () => ShallowWrapper<AuthProps>;
  let useEffect: any;

  const mockUseEffect = () => {
    useEffect.mockImplementationOnce((f: any) => f());
  };
  const curriedComponent = (props: AuthProps) => () =>
    shallow(<Auth {...props} />);

  beforeEach(() => {
    useEffect = jest.spyOn(React, 'useEffect');
    mockUseEffect();

    defaultProps = {
      isAuthenticated: false,
      checkUserSession: jest.fn(),
    };
    component = curriedComponent(defaultProps);
  });

  it('renders without crashing', () => {
    const wrapper = component();
    expect(wrapper.exists()).toEqual(true);
    expect(wrapper).toMatchSnapshot();
  });

  it('validates userSession by dispatch action', () => {
    expect(defaultProps.checkUserSession).toHaveBeenCalled();
  });
});
