import 'antd/dist/antd.css';
import 'styles/reset.global.scss';
import 'styles/app.global.scss';
import * as React from 'react';
import { ConnectedRouter } from 'connected-react-router';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { RootState } from 'src/reducers';
import { Routes } from 'src/routes';
import Dashboard from 'src/pages/Dashboard';
import Login from 'src/pages/Login';
import Logout from 'src/pages/Logout';
import ForgotPassword from 'src/pages/ForgotPassword';
import { RouteProtected } from './RouteProtected';
import { Redirect, Route, Switch } from 'react-router-dom';
import { checkUserSession } from 'src/actions/auth';
import ErrorPage from 'src/components/ErrorPage';
import ResetPassword from 'src/pages/ResetPassword';
import { history } from '../store';

interface OwnProps {}

interface StateProps {
  isAuthenticated?: boolean;
}

interface ActionProps {
  checkUserSession: () => void;
}

export type AuthProps = OwnProps & StateProps & ActionProps;

export const Auth: React.FunctionComponent<AuthProps> = ({
  isAuthenticated,
  checkUserSession,
}) => {
  React.useEffect(() => {
    checkUserSession();
  }, [checkUserSession]);

  return (
    <React.Fragment>
      <ConnectedRouter history={history}>
        <Switch>
          <Route
            exact
            path={Routes.HOME}
            render={() =>
              isAuthenticated ? <Redirect to={Routes.DASHBOARD} /> : <Login />
            }
          />
          <Route exact path={Routes.LOGOUT} component={Logout} />
          <Route
            exact
            path={Routes.FORGOT_PASSWORD}
            component={ForgotPassword}
          />
          <Route exact path={Routes.RESET_PASSWORD} component={ResetPassword} />

          <RouteProtected
            isAuthenticated={isAuthenticated}
            path={Routes.DASHBOARD}
            component={Dashboard}
          />
          <Route component={ErrorPage} />
        </Switch>
      </ConnectedRouter>
    </React.Fragment>
  );
};

const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = ({
  auth,
}) => ({
  isAuthenticated: auth.isAuthenticated,
});

const mapDispatchToProps: MapDispatchToProps<ActionProps, OwnProps> = (
  dispatch,
) => {
  return {
    checkUserSession: () => dispatch(checkUserSession()),
  };
};

const connected = connect(mapStateToProps, mapDispatchToProps)(Auth);

export default connected;
