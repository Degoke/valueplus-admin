import * as React from 'react';
import { Redirect, Route, RouteProps } from 'react-router';
import { Routes } from 'src/routes';

interface ProtectedRouteProps extends RouteProps {
  isAuthenticated: boolean;
  path: Routes;
}
export const RouteProtected: React.FunctionComponent<ProtectedRouteProps> = ({
  component: Component,
  isAuthenticated,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={(props) =>
        isAuthenticated ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: Routes.HOME }} />
        )
      }
    />
  );
};
