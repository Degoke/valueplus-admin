import * as React from 'react';
import { Routes } from 'src/routes';
import Login from 'src/pages/Login';
import Logout from 'src/pages/Logout';
import ForgotPassword from 'src/pages/ForgotPassword';
import Dashboard from 'src/pages/Dashboard';
import { useParams } from 'react-router-dom';
import ErrorPage from 'src/components/ErrorPage';

export const routes: {
  [key: string]: {
    path: Routes;
    component: React.FunctionComponent;
  };
} = {
  [Routes.HOME]: {
    path: Routes.HOME,
    component: Login,
  },
  [Routes.LOGOUT]: {
    path: Routes.LOGOUT,
    component: Logout,
  },
  [Routes.FORGOT_PASSWORD]: {
    path: Routes.FORGOT_PASSWORD,
    component: ForgotPassword,
  },
  [Routes.DASHBOARD]: {
    path: Routes.DASHBOARD,
    component: Dashboard,
  },
};

export default () => {
  const { type } = useParams<{ type: string }>();
  try {
    const Component = routes[`/${type}`].component;

    if (Component) return <Component />;

    throw new Error('Component Not Found');
  } catch (e) {
    console.log(e);
    return <div>{<ErrorPage fallbackRoute={Routes.HOME} />}</div>;
  }
};
