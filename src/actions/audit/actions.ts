import { ATypes, Audits, AuditRequestPayload } from './types';
import { createAction, createAsyncAction } from 'typesafe-actions';

export const destroy = createAction(
  ATypes.DESTROY,
)<void>();


export const getAudits = createAsyncAction(
  ATypes.REQ_AUDITS,
  ATypes.RES_AUDITS,
  ATypes.ERR_AUDITS,
)<AuditRequestPayload, Audits, string>();

export const getEntityActionMap = createAsyncAction(
  ATypes.REQ_ENTITY_ACTION_MAP,
  ATypes.RES_ENTITY_ACTION_MAP,
  ATypes.ERR_ENTITY_ACTION_MAP,
)<void, unknown, string>();
