import { combineEpics, Epic as RootEpic } from 'redux-observable';
import { from, of } from 'rxjs';
import { catchError, filter, mergeMap, map } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';
import * as action from '../actions';
import api from 'src/api/api';

const toResGetAudits: RootEpic = (action$) =>
  action$.pipe(
    filter(
      isActionOf([
        action.getAudits.request,
      ]),
    ),
    mergeMap(({ payload }) =>
      from(api.post(`/api/v1/audits?size=${payload.size}&page=${payload.page}`, payload)).pipe(
        map(action.getAudits.success),
        catchError((e) => of(action.getAudits.failure(e))),
      ),
    ),
  );

const toResGetEntityActionMap: RootEpic = (action$) =>
  action$.pipe(
    filter(
      isActionOf([
        action.getEntityActionMap.request,
      ]),
    ),
    mergeMap(() =>
      from(api.get('/api/v1/audits/entity-action-mapping')).pipe(
        map(action.getEntityActionMap.success),
        catchError((e) => of(action.getEntityActionMap.failure(e))),
      ),
    ),
  );

export default combineEpics(toResGetAudits, toResGetEntityActionMap);
