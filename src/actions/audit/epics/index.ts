import { combineEpics } from 'redux-observable';
import summary from './audits';

export default combineEpics(
  summary,
);
