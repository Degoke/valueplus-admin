import { RootState } from 'src/reducers';

export const SEntities = (state: RootState) => state.audits?.entityActionMap

export const SAudits = (state: RootState) => state.audits?.audit

export const SAuditsIsLoading = (state: RootState) => state.audits?.loading.audits
export const SAuditsIsSuccess = (state: RootState) => state.audits?.success.audits
export const SAuditsIsError = (state: RootState) => state.audits?.success.audits
