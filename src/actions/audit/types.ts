import { Action } from 'redux';
import { Pagination, PaginationPageOptions } from 'src/utils';

export interface State {
  entityActionMap: Entities;
  audit: Audits;
  loading: Loading;
  error: Error;
  success: Success;
};

export interface Entities {
  ACCOUNT: string[];
  PRODUCT: string[];
  PRODUCT_ORDER: string[];
  SETTING: string[];
  TRANSACTION: string[];
  USER: string[];
};

export type EntityType = keyof Entities;

export type ActionType = string

export interface Audit {
  action: ActionType
  actor: {
    email: string;
    userId: number;
    firstname: string;
    lastname: string;
    photo: string;
  };
  description: string;
  createdAt: Date;
  entityType: EntityType;
  newData: Record<string, any>;
  previousData: Record<string, any>;
}

export interface AuditRequestPayload extends PaginationPageOptions {
  endDate: string,
  startDate: string,
  action: ActionType,
  entityType: EntityType,
}

export type Audits = Pagination & {
  content: Audit[],
}

export type Loading = {
  audits: boolean,
};

export type Error = {
  audits: boolean,
};

export type Success = {
  audits: boolean,
};


export enum ATypes {
  DESTROY = 'DESTROY',

  REQ_AUDITS = 'REQ_AUDITS',
  RES_AUDITS = 'RES_AUDITS',
  ERR_AUDITS = 'ERR_AUDITS',

  REQ_ENTITY_ACTION_MAP = 'REQ_ENTITY_ACTION_MAP',
  RES_ENTITY_ACTION_MAP = 'RES_ENTITY_ACTION_MAP',
  ERR_ENTITY_ACTION_MAP = 'ERR_ENTITY_ACTION_MAP',
}


export interface IActionAudits extends Action {
  payload: unknown;
  type: ATypes;
}
