import {
  authLogout,
  authenticateUser,
  checkUserSession,
  checkUserSessionSuccess,
} from './actions';
import * as types from './types';

describe('auth -> actions', () => {
  it('authenticateUser', () => {
    expect(authenticateUser.request({ email: 'itunu', password: 'adekoya' })).toEqual({
      type: types.ATypes.REQ_LOGIN,
      payload: {
        email: 'itunu', password: 'adekoya',
      },
    });

    expect(authenticateUser.success(true)).toEqual({
      type: types.ATypes.RES_LOGIN,
      payload: true,
    });

    expect(authenticateUser.failure('error')).toEqual({
      type: types.ATypes.ERR_LOGIN,
      payload: 'error',
    });
  });

  it('checkUserSession', () => {
    expect(checkUserSession()).toEqual({
      type: types.ATypes.CHECK_USER_SESSION,
    });

    expect(checkUserSessionSuccess(true)).toEqual({
      type: types.ATypes.CHECK_USER_SESSION_SUCCESS,
      payload: true,
    });
  });

  it('authLogout', () => {
    expect(authLogout()).toEqual({
      type: types.ATypes.AUTH_LOGOUT,
    });
  });
});
