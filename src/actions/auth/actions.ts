import * as types from './types';
import { User } from 'src/actions/users/types';
import { createAction, createAsyncAction } from 'typesafe-actions';

export const authenticateUser = createAsyncAction(
  types.ATypes.REQ_LOGIN,
  types.ATypes.RES_LOGIN,
  types.ATypes.ERR_LOGIN,
)<types.LoginDetails, boolean, string>();

export const registerUser = createAsyncAction(
  types.ATypes.REQ_SIGNUP,
  types.ATypes.RES_SIGNUP,
  types.ATypes.ERR_SIGNUP,
)<types.SignupCredentials, User, string>();

export const resetPassword = createAsyncAction(
  types.ATypes.REQ_PASSWORD_RESET,
  types.ATypes.RES_PASSWORD_RESET,
  types.ATypes.ERR_PASSWORD_RESET,
)<types.PasswordReset, boolean, string>();

export const newPassword = createAsyncAction(
  types.ATypes.REQ_NEW_PASSWORD,
  types.ATypes.RES_NEW_PASSWORD,
  types.ATypes.ERR_NEW_PASSWORD,
)<types.NewPassword, boolean, string>();

export const checkUserSession = createAction(
  types.ATypes.CHECK_USER_SESSION,
)<void>();

export const checkUserSessionSuccess = createAction(
  types.ATypes.CHECK_USER_SESSION_SUCCESS,
)<boolean>();

export const authLogout = createAction(
  types.ATypes.AUTH_LOGOUT,
)<void>();

export const destroy = createAction(
  types.ATypes.DESTROY,
)<void>();
