import { checkUserSessionSuccess, authenticateUser, authLogout } from 'src/actions/auth';
import { ActionsObservable } from 'redux-observable';
import { toResLogin, toLogout } from './auth';
import api from 'src/api/api';

const loginDetails = {
  email: 'itunu',
  password: 'lol',
};

describe('Auth -> epic', () => {
  let store: any;

  beforeEach(() => {
    jest.clearAllMocks();
    store = {};
  });

  it('toResLogin -> success', async () => {
    const action$ = ActionsObservable.of(authenticateUser.request(loginDetails));
    const response: any = { data: { token: 'djgk' } };
    const emit: any = jest.spyOn(api, 'post').mockImplementation(() => Promise.resolve(response) as any);

    const epic$ = toResLogin(action$, store, null);

    epic$.subscribe(emit);
    await epic$;

    expect(emit).toHaveBeenCalledWith(authenticateUser.success(true));
  });

  it('toResLogin -> failure', async () => {
    const action$ = ActionsObservable.of(authenticateUser.request(loginDetails));
    const emit: any = jest.spyOn(api, 'post').mockImplementation(() => Promise.reject('lol') as any);

    const epic$ = toResLogin(action$, store, null);

    epic$.subscribe(emit);
    await epic$;

    expect(emit).toHaveBeenCalledWith(authenticateUser.failure('lol'));
  });

  it('logout', async () => {
    const action$ = ActionsObservable.of(authLogout());
    const emit = jest.fn();

    const epic$ = toLogout(action$, store, { localStorage });

    epic$.subscribe(emit);
    await epic$;
    // expect(localStorageMock.removeItem).toHaveBeenCalled();
    expect(emit).toHaveBeenCalledWith(checkUserSessionSuccess(false));
  });
});
