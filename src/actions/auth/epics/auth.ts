import { combineEpics, ofType, Epic as RootEpic } from 'redux-observable';
import { from, of } from 'rxjs';
import { catchError, filter, mergeMap, map } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';
import * as action from 'src/actions/auth';
import * as types from '../types';
import api from 'src/api/api';
import constants from 'src/utils/constants';
import { setWithExpiry, getWithExpiry } from 'src/utils/setLocalStorageWithExpiry';

export const toResSessionSuccess: RootEpic = (action$) =>
  action$.pipe(
    ofType(types.ATypes.CHECK_USER_SESSION),
    mergeMap(() => of(getWithExpiry(constants.token))
      .pipe(
        map(token => (action.checkUserSessionSuccess(!!token))),
      ),
    ),
  );

export const toResLogin: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.authenticateUser.request)),
    mergeMap(({ payload }) => from(api.post('/api/v1/auth/admin/login', payload))
      .pipe(
        map(({ token }) => of(setWithExpiry(constants.token, token, 86400000))),
        map(token => action.authenticateUser.success(!!token)),
        catchError((e) => of(action.authenticateUser.failure(e))),
      ),
    ),
  );

export const toResSignup: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.registerUser.request)),
    mergeMap(({ payload }) => from(api.post('/api/v1/register/admin', payload))
      .pipe(
        map((action.registerUser.success)),
        catchError((e) => of(action.registerUser.failure(e))),
      ),
    ),
  );

export const toResResetPassword: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.resetPassword.request)),
    mergeMap(({ payload }) => from(api.post('/api/v1/user/reset-password', payload))
      .pipe(
        map(() => action.resetPassword.success(true)),
        catchError((e) => of(action.resetPassword.failure(e))),
      ),
    ),
  );

export const toResNewPassword: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.newPassword.request)),
    mergeMap(({ payload }) => from(api.post('/api/v1/user/new-password', payload))
      .pipe(
        map(() => action.newPassword.success(true)),
        catchError((e) => of(action.newPassword.failure(e))),
      ),
    ),
  );
export const toLogout: RootEpic = (action$) =>
  action$.pipe(
    ofType(types.ATypes.AUTH_LOGOUT),
    mergeMap(() => of(localStorage.removeItem(constants.token))
      .pipe(
        map(() => (action.checkUserSessionSuccess(false))),
      ),
    ),
  );

export default combineEpics(
  toResLogin,
  toLogout,
  toResSignup,
  toResNewPassword,
  toResResetPassword,
  toResSessionSuccess,
);
