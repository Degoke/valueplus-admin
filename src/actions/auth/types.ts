import { Action } from 'redux';
import { User } from 'src/actions/users/types';

export interface State {
  isAuthenticated: boolean,
  isLoading: boolean,
  hasErrored: boolean,
  registrationSuccessful: User,
  passwordResetSuccessful: boolean
};

export interface PasswordReset {
  email: string;
};

export interface NewPassword {
  newPassword: string;
  resetToken: string;
};

export interface LoginDetails {
  email: string;
  password: string;
};

export interface SignupCredentials {
  email: string;
  firstname: string;
  lastname: string;
  phone: string;
  authorityIds?: number[]
};

//TODO probably bring Profile.UserDetails here
export enum ATypes {
  DESTROY = 'DESTROY',

  AUTH_LOGOUT = 'AUTH_LOGOUT',

  REQ_LOGIN = 'AUTH.REQ_LOGIN',
  RES_LOGIN = 'AUTH.RES_LOGIN',
  ERR_LOGIN = 'AUTH.ERR_LOGIN',

  REQ_SIGNUP = 'AUTH.REQ_SIGNUP',
  RES_SIGNUP = 'AUTH.RES_SIGNUP',
  ERR_SIGNUP = 'AUTH.ERR_SIGNUP',

  REQ_PASSWORD_RESET = 'AUTH.REQ_PASSWORD_RESET',
  RES_PASSWORD_RESET = 'AUTH.RES_PASSWORD_RESET',
  ERR_PASSWORD_RESET = 'AUTH.ERR_PASSWORD_RESET',

  REQ_NEW_PASSWORD = 'AUTH.REQ_NEW_PASSWORD',
  RES_NEW_PASSWORD = 'AUTH.RES_NEW_PASSWORD',
  ERR_NEW_PASSWORD = 'AUTH.ERR_NEW_PASSWORD',

  CHECK_USER_SESSION = 'AUTH_CHECK_USER_SESSION',
  CHECK_USER_SESSION_SUCCESS = 'CHECK_USER_SESSION_SUCCESS',
}

export interface IActionAuth extends Action {
  payload: boolean;
  type: ATypes;
}
