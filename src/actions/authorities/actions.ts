import * as types from './types';
import { createAsyncAction } from 'typesafe-actions';

export const getAuthorities = createAsyncAction(
  types.ATypes.REQ_AUTHORITIES,
  types.ATypes.RES_AUTHORITIES,
  types.ATypes.ERR_AUTHORITIES,
)<void, types.Authorities, string>();
