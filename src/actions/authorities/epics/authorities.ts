import { combineEpics, Epic as RootEpic } from 'redux-observable';
import { from, of } from 'rxjs';
import { catchError, filter, mergeMap, map } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';
import * as action from '../actions';
import api from 'src/api/api';
import { transformAuthorities } from '../transforms';

export const toResGetAuthorities: RootEpic = (action$) =>
  action$.pipe(
    filter(
      isActionOf([
        action.getAuthorities.request,
      ]),
    ),
    mergeMap(() =>
      from(api.get('/api/v1/user/authorities/ui')).pipe(
        map(transformAuthorities),
        map(action.getAuthorities.success),
        catchError((e) => of(action.getAuthorities.failure(e))),
      ),
    ),
  );


export default combineEpics(toResGetAuthorities);
