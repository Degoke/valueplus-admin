import { combineEpics } from 'redux-observable';
import authorities from './authorities';

export default combineEpics(
  authorities,
);
