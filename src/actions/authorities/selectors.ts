import { RootState } from 'src/reducers';

export const SAllAuthorities = (state: RootState) => state.authorities?.privileges;
