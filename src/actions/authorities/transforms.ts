import { TransformAuthorities } from 'src/actions/authorities/types';

export const transformAuthorities: TransformAuthorities = (response) =>
  response.map(({ authority, id }) => ({
    label: authority.replaceAll('_', ' '),
    value: id.toString()
  }));
