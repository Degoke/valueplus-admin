import { Action } from 'redux';

export type State = {
  privileges: Authorities;
};

export type Authorities = {
  label: string, value: string
}[]

export type AuthorityResponse = {
  authority: string;
  id: number
}

export type TransformAuthorities = (response: AuthorityResponse[]) => Authorities

export enum ATypes {

  REQ_AUTHORITIES = 'REQ_AUTHORITIES',
  RES_AUTHORITIES = 'RES_AUTHORITIES',
  ERR_AUTHORITIES = 'ERR_AUTHORITIES',
}

export interface IActionSummary extends Action {
  payload: any;
  type: ATypes;
}
