import * as types from './types';
import { createAction, createAsyncAction } from 'typesafe-actions';

export const destroy = createAction(
  types.ATypes.DESTROY,
)<void>();

export const getProduct = createAsyncAction(
  types.ATypes.REQ_GET_PRODUCT,
  types.ATypes.RES_GET_PRODUCT,
  types.ATypes.ERR_GET_PRODUCT,
)<types.IProduct['id'], types.IProduct, string>();

export const disableProduct = createAsyncAction(
  types.ATypes.REQ_DISABLE_PRODUCT_STATUS,
  types.ATypes.RES_DISABLE_PRODUCT_STATUS,
  types.ATypes.ERR_DISABLE_PRODUCT_STATUS,
)<types.IProduct['id'], types.Products, string>();

export const enableProduct = createAsyncAction(
  types.ATypes.REQ_ENABLE_PRODUCT_STATUS,
  types.ATypes.RES_ENABLE_PRODUCT_STATUS,
  types.ATypes.ERR_ENABLE_PRODUCT_STATUS,
)<types.IProduct['id'], types.Products, string>();

export const updateProduct = createAsyncAction(
  types.ATypes.REQ_UPDATE_PRODUCT,
  types.ATypes.RES_UPDATE_PRODUCT,
  types.ATypes.ERR_UPDATE_PRODUCT,
)<types.IProduct, types.IProduct, string>();

export const createProduct = createAsyncAction(
  types.ATypes.REQ_CREATE_PRODUCT,
  types.ATypes.RES_CREATE_PRODUCT,
  types.ATypes.ERR_CREATE_PRODUCT,
)<types.IProduct, types.IProduct, string>();

export const getProducts = createAsyncAction(
  types.ATypes.REQ_GET_PRODUCTS,
  types.ATypes.RES_GET_PRODUCTS,
  types.ATypes.ERR_GET_PRODUCTS,
)<types.ProductsPageOptions, types.Products, string>();

export const getProductOrders = createAsyncAction(
  types.ATypes.REQ_GET_PRODUCT_ORDERS,
  types.ATypes.RES_GET_PRODUCT_ORDERS,
  types.ATypes.ERR_GET_PRODUCT_ORDERS,
)<types.ProductOrdersOptions, types.ProductOrderItems, string>();

export const updateProductOrderStatus = createAsyncAction(
  types.ATypes.REQ_UPDATE_PRODUCT_ORDER_STATUS,
  types.ATypes.RES_UPDATE_PRODUCT_ORDER_STATUS,
  types.ATypes.ERR_UPDATE_PRODUCT_ORDER_STATUS,
)<types.ProductStatusUpdate, types.ProductOrderItem, string>();

export const getFilteredProductOrders = createAsyncAction(
  types.ATypes.REQ_GET_FILTERED_PRODUCT_ORDERS,
  types.ATypes.RES_GET_FILTERED_PRODUCT_ORDERS,
  types.ATypes.ERR_GET_FILTERED_PRODUCT_ORDERS,
)<types.ProductOrdersOptions, types.ProductOrderItems, string>();

export const getUserOrders = createAsyncAction(
  types.ATypes.REQ_USER_ORDERS,
  types.ATypes.RES_USER_ORDERS,
  types.ATypes.ERR_USER_ORDERS,
)<types.UserOrderFilter, types.ProductOrderItems, string>();
