import { combineEpics } from 'redux-observable';
import products from './products';
import productOrders from './productOrders';

export default combineEpics(
  products,
  productOrders,
);
