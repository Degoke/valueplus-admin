import * as action from '../actions';

import { Epic as RootEpic, combineEpics } from 'redux-observable';
import { catchError, filter, mergeMap, map } from 'rxjs/operators';
import { from, of } from 'rxjs';

import api from 'src/api/api';
import { isActionOf } from 'typesafe-actions';
import { transformOrders } from '../transforms';

export const toResGetProductOrders: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.getProductOrders.request)),
    mergeMap(({ payload }) =>
      from(api.get('/api/v1/product-orders', payload)).pipe(
        map(transformOrders),
        map(action.getProductOrders.success),
        catchError((e) => of(action.getProductOrders.failure(e))),
      ),
    ),
  );
export const toResGetFilteredProductOrders: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.getFilteredProductOrders.request)),
    mergeMap(({ payload }) =>
      from(api.get('/api/v1/product-orders/filter', payload)).pipe(
        map(action.getFilteredProductOrders.success),
        catchError((e) => of(action.getFilteredProductOrders.failure(e))),
      ),
    ),
  );

export const toResUserProductOrders: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf([action.getUserOrders.request])),
    mergeMap(({ payload }) =>
      from(
        api.post(
          `/api/v1/product-orders/searches?size=${payload.size}&page=${payload.page}`,
          payload,
        ),
      ).pipe(
        map(action.getUserOrders.success),
        catchError((e) => of(action.getUserOrders.failure(e))),
      ),
    ),
  );
export const toResUpdateOrderStatus: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.updateProductOrderStatus.request)),
    mergeMap(({ payload }) =>
      from(
        api.post(
          `/api/v1/product-orders/${payload.id}/status/${payload.status}/update`,
          payload,
        ),
      ).pipe(
        map(action.updateProductOrderStatus.success),
        catchError((e) => of(action.updateProductOrderStatus.failure(e))),
      ),
    ),
  );

export default combineEpics(
  toResGetProductOrders,
  toResUpdateOrderStatus,
  toResUserProductOrders,
  toResGetFilteredProductOrders,
);
