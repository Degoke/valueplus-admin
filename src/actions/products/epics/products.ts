import { combineEpics, Epic as RootEpic } from 'redux-observable';
import { of, from } from 'rxjs';
import { catchError, filter, map, mergeMap } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';
import * as action from '../actions';
import api from 'src/api/api';
import constants from 'src/utils/constants';

export const toResGetProducts: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.getProducts.request)),
    mergeMap(({ payload }) => from(api.get('/api/v1/products', payload))
      .pipe(
        map(action.getProducts.success),
        catchError((e) => of(action.getProducts.failure(e))),
      ),
    ),
  );

export const toResGetProduct: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.getProduct.request)),
    mergeMap(({ payload }) => from(api.get(`/api/v1/products/${payload}`))
      .pipe(
        map(action.getProduct.success),
        catchError((e) => of(action.getProduct.failure(e))),
      ),
    ),
  );

export const toResCreateProduct: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.createProduct.request)),
    mergeMap(({ payload }) => from(api.post('/api/v1/products', payload))
      .pipe(
        map(action.createProduct.success),
        catchError((e) => of(action.createProduct.failure(e))),
      ),
    ),
  );

export const toResUpdateProduct: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.updateProduct.request)),
    mergeMap(({ payload }) => from(api.put(`/api/v1/products/${payload.id}`, payload))
      .pipe(
        map(action.updateProduct.success),
        catchError((e) => of(action.updateProduct.failure(e))),
      ),
    ),
  );

export const toResDisableProduct: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.disableProduct.request)),
    mergeMap(({ payload }) => from(api.post(`/api/v1/products/${payload}/disable`, { id: payload }))
      .pipe(
        map(() => action.getProducts.request({ size: constants.PRODUCTS_ORDERS_SIZE })),
        catchError((e) => of(action.disableProduct.failure(e))),
      ),
    ),
  );


export const toResEnableProduct: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.enableProduct.request)),
    mergeMap(({ payload }) => from(api.post(`/api/v1/products/${payload}/enable`, { id: payload }))
      .pipe(
        map(() => action.getProducts.request({ size: constants.PRODUCTS_ORDERS_SIZE })),
        catchError((e) => of(action.enableProduct.failure(e))),
      ),
    ),
  );

export default combineEpics(
  toResGetProduct,
  toResGetProducts,
  toResDisableProduct,
  toResEnableProduct,
  toResUpdateProduct,
  toResCreateProduct,
);
