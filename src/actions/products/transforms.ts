import { ProductOrderItems } from './types';

export const transformOrders = (response: ProductOrderItems): ProductOrderItems =>
({
  ...response,
  content: response.content.map(order => ({ ...order, agentEmail: order.agent.email }))
});
