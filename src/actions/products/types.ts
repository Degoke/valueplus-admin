import { Action } from 'redux';
import * as types from 'src/actions/auth/types';
import { Status, Pagination, PaginationPageOptions } from 'src/utils';
import { User } from 'src/actions/users/types';

export interface State {
  loading: Loading,
  error: Error,
  success: Success,
  product: IProduct,
  products: Products,
  productOrders: ProductOrderItems;
  userOrders: ProductOrderItems;
};

export interface Loading {
  product: boolean,
  products: boolean,
  userOrders: boolean,
  productOrders: boolean,
};

export interface Error {
  product: boolean,
  products: boolean,
  userOrders: boolean,
  productOrders: boolean,
};

export interface Success {
  product: boolean,
  products: boolean,
  userOrders: boolean,
  productOrders: boolean,
};

export interface IProduct {
  id?: number;
  name: string;
  description: string;
  image: string;
  price: number;
  disabled?: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}

export type Products = Pagination & {
  content: IProduct[],
}

export type ProductsPageOptions = PaginationPageOptions;
export interface ProductOrderItem {
  id?: number;
  key?: number;
  agent: User;
  agentId: number;
  address: string;
  createdAt: Date;
  customerName: string;
  phoneNumber: string;
  productId: number;
  quantity: string;
  sellingPrice: string;
  status: Status;
  productName: string;
  updatedAt: string;
  totalProfit: number;
  productPrice: number;
  agentEmail: string
}

export type ProductOrderItems = Pagination & {
  content: ProductOrderItem[],
}

export interface ProductStatusUpdate {
  id: string,
  status: Status,
}

export type ProductOrdersOptions = PaginationPageOptions & {
  customerName?: string,
  productId?: number,
  status?: Status,
  startDate?: string,
  endDate?: string
}

export type UserOrderFilter = PaginationPageOptions & {
  agentId: number;
}

export enum ATypes {
  DESTROY = 'DESTROY',

  REQ_USER_ORDERS = 'REQ_USER_ORDERS',
  RES_USER_ORDERS = 'RES_USER_ORDERS',
  ERR_USER_ORDERS = 'ERR_USER_ORDERS',

  REQ_GET_PRODUCTS = 'REQ_GET_PRODUCTS',
  RES_GET_PRODUCTS = 'RES_GET_PRODUCTS',
  ERR_GET_PRODUCTS = 'ERR_GET_PRODUCTS',

  REQ_GET_PRODUCT = 'REQ_GET_PRODUCT',
  RES_GET_PRODUCT = 'RES_GET_PRODUCT',
  ERR_GET_PRODUCT = 'ERR_GET_PRODUCT',

  REQ_DISABLE_PRODUCT_STATUS = 'REQ_DISABLE_PRODUCT_STATUS',
  RES_DISABLE_PRODUCT_STATUS = 'RES_DISABLE_PRODUCT_STATUS',
  ERR_DISABLE_PRODUCT_STATUS = 'ERR_DISABLE_PRODUCT_STATUS',

  REQ_ENABLE_PRODUCT_STATUS = 'REQ_ENABLE_PRODUCT_STATUS',
  RES_ENABLE_PRODUCT_STATUS = 'RES_ENABLE_PRODUCT_STATUS',
  ERR_ENABLE_PRODUCT_STATUS = 'ERR_ENABLE_PRODUCT_STATUS',

  REQ_CREATE_PRODUCT = 'REQ_CREATE_PRODUCT',
  RES_CREATE_PRODUCT = 'RES_CREATE_PRODUCT',
  ERR_CREATE_PRODUCT = 'ERR_CREATE_PRODUCT',

  REQ_UPDATE_PRODUCT = 'REQ_UPDATE_PRODUCT',
  RES_UPDATE_PRODUCT = 'RES_UPDATE_PRODUCT',
  ERR_UPDATE_PRODUCT = 'ERR_UPDATE_PRODUCT',

  REQ_GET_PRODUCT_ORDERS = 'REQ_GET_PRODUCT_ORDERS',
  RES_GET_PRODUCT_ORDERS = 'RES_GET_PRODUCT_ORDERS',
  ERR_GET_PRODUCT_ORDERS = 'ERR_GET_PRODUCT_ORDERS',

  REQ_GET_FILTERED_PRODUCT_ORDERS = 'REQ_GET_FILTERED_PRODUCT_ORDERS',
  RES_GET_FILTERED_PRODUCT_ORDERS = 'RES_GET_FILTERED_PRODUCT_ORDERS',
  ERR_GET_FILTERED_PRODUCT_ORDERS = 'ERR_GET_FILTERED_PRODUCT_ORDERS',

  REQ_UPDATE_PRODUCT_ORDER_STATUS = 'REQ_UPDATE_PRODUCT_ORDER_STATUS',
  RES_UPDATE_PRODUCT_ORDER_STATUS = 'RES_UPDATE_PRODUCT_ORDER_STATUS',
  ERR_UPDATE_PRODUCT_ORDER_STATUS = 'ERR_UPDATE_PRODUCT_ORDER_STATUS',

}

export interface IActionProducts extends Action {
  payload: any;
  type: ATypes & types.ATypes.AUTH_LOGOUT
}
