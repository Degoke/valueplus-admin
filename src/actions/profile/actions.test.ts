import {
  getUser,
  updateProfile,
  updatePassword,
} from './actions';
import * as types from './types';
import { ROLE_TYPE } from 'src/utils';

const profile = {
  id: 1,
  address: 'null',
  agentCode: 'null',
  authorities: ['blaa'],
  email: 'hkdvjhfj@gail.com',
  emailVerified: true,
  firstname: 'Dapo',
  lastname: 'Olowoeyo',
  link: 'null',
  phone: '860669427',
  photo: 'null',
  referralCode: 'jndjfhkjdhgkjsdf',
  roleType: ROLE_TYPE.SUPER_AGENT,
  superAgentCode: 'null',
  transactionTokenSet: false,
}

const password = {
  oldPassword: 'strin',
  newPassword: 'string',
};

describe('auth -> actions', () => {
  it('getUser', () => {
    expect(getUser.request()).toEqual({
      type: types.ATypes.REQ_USER,
    });

    expect(getUser.success(profile)).toEqual({
      type: types.ATypes.RES_USER,
      payload: profile,
    });

    expect(getUser.failure('error')).toEqual({
      type: types.ATypes.ERR_USER,
      payload: 'error',
    });
  });
  it('updateProfile', () => {
    expect(updateProfile.request(profile)).toEqual({
      type: types.ATypes.REQ_PROFILE_UPDATE,
      payload: {
        email: 'itunu',
      },
    });

    expect(updateProfile.success({ ...profile, email: 'itunu' })).toEqual({
      type: types.ATypes.RES_PROFILE_UPDATE,
      payload: { ...profile, email: 'itunu' },
    });

    expect(updateProfile.failure('error')).toEqual({
      type: types.ATypes.ERR_PROFILE_UPDATE,
      payload: 'error',
    });
  });

  it('updatePassword', () => {
    expect(updatePassword.request(password)).toEqual({
      type: types.ATypes.REQ_PASSWORD_UPDATE,
      payload: password,
    });

    expect(updatePassword.success(profile)).toEqual({
      type: types.ATypes.RES_PASSWORD_UPDATE,
      payload: profile,
    });

    expect(updatePassword.failure('error')).toEqual({
      type: types.ATypes.ERR_PASSWORD_UPDATE,
      payload: 'error',
    });
  });

});
