import * as types from './types';
import { createAction, createAsyncAction } from 'typesafe-actions';
import { User } from '../users/types';

export const destroy = createAction(
  types.ATypes.DESTROY,
)<void>();

export const getUser = createAsyncAction(
  types.ATypes.REQ_USER,
  types.ATypes.RES_USER,
  types.ATypes.ERR_USER,
)<void, User, string>();

export const updatePassword = createAsyncAction(
  types.ATypes.REQ_PASSWORD_UPDATE,
  types.ATypes.RES_PASSWORD_UPDATE,
  types.ATypes.ERR_PASSWORD_UPDATE,
)<types.UpdatePassword, User, string>();

export const updateProfile = createAsyncAction(
  types.ATypes.REQ_PROFILE_UPDATE,
  types.ATypes.RES_PROFILE_UPDATE,
  types.ATypes.ERR_PROFILE_UPDATE,
)<User, User, string>();

export const uploadPicture = createAsyncAction(
  types.ATypes.REQ_PROFILE_IMAGE,
  types.ATypes.RES_PROFILE_IMAGE,
  types.ATypes.ERR_PROFILE_IMAGE,
)<string, types.ProfileImageResponse, string>();
