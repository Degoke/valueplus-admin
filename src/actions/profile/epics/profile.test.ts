import { getUser } from '../../profile/actions';
import { ActionsObservable } from 'redux-observable';
import { toResGetUser } from './profile';
import api from 'src/api/api';
import { ROLE_TYPE } from 'src/utils';

const response = {
  id: 1,
  address: 'null',
  agentCode: 'null',
  authorities: ['blaa'],
  email: 'hkdvjhfj@gail.com',
  emailVerified: true,
  firstname: 'Dapo',
  lastname: 'Olowoeyo',
  link: 'null',
  phone: '860669427',
  photo: 'null',
  referralCode: 'jndjfhkjdhgkjsdf',
  roleType: ROLE_TYPE.SUPER_AGENT,
  superAgentCode: 'null',
  transactionTokenSet: false,
};
describe('Server -> epic', () => {
  let store: any;

  beforeEach(() => {
    jest.clearAllMocks();
    store = {};
  });

  it('toResGetUser -> success', async () => {
    const action$ = ActionsObservable.of(getUser.request());

    const emit: any = jest.spyOn(api, 'get').mockImplementation(() => Promise.resolve(response) as any);

    const epic$ = toResGetUser(action$, store, null);

    epic$.subscribe(emit);
    await epic$;

    expect(emit).toHaveBeenCalledWith({
      meta: 0,
      ...getUser.success(response),
    });
  });

  it('toResGetUser -> failure', async () => {
    const action$ = ActionsObservable.of(getUser.request());
    const emit: any = jest.spyOn(api, 'get').mockImplementation(() => Promise.reject('lol') as any);

    const epic$ = toResGetUser(action$, store, null);

    epic$.subscribe(emit);
    await epic$;

    expect(emit).toHaveBeenCalledWith(getUser.failure('lol'));
  });
});
