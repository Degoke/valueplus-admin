import { combineEpics, Epic as RootEpic } from 'redux-observable';
import { from, of } from 'rxjs';
import { catchError, filter, mergeMap, map } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';
import * as action from 'src/actions/profile';
import api from 'src/api/api';
import { authLogout } from 'src/actions/auth';

export const toResGetUser: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.getUser.request)),
    mergeMap(() => from(api.get('/api/v1/user/current'))
      .pipe(
        map(action.getUser.success),
        catchError((e) => of(authLogout(e))),
      ),
    ),
  );

export const toResUpdateProfile: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.updateProfile.request)),
    mergeMap(({ payload }) => from(api.put('/api/v1/user/current', payload))
      .pipe(
        map(action.updateProfile.success),
        catchError((e) => of(action.updateProfile.failure(e))),
      ),
    ),
  );

export const toResUpdatePassword: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.updatePassword.request)),
    mergeMap(({ payload }) => from(api.post('/api/v1/user/current/password-change', payload))
      .pipe(
        map(action.updatePassword.success),
        catchError((e) => of(action.updatePassword.failure(e))),
      ),
    ),
  );

export const toResUploadPicture: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.uploadPicture.request)),
    mergeMap(({ payload }) => from(api.post('/api/v1/profile-picture', { photo: payload }))
      .pipe(
        map(action.uploadPicture.success),
        catchError((e) => of(action.uploadPicture.failure(e))),
      ),
    ),
  );

export default combineEpics(
  toResGetUser,
  toResUploadPicture,
  toResUpdateProfile,
  toResUpdatePassword,
);
