import { createSelector } from 'reselect';
import { RootState } from 'src/reducers';

export const currentUser = (state: RootState) => state.profile?.user;

export const profileLoading = (state: RootState) => state.profile?.loading;
export const profileSuccess = (state: RootState) => state.profile?.success;
export const profileError = (state: RootState) => state.profile?.error;

export const isUserLoading = (state: RootState) => state.profile?.loading?.user;
export const isUserSuccess = (state: RootState) => state.profile?.success?.user;
export const isUserError = (state: RootState) => state.profile?.error.user;

export const isProfileUpdateLoading = (state: RootState) =>
  state.profile?.loading?.profileUpdate;
export const isProfileUpdateSuccess = (state: RootState) =>
  state.profile?.success?.profileUpdate;
export const isProfileUpdateError = (state: RootState) =>
  state.profile?.error.profileUpdate;

export const isProfilePictureLoading = (state: RootState) =>
  state.profile?.loading?.profilePicture;
export const isProfilePictureSuccess = (state: RootState) =>
  state.profile?.success?.profilePicture;
export const isProfilePictureError = (state: RootState) =>
  state.profile?.error.profilePicture;

export const isPasswordUpdateLoading = (state: RootState) =>
  state.profile?.loading?.passwordUpdate;
export const isPasswordUpdateSuccess = (state: RootState) =>
  state.profile?.success?.passwordUpdate;
export const isPasswordUpdateError = (state: RootState) =>
  state.profile?.error.passwordUpdate;

export const SUserAuthorities = (state: RootState) => state.profile?.user?.authorities;

export const hasAuthority = (privilege: string) => createSelector(
  SUserAuthorities, auth => auth?.includes(privilege)
)
