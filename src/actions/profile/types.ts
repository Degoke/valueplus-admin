import { Action } from 'redux';
import * as types from 'src/actions/auth/types';
import { User } from 'src/actions/users/types';

export interface State {
  loading: Loading,
  error: Error,
  success: Success,
  user: User,
};
export interface Loading {
  user: boolean,
  profileUpdate: boolean,
  profilePicture: boolean,
  passwordUpdate: boolean,
};

export interface Error {
  user: boolean,
  profileUpdate: boolean,
  profilePicture: boolean,
  passwordUpdate: boolean,
};

export interface Success {
  user: boolean,
  profileUpdate: boolean,
  profilePicture: boolean,
  passwordUpdate: boolean,
};

export interface ProfileImageResponse {
  photo: string
}

export interface UpdatePassword {
  oldPassword: string;
  newPassword: string;
};

export enum ATypes {
  DESTROY = 'DESTROY',

  REQ_USER = 'REQ_USER',
  RES_USER = 'RES_USER',
  ERR_USER = 'ERR_USER',

  REQ_PROFILE_UPDATE = 'REQ_PROFILE_UPDATE',
  RES_PROFILE_UPDATE = 'RES_PROFILE_UPDATE',
  ERR_PROFILE_UPDATE = 'ERR_PROFILE_UPDATE',

  REQ_PASSWORD_UPDATE = 'REQ_PASSWORD_UPDATE',
  RES_PASSWORD_UPDATE = 'RES_PASSWORD_UPDATE',
  ERR_PASSWORD_UPDATE = 'ERR_PASSWORD_UPDATE',

  REQ_PROFILE_IMAGE = 'REQ_PROFILE_IMAGE',
  RES_PROFILE_IMAGE = 'RES_PROFILE_IMAGE',
  ERR_PROFILE_IMAGE = 'ERR_PROFILE_IMAGE',
}

export interface IActionProfile extends Action {
  payload: boolean;
  type: ATypes & types.ATypes.AUTH_LOGOUT
}
