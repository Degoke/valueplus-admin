import { routerActions } from 'connected-react-router';
import * as authActions from './auth/actions';
import * as auditActions from './audit/actions';
import * as usersActions from './users/actions';
import * as profileActions from './profile/actions';
import * as transactionsActions from './transactions/actions';
import * as walletActions from './wallet/actions';
import * as summaryActions from './summary/actions';
import * as settingsActions from './settings/actions';
import * as superAgentsActions from './superAgents/actions';
import * as authoritiesActions from './authorities/actions';
import * as productActions from './products/actions';

export default {
  router: routerActions,
  auth: authActions,
  audits: auditActions,
  users: usersActions,
  wallet: walletActions,
  summary: summaryActions,
  products: productActions,
  profile: profileActions,
  settings: settingsActions,
  authorities: authoritiesActions,
  superAgents: superAgentsActions,
  transactions: transactionsActions,
};
