import { combineEpics } from 'redux-observable';
import auth from './auth/epics';
import users from './users/epics';
import audit from './audit/epics';
import wallet from './wallet/epics';
import summary from './summary/epics';
import profile from './profile/epics';
import transactions from './transactions/epics';
import products from './products/epics';
import settings from './settings/epics';
import authorities from './authorities/epics';
import superAgents from './superAgents/epics';

export default combineEpics(
  auth,
  users,
  audit,
  wallet,
  summary,
  profile,
  products,
  settings,
  authorities,
  superAgents,
  transactions,
);
