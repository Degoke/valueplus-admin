import * as types from './types';
import { createAction, createAsyncAction } from 'typesafe-actions';

export const destroy = createAction(
  types.ATypes.DESTROY,
)<void>();

export const updateCommission = createAsyncAction(
  types.ATypes.REQ_UPDATE_COMMISSION,
  types.ATypes.RES_UPDATE_COMMISSION,
  types.ATypes.ERR_UPDATE_COMMISSION,
)<types.CommissionPayload, types.CommissionPageOptions, string>();

export const getCommissionLogs = createAsyncAction(
  types.ATypes.REQ_GET_COMMISSION_LOGS,
  types.ATypes.RES_GET_COMMISSION_LOGS,
  types.ATypes.ERR_GET_COMMISSION_LOGS,
)<types.CommissionPageOptions, types.CommissionLogs, string>();

export const getScheduledCommissions = createAsyncAction(
  types.ATypes.REQ_GET_SCHEDULE_COMMISSION,
  types.ATypes.RES_GET_SCHEDULE_COMMISSION,
  types.ATypes.ERR_GET_SCHEDULE_COMMISSION,
)<types.CommissionPageOptions, types.Schedules, string>();
