import { combineEpics, Epic as RootEpic } from 'redux-observable';
import { from, of } from 'rxjs';
import { catchError, filter, mergeMap, map } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';
import * as action from '../actions';
import api from 'src/api/api';
import constants from 'src/utils/constants';

export const toResUpdateCommision: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf([action.updateCommission.request])),
    mergeMap(({ payload }) =>
      from(api.put('/api/v1/setting', payload)).pipe(
        map(() => action.updateCommission.success({ size: constants.TRANSACTION_PAGE_SIZE })),
        catchError((e) => of(action.updateCommission.failure(e))),
      ),
    ),
  );

export const toResGetCommisionLogs: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf([action.getCommissionLogs.request])),
    mergeMap(({ payload }) =>
      from(api.get('/api/v1/setting/logs', payload)).pipe(
        map(action.getCommissionLogs.success),
        catchError((e) => of(action.getCommissionLogs.failure(e))),
      ),
    ),
  );

export const toResGetScheduledCommissions: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf([action.getScheduledCommissions.request, action.updateCommission.success])),
    mergeMap(({ payload }) =>
      from(api.get('/api/v1/setting/schedules', payload)).pipe(
        map(action.getScheduledCommissions.success),
        catchError((e) => of(action.getScheduledCommissions.failure(e))),
      ),
    ),
  );

export default combineEpics(toResUpdateCommision, toResGetCommisionLogs, toResGetScheduledCommissions);
