import { RootState } from 'src/reducers';

export const SCommissionLogs = (state: RootState) => state.settings?.commissionLogs;
export const SScheduledCommisions = (state: RootState) => state.settings?.scheduledCommissions;

export const SCommissionError = (state: RootState) => state.settings?.error?.commission;
export const SCommissionLoading = (state: RootState) => state.settings?.loading?.commission;
export const SCommissionSuccess = (state: RootState) => state.settings?.success?.commission;

export const SCommissionLogsError = (state: RootState) => state.settings?.error?.commissionLogs;
export const SCommissionLogsLoading = (state: RootState) => state.settings?.loading?.commissionLogs;
export const SCommissionLogsSuccess = (state: RootState) => state.settings?.success?.commissionLogs;

export const SCommissionScheduleError = (state: RootState) => state.settings?.error?.schedule;
export const SCommissionScheduleLoading = (state: RootState) => state.settings?.loading?.schedule;
export const SCommissionScheduleSuccess = (state: RootState) => state.settings?.success?.schedule;
