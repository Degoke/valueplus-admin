import { Action } from 'redux';
import { Pagination, PaginationPageOptions } from 'src/utils';

export type State = {
  error: Error,
  loading: Loading,
  success: Success,
  commissionLogs: CommissionLogs;
  scheduledCommissions: Schedules
};

export type Loading = {
  commission: boolean,
  schedule: boolean,
  commissionLogs: boolean,
};

export type Error = {
  commission: boolean,
  schedule: boolean,
  commissionLogs: boolean,
};

export type Success = {
  commission: boolean,
  schedule: boolean,
  commissionLogs: boolean,
};

export type CommissionPayload = {
  commissionPercentage: string;
  commissionEffectiveDate: string;
};

export type CommissionLogs = Pagination & {
  content: CommissionLog[],
}

export interface CommissionLog {
  commissionPercentage: number;
  createdAt: Date;
  initiator: string;
  prevCommissionPercentage?: number;
  updatedAt: Date;
};

export interface Schedule extends CommissionLog {
  status: string;
  effectiveDate: string;
};

export type Schedules = Pagination & {
  content: Schedule[],
}

export type CommissionPageOptions = PaginationPageOptions;

export enum ATypes {
  DESTROY = 'DESTROY',

  REQ_GET_COMMISSION_LOGS = 'REQ_GET_COMMISSION_LOGS',
  RES_GET_COMMISSION_LOGS = 'RES_GET_COMMISSION_LOGS',
  ERR_GET_COMMISSION_LOGS = 'ERR_GET_COMMISSION_LOGS',

  REQ_UPDATE_COMMISSION = 'REQ_UPDATE_COMMISSION',
  RES_UPDATE_COMMISSION = 'RES_UPDATE_COMMISSION',
  ERR_UPDATE_COMMISSION = 'ERR_UPDATE_COMMISSION',

  REQ_GET_SCHEDULE_COMMISSION = 'REQ_GET_SCHEDULE_COMMISSION',
  RES_GET_SCHEDULE_COMMISSION = 'RES_GET_SCHEDULE_COMMISSION',
  ERR_GET_SCHEDULE_COMMISSION = 'ERR_GET_SCHEDULE_COMMISSION',
}

export interface IActionSettings extends Action {
  payload: any;
  type: ATypes;
}
