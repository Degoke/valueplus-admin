import * as types from './types';
import { createAsyncAction } from 'typesafe-actions';

export const getSummary = createAsyncAction(
  types.ATypes.REQ_SUMMARY,
  types.ATypes.RES_SUMMARY,
  types.ATypes.ERR_SUMMARY,
)<void, types.Summary, string>();
