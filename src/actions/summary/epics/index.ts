import { combineEpics } from 'redux-observable';
import summary from './summary';

export default combineEpics(
  summary,
);
