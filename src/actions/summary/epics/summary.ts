import { combineEpics, Epic as RootEpic } from 'redux-observable';
import { from, of } from 'rxjs';
import { catchError, filter, mergeMap, map } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';
import * as action from '../actions';
import api from 'src/api/api';

export const toResGetSummary: RootEpic = (action$) =>
  action$.pipe(
    filter(
      isActionOf([
        action.getSummary.request,
      ]),
    ),
    mergeMap(() =>
      from(api.get('/api/v1/summary')).pipe(
        map(action.getSummary.success),
        catchError((e) => of(action.getSummary.failure(e))),
      ),
    ),
  );

export default combineEpics(toResGetSummary);
