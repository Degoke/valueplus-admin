import { RootState } from 'src/reducers';

export const SSummary = (state: RootState) => state.summary?.summary;
