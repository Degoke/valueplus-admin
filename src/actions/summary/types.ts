import { Action } from 'redux';

export type State = {
  summary: Summary;
};

export type Summary = {
  totalActiveUsers: number,
  pendingWithdrawalCount: number,
  totalApprovedWithdrawals: number,
  totalDownloads: number,
  totalPendingWithdrawal: number,
  totalProductAgentProfits: number,
  totalProductSales: number,
  totalAgents: number
}

export enum ATypes {

  REQ_SUMMARY = 'REQ_SUMMARY',
  RES_SUMMARY = 'RES_SUMMARY',
  ERR_SUMMARY = 'ERR_SUMMARY',
}

export interface IActionSummary extends Action {
  payload: any;
  type: ATypes;
}
