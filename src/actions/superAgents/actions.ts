import * as types from './types';
import { createAction, createAsyncAction } from 'typesafe-actions';
import { User, Users } from 'src/actions/users/types';
import { SignupCredentials } from '../auth/types';
import { PaginationPageOptions } from 'src/utils';

export const destroy = createAction(
  types.ATypes.DESTROY,
)<void>();

export const createSuperAgent = createAsyncAction(
  types.ATypes.REQ_CREATE_SUPER_AGENT,
  types.ATypes.RES_CREATE_SUPER_AGENT,
  types.ATypes.ERR_CREATE_SUPER_AGENT,
)<SignupCredentials, User, string>();

export const getSuperAgents = createAsyncAction(
  types.ATypes.REQ_GET_SUPER_AGENTS,
  types.ATypes.RES_GET_SUPER_AGENTS,
  types.ATypes.ERR_GET_SUPER_AGENTS,
)<PaginationPageOptions, Users, string>();

export const getSuperAgentActiveUsers = createAsyncAction(
  types.ATypes.REQ_GET_SUPER_AGENT_ACTIVE_USERS,
  types.ATypes.RES_GET_SUPER_AGENT_ACTIVE_USERS,
  types.ATypes.ERR_GET_SUPER_AGENT_ACTIVE_USERS,
)<types.FilterOptions, User[], string>();
