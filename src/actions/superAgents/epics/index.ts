import { combineEpics } from 'redux-observable';
import superAgents from './superAgents';

export default combineEpics(
  superAgents,
);
