import { combineEpics, Epic as RootEpic } from 'redux-observable';
import { from, of } from 'rxjs';
import { catchError, filter, mergeMap, map } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';
import * as action from '../actions';
import api from 'src/api/api';

export const toResCreateSuperAgent: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.createSuperAgent.request)),
    mergeMap(({ payload }) => from(api.post('/api/v1/register/super-agent', payload))
      .pipe(
        map((action.createSuperAgent.success)),
        catchError((e) => of(action.createSuperAgent.failure(e))),
      ),
    ),
  );

export const toResGetSuperAgents: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf([action.getSuperAgents.request])),
    mergeMap(({ payload }) =>
      from(api.get('/api/v1/user/super-agents', payload)).pipe(
        map(action.getSuperAgents.success),
        catchError((e) => of(action.getSuperAgents.failure(e))),
      ),
    ),
  );

export const toResGetSuperAgentActiveUsers: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf([action.getSuperAgentActiveUsers.request])),
    mergeMap(({ payload }) =>
      from(api.post('/api/v1/user/super-agents/filter-active-users', payload)).pipe(
        map(action.getSuperAgentActiveUsers.success),
        catchError((e) => of(action.getSuperAgentActiveUsers.failure(e))),
      ),
    ),
  );

export default combineEpics(toResCreateSuperAgent, toResGetSuperAgents, toResGetSuperAgentActiveUsers);
