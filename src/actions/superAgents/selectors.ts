import { RootState } from 'src/reducers';

export const SSuperAgent = (state: RootState) => state.superAgents?.superAgent;
export const SSuperAgents = (state: RootState) => state.superAgents?.superAgents;
export const SSuperAgentActiveUsers = (state: RootState) => state.superAgents?.superAgentActiveUsers;

export const SSuperAgentLoading = (state: RootState) => state.superAgents?.loading?.superAgent;
export const SSuperAgentError = (state: RootState) => state.superAgents?.error?.superAgent;
export const SSuperAgentSuccess = (state: RootState) => state.superAgents?.success?.superAgent;

export const SSuperAgentsLoading = (state: RootState) => state.superAgents?.loading?.superAgents;
export const SSuperAgentsError = (state: RootState) => state.superAgents?.error?.superAgents;
export const SSuperAgentsSuccess = (state: RootState) => state.superAgents?.success?.superAgents;

export const SSuperAgentUsersLoading = (state: RootState) => state.superAgents?.loading?.superAgents;
export const SSuperAgentUsersError = (state: RootState) => state.superAgents?.error?.superAgents;
export const SSuperAgentUsersSuccess = (state: RootState) => state.superAgents?.success?.superAgents;
