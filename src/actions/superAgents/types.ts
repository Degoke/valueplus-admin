import { Action } from 'redux';
import { User, Users } from 'src/actions/users/types';
import { PaginationPageOptions } from 'src/utils';

export interface State {
  error: Error;
  loading: Loading;
  success: Success;
  superAgent: User;
  superAgents: Users;
  superAgentActiveUsers: Users;
};

export interface Loading {
  superAgent: boolean;
  superAgents: boolean;
  superAgentActiveUsers: boolean;
};

export interface Error {
  superAgent: boolean;
  superAgents: boolean;
  superAgentActiveUsers: boolean;
};

export interface Success {
  superAgent: boolean;
  superAgents: boolean;
  superAgentActiveUsers: boolean;
};

export type FilterOptions = PaginationPageOptions & {
  endDate: string;
  startDate: string;
  superAgentCode: string;
}

export enum ATypes {
  DESTROY = 'DESTROY',
  REQ_CREATE_SUPER_AGENT = 'REQ_CREATE_SUPER_AGENT',
  RES_CREATE_SUPER_AGENT = 'RES_CREATE_SUPER_AGENT',
  ERR_CREATE_SUPER_AGENT = 'ERR_CREATE_SUPER_AGENT',

  REQ_GET_SUPER_AGENTS = 'REQ_GET_SUPER_AGENTS',
  RES_GET_SUPER_AGENTS = 'RES_GET_SUPER_AGENTS',
  ERR_GET_SUPER_AGENTS = 'ERR_GET_SUPER_AGENTS',

  REQ_GET_SUPER_AGENT_ACTIVE_USERS = 'REQ_GET_SUPER_AGENT_ACTIVE_USERS',
  RES_GET_SUPER_AGENT_ACTIVE_USERS = 'RES_GET_SUPER_AGENT_ACTIVE_USERS',
  ERR_GET_SUPER_AGENT_ACTIVE_USERS = 'ERR_GET_SUPER_AGENT_ACTIVE_USERS',
}

export interface IActionSuperAgents extends Action {
  payload: any;
  type: ATypes;
}
