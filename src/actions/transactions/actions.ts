import * as types from './types';
import { createAction, createAsyncAction } from 'typesafe-actions';

export const destroy = createAction(
  types.ATypes.DESTROY,
)<void>();

export const getTransactions = createAsyncAction(
  types.ATypes.REQ_TRANSACTIONS,
  types.ATypes.RES_TRANSACTIONS,
  types.ATypes.ERR_TRANSACTIONS,
)<types.TransactionsPageOptions, types.Transactions, string>();

export const getFilteredTransactions = createAsyncAction(
  types.ATypes.REQ_TRANSACTIONS_FILTER,
  types.ATypes.RES_TRANSACTIONS_FILTER,
  types.ATypes.ERR_TRANSACTIONS_FILTER,
)<types.TransactionFilterOptions, types.Transactions, string>();

export const refreshTransaction = createAsyncAction(
  types.ATypes.REQ_REFRESH_TRANSACTIONS,
  types.ATypes.RES_REFRESH_TRANSACTIONS,
  types.ATypes.ERR_REFRESH_TRANSACTIONS,
)<string, types.Transactions, string>();
