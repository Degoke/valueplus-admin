import { combineEpics } from 'redux-observable';
import transactions from './transactions';

export default combineEpics(
  transactions,
);
