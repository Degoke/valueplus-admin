import { combineEpics, Epic as RootEpic } from 'redux-observable';
import { from, of } from 'rxjs';
import { catchError, filter, mergeMap, map } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';
import * as action from 'src/actions/transactions';
import api from 'src/api/api';
import constants from 'src/utils/constants';

export const toResGetTransactions: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.getTransactions.request)),
    mergeMap(({ payload }) => from(api.get('/api/v1/transactions', payload))
      .pipe(
        map(action.getTransactions.success),
        catchError((e) => of(action.getTransactions.failure(e))),
      ),
    ),
  );


export const toResGetFilteredTransactions: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.getFilteredTransactions.request)),
    mergeMap(({ payload }) => from(api.get(`/api/v1/transactions/filter`, payload))
      .pipe(
        map(action.getFilteredTransactions.success),
        catchError((e) => of(action.getFilteredTransactions.failure(e))),
      ),
    ),
  );

export const toResRefreshTransactions: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.refreshTransaction.request)),
    mergeMap(({ payload }) => from(api.get(`/api/v1/transactions/verify/${payload}`))
      .pipe(
        map(() => action.getTransactions.request({ size: constants.TRANSACTION_PAGE_SIZE })),
        catchError((e) => of(action.refreshTransaction.failure(e))),
      ),
    ),
  );

export default combineEpics(
  toResGetTransactions,
  toResGetFilteredTransactions,
  toResRefreshTransactions,
);
