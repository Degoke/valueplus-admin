import { RootState } from 'src/reducers';

export const SBanks = (state: RootState) => state.transactions?.banks;
export const STransaction = (state: RootState) => state.transactions?.transaction;
export const STransactions = (state: RootState) => state.transactions?.transactions;

export const STransactionsLoading = (state: RootState) => state.transactions?.loading.transactions;
export const STransactionsSuccess = (state: RootState) => state.transactions?.success.transactions;
export const STransactionsError = (state: RootState) => state.transactions?.error.transactions;
