import { Action } from 'redux';
import * as types from 'src/actions/auth/types';
import { DateFilter, TransactionStatus, Pagination, PaginationPageOptions } from 'src/utils';

export type State = {
  banks: Bank[],
  transactions: Transactions,
  transaction: Transaction,
  loading: Loading,
  error: Error,
  success: Success
};

export type Transaction = {
  accountNumber: string,
  amount: number,
  bankCode: string,
  createdAt: Date,
  currency: string,
  id: number,
  reference: string,
  paystackStatus: TransactionStatus,
  status: TransactionStatus,
  updatedAt: Date,
  userId: number
}

export type Transactions = Pagination & {
  content: Transaction[],
}

export type Bank = {
  name: string,
  code: string,
  slug?: string,
  longcode?: string,
  gateway?: string,
  active?: boolean,
  deleted?: boolean,
  id: number,
  created_at?: string,
  updated_at?: string
}

export type Loading = {
  transactions: boolean,
};

export type Error = {
  transactions: boolean,
};

export type Success = {
  transactions: boolean,
};

export type TransactionsPageOptions = PaginationPageOptions;

export type PaymentRequest = {
  amount: number
};

export type ReferenceFilter = {
  reference: string,
};
export type TransactionFilterOptions = Partial<DateFilter> & PaginationPageOptions & {
  status?: TransactionStatus,
}
export enum ATypes {
  DESTROY = 'DESTROY',

  REQ_TRANSACTIONS = 'REQ_TRANSACTIONS',
  RES_TRANSACTIONS = 'RES_TRANSACTIONS',
  ERR_TRANSACTIONS = 'ERR_TRANSACTIONS',

  REQ_REFRESH_TRANSACTIONS = 'REQ_REFRESH_TRANSACTIONS',
  RES_REFRESH_TRANSACTIONS = 'RES_REFRESH_TRANSACTIONS',
  ERR_REFRESH_TRANSACTIONS = 'ERR_REFRESH_TRANSACTIONS',

  REQ_TRANSACTIONS_FILTER = 'REQ_TRANSACTIONS_FILTER',
  RES_TRANSACTIONS_FILTER = 'RES_TRANSACTIONS_FILTER',
  ERR_TRANSACTIONS_FILTER = 'ERR_TRANSACTIONS_FILTER',
}

export interface IActionTransaction extends Action {
  payload: any;
  type: ATypes & types.ATypes.AUTH_LOGOUT;
}
