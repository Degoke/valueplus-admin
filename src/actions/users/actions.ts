import * as types from './types';
import { createAction, createAsyncAction } from 'typesafe-actions';

export const destroy = createAction(
  types.ATypes.DESTROY,
)<void>();

export const getUsers = createAsyncAction(
  types.ATypes.REQ_USERS,
  types.ATypes.RES_USERS,
  types.ATypes.ERR_USERS,
)<types.UserPageOptions, types.Users, string>();

export const getFilteredUsers = createAsyncAction(
  types.ATypes.REQ_GET_FILTERED_USERS,
  types.ATypes.RES_GET_FILTERED_USERS,
  types.ATypes.ERR_GET_FILTERED_USERS,
)<types.FilterOptions, types.Users, string>();

export const enableUser = createAsyncAction(
  types.ATypes.REQ_ENABLE_USER,
  types.ATypes.RES_ENABLE_USER,
  types.ATypes.ERR_ENABLE_USER,
)<number, types.Users, string>();

export const disableUser = createAsyncAction(
  types.ATypes.REQ_DISABLE_USER,
  types.ATypes.RES_DISABLE_USER,
  types.ATypes.ERR_DISABLE_USER,
)<number, types.Users, string>();

export const updateUserAuthorities = createAsyncAction(
  types.ATypes.REQ_UPDATE_USER_AUTHORITIES,
  types.ATypes.RES_UPDATE_USER_AUTHORITIES,
  types.ATypes.ERR_UPDATE_USER_AUTHORITIES,
)<types.UpdateUserAuthorities, types.Users, string>();
