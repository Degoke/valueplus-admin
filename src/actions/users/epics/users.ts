import { combineEpics, Epic as RootEpic } from 'redux-observable';
import { from, of } from 'rxjs';
import { catchError, filter, mergeMap, map } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';
import * as action from 'src/actions/users';
import api from 'src/api/api';
import constants from 'src/utils/constants';
import { ROLE_TYPE } from 'src/utils';
import { transformUsers } from '../transforms';

export const toResGetUsers: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.getUsers.request)),
    mergeMap(({ payload }) =>
      from(api.get('/api/v1/user', payload)).pipe(
        map(transformUsers),
        map(action.getUsers.success),
        catchError((e) => of(action.getUsers.failure(e))),
      ),
    ),
  );

export const toResGetFilterUsers: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf([action.getFilteredUsers.request])),
    mergeMap(({ payload }) =>
      from(
        api.post(
          `/api/v1/user/searches?size=${payload.size}&page=${payload.page}`,
          payload,
        ),
      ).pipe(
        map(transformUsers),
        map(action.getFilteredUsers.success),
        catchError((e) => of(action.getFilteredUsers.failure(e))),
      ),
    ),
  );

export const toResDisableUser: RootEpic = (action$, state$) =>
  action$.pipe(
    filter(isActionOf(action.disableUser.request)),
    mergeMap(({ payload }) =>
      from(
        api.post(`/api/v1/user/${payload}/disable`, { userId: payload }),
      ).pipe(
        map(() =>
          state$.value.router.location.pathname === '/dashboard/create-admin' ?
            action.getFilteredUsers.request({ roleType: ROLE_TYPE.ADMIN }) :
            action.getUsers.request({ size: constants.PRODUCTS_ORDERS_SIZE })
        ),
        catchError((e) => of(action.disableUser.failure(e))),
      ),
    ),
  );

export const toResEnableUser: RootEpic = (action$, state$) =>
  action$.pipe(
    filter(isActionOf(action.enableUser.request)),
    mergeMap(({ payload }) =>
      from(
        api.post(`/api/v1/user/${payload}/enable`, { userId: payload }),
      ).pipe(
        map(() =>
          state$.value.router.location.pathname === '/dashboard/create-admin' ?
            action.getFilteredUsers.request({ roleType: ROLE_TYPE.ADMIN }) :
            action.getUsers.request({ size: constants.PRODUCTS_ORDERS_SIZE }),
        ),
        catchError((e) => of(action.enableUser.failure(e))),
      ),
    ),
  );

export const toResUpdateUserAuthorities: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.updateUserAuthorities.request)),
    mergeMap(({ payload: { userId, authorities } }) =>
      from(
        api.post(`/api/v1/user/${userId}/update-authority`, { userId, authorities }),
      ).pipe(
        map(() =>
          action.getFilteredUsers.request({ roleType: ROLE_TYPE.ADMIN })
        ),
        catchError((e) => of(action.updateUserAuthorities.failure(e))),
      ),
    ),
  );
export default combineEpics(
  toResGetUsers,
  toResDisableUser,
  toResEnableUser,
  toResGetFilterUsers,
  toResUpdateUserAuthorities,
);
