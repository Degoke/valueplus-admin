import { RootState } from 'src/reducers';

export const getAllUsers = (state: RootState) => state.users?.users;

export const isUsersLoading = (state: RootState) => state.users?.loading?.users;
export const isUsersSuccess = (state: RootState) => state.users?.success?.users;
export const isUsersError = (state: RootState) => state.users?.error.users;
