import { Users } from 'src/actions/users/types';

export const transformUsers = (response: Users): Users =>
({
  ...response,
  content: response.content.map(user => ({ ...user, authorities: user.authorities.map(auth => auth.replaceAll('_', ' ')) }))
});
