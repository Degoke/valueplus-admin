import { Action } from 'redux';
import * as types from 'src/actions/auth/types';
import { ROLE_TYPE, Pagination, PaginationPageOptions } from 'src/utils';

export interface State {
  users: Users;
  loading: Loading;
  error: Error,
  success: Success;
};

export interface User {
  key?: number;
  id: number;
  address: string;
  authorities: string[];
  agentCode: string;
  email: string;
  enabled?: boolean;
  emailVerified: boolean;
  firstname: string;
  lastname: string;
  link: string;
  phone: string;
  photo: string;
  roleType: ROLE_TYPE;
  referralCode: string;
  superAgentCode: string;
  transactionTokenSet: boolean;
}


export type Users = Pagination & {
  content: User[];
}

export interface Loading {
  users: boolean,
};

export interface Error {
  users: boolean,
};

export interface Success {
  users: boolean,
};

export type FilterOptions = PaginationPageOptions & {
  email?: string;
  firstname?: string;
  lastname?: string;
  roleType?: ROLE_TYPE;
  superAgentCode?: string;
}

export interface UpdateUserAuthorities {
  userId: number;
  authorities: number[]
}


export type UserPageOptions = PaginationPageOptions;

export enum ATypes {
  DESTROY = 'DESTROY',

  REQ_USERS = 'REQ_USERS',
  RES_USERS = 'RES_USERS',
  ERR_USERS = 'ERR_USERS',

  REQ_ENABLE_USER = 'REQ_ENABLE_USER',
  RES_ENABLE_USER = 'RES_ENABLE_USER',
  ERR_ENABLE_USER = 'ERR_ENABLE_USER',

  REQ_DISABLE_USER = 'REQ_DISABLE_USER',
  RES_DISABLE_USER = 'RES_DISABLE_USER',
  ERR_DISABLE_USER = 'ERR_DISABLE_USER',

  REQ_GET_FILTERED_USERS = 'REQ_GET_FILTERED_USERS',
  RES_GET_FILTERED_USERS = 'RES_GET_FILTERED_USERS',
  ERR_GET_FILTERED_USERS = 'ERR_GET_FILTERED_USERS',

  REQ_UPDATE_USER_AUTHORITIES = 'REQ_UPDATE_USER_AUTHORITIES',
  RES_UPDATE_USER_AUTHORITIES = 'RES_UPDATE_USER_AUTHORITIES',
  ERR_UPDATE_USER_AUTHORITIES = 'ERR_UPDATE_USER_AUTHORITIES',
}

export interface IActionUsers extends Action {
  payload: any;
  type: ATypes & types.ATypes.AUTH_LOGOUT;
}
