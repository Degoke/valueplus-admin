import * as types from './types';
import { createAction, createAsyncAction } from 'typesafe-actions';

export const destroy = createAction(
  types.ATypes.DESTROY,
)<void>();

export const getWallet = createAsyncAction(
  types.ATypes.REQ_ALL_WALLET,
  types.ATypes.RES_ALL_WALLET,
  types.ATypes.ERR_ALL_WALLET,
)<void, types.Wallet, string>();

export const getWalletHistoryFilter = createAsyncAction(
  types.ATypes.REQ_WALLET_HISTORY_FILTER,
  types.ATypes.RES_WALLET_HISTORY_FILTER,
  types.ATypes.ERR_WALLET_HISTORY_FILTER,
)<types.WalletHistoryPageOptions, types.WalletHistory, string>();
