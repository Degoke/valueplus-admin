import { combineEpics, Epic as RootEpic } from 'redux-observable';
import { from, of } from 'rxjs';
import { catchError, filter, mergeMap, map } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';
import * as action from '../actions';
import api from 'src/api/api';

export const toResGetWallet: RootEpic = (action$) =>
  action$.pipe(
    filter(
      isActionOf([
        action.getWallet.request,
      ]),
    ),
    mergeMap(() =>
      from(api.get('/api/v1/wallets')).pipe(
        map(action.getWallet.success),
        catchError((e) => of(action.getWallet.failure(e))),
      ),
    ),
  );

export const toResGetWalletHistory: RootEpic = (action$) =>
  action$.pipe(
    filter(isActionOf(action.getWalletHistoryFilter.request)),
    mergeMap(({ payload }) =>
      from(
        api.get(
          `/api/v1/wallets/admin/history`,
          payload,
        ),
      ).pipe(
        map(action.getWalletHistoryFilter.success),
        catchError((e) => of(action.getWalletHistoryFilter.failure(e))),
      ),
    ),
  );

export default combineEpics(toResGetWallet, toResGetWalletHistory);
