import { RootState } from 'src/reducers';

export const userWallet = (state: RootState) => state.wallet?.wallet;
export const userWalletHistory = (state: RootState) =>
  state.wallet?.walletHistory;

export const isWalletError = (state: RootState) => state.wallet?.error?.wallet;
export const isWalletLoading = (state: RootState) =>
  state.wallet?.loading?.wallet;
export const isWalletSuccess = (state: RootState) =>
  state.wallet?.success?.wallet;

export const isWalletHistoryError = (state: RootState) =>
  state.wallet?.error?.walletHistory;
export const isWalletHistoryLoading = (state: RootState) =>
  state.wallet?.loading?.walletHistory;
export const isWalletHistorySuccess = (state: RootState) =>
  state.wallet?.success?.walletHistory;
