import { Action } from 'redux';
import { DateFilter, Pagination, PaginationPageOptions } from 'src/utils';

export type State = {
  loading: Loading,
  error: Error,
  success: Success,
  wallet: Wallet,
  walletHistory: WalletHistory,
};

export type Wallet = {
  amount: number;
  createdAt: Date;
  updatedAt: Date;
  userId: number;
  walletId: number;
}

export enum WalletReport {
  CREDIT = 'CREDIT',
  DEBIT = 'DEBIT',
}

export type WalletOverview = Omit<Wallet, 'userId'> & {
  description: string;
  type: WalletReport;
  walletHistoryId: number;
}

export type WalletHistory = Pagination & {
  content: WalletOverview[],
}

export type Loading = {
  wallet: boolean,
  walletHistory: boolean,
};

export type Error = {
  wallet: boolean,
  walletHistory: boolean,
};

export type Success = {
  wallet: boolean,
  walletHistory: boolean,
};

export type WalletHistoryPageOptions = Partial<DateFilter> & PaginationPageOptions;

export enum ATypes {
  DESTROY = 'DESTROY',

  REQ_ALL_WALLET = 'REQ_ALL_WALLET',
  RES_ALL_WALLET = 'RES_ALL_WALLET',
  ERR_ALL_WALLET = 'ERR_ALL_WALLET',

  REQ_WALLET_HISTORY_FILTER = 'REQ_WALLET_HISTORY_FILTER',
  RES_WALLET_HISTORY_FILTER = 'RES_WALLET_HISTORY_FILTER',
  ERR_WALLET_HISTORY_FILTER = 'ERR_WALLET_HISTORY_FILTER',
}

export interface IActionWallet extends Action {
  payload: any;
  type: ATypes;
}
