import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';

import constants from 'src/utils/constants';
import { getWithExpiry } from 'src/utils/setLocalStorageWithExpiry';

const initialization = (config: AxiosRequestConfig): AxiosInstance => {
  const axiosInstance = axios.create(config);
  axiosInstance.interceptors.request.use(config => {
    config.headers['Authorization'] = `${getWithExpiry(constants.token)}`;

    return config;
  });

  return axiosInstance;
};

export default initialization;
