import { AxiosRequestConfig } from 'axios';
import testConfig from '../../test-config.json';

export const baseURL =
  process.env.NODE_ENV === 'development'
    ? testConfig.dev_api
    : testConfig.prod_api;

export const axiosRequestConfiguration: AxiosRequestConfig = {
  baseURL,
  responseType: 'json',
  headers: {
    'Content-Type': 'application/json',
  },
};
