import * as React from 'react';
import { Collapse, CollapseProps } from 'antd';

const { Panel } = Collapse;
export interface AccordionProps extends CollapseProps {
  contents: {
    key?: string;
    header: string;
    body: string | JSX.Element;
  }[];
}

export const Accordion: React.FunctionComponent<AccordionProps> = ({
  contents,
  ...props
}) => (
  <Collapse bordered={false} expandIconPosition={'right'} {...props}>
    {contents.map(({ key, header, body }, index) => (
      <Panel header={header} key={key ?? index}>
        {body}
      </Panel>
    ))}
  </Collapse>
);

export default Accordion;
