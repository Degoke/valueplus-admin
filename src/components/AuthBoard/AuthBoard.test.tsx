import * as React from 'react';
import { AuthBoard, AuthBoardProps } from './AuthBoard';
import { shallow, ShallowWrapper } from 'enzyme';

describe('<AuthBoard /> ', () => {
  let defaultProps: AuthBoardProps;
  let component: () => ShallowWrapper;
  const curriedComponent = (props: AuthBoardProps) => () => shallow(<AuthBoard {...props} />);

  beforeEach(() => {
    defaultProps = {
      children: <div>lol</div>,
      backgroundimage: 'lol',
    };
    component = curriedComponent(defaultProps);
  });

  it('renders without crashing', () => {
    const wrapper = component();
    expect(wrapper.exists()).toEqual(true);
    expect(wrapper).toMatchSnapshot();
  });
});
