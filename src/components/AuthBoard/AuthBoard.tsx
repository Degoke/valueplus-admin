import { default as styles } from './AuthBoard.module.scss';
import * as React from 'react';

export interface AuthBoardProps {
  children: React.ReactNode;
  backgroundimage: string;
}
export const AuthBoard: React.FunctionComponent<AuthBoardProps> = ({
  children,
  backgroundimage,
}) => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>
        <div className={styles.content}>
          <div className={styles.mainContainer}>
            <a
              href={'https://valueplusweb.netlify.app'}
              className={styles.logoLink}
            >
              <img
                className={styles.logoImage}
                src={'/images/raw/ValuePlus_logo_.png'}
                alt="logo"
              />
            </a>
            {children}
          </div>
          <div className={styles.illustrationContainer}>
            <div
              className={styles.illustrationImage}
              style={{
                backgroundImage: `url(${backgroundimage})`,
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default AuthBoard;
