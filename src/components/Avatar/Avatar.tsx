import  { default as styles }  from './Avatar.module.scss';
import * as React from 'react';
import { Avatar } from 'antd';
import { AvatarProps } from 'antd/lib/avatar';

export interface CustomAvatarProps extends AvatarProps {
  firstName: string;
  lastName: string;
}

export const CustomAvatar: React.FunctionComponent<CustomAvatarProps> = ({ firstName,
  lastName, ...rest }) => (
  <Avatar
    className={styles.avatar}
    size="large"
    gap={4}
    {...rest}
  >
    {firstName.substring(0, 1)}{lastName.substring(0, 1)}
  </Avatar>
);

export default CustomAvatar;
