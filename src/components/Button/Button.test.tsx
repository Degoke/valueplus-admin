import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { Button, ButtonProps } from './Button';
import Spinner from 'src/components/Spinner';

describe('<Button />', () => {
  let defaultProps: ButtonProps;
  let component: () => ShallowWrapper;
  const curriedComponent = (props: ButtonProps) => () => shallow(<Button {...props} />);

  beforeEach(() => {
    defaultProps = {
      onClick: jest.fn(),
      size: 'normal',
      type: 'button',
      isLoading: false,
    };
    component = curriedComponent(defaultProps);
  });

  it('renders without crashing', () => {
    const wrapper = component();
    expect(wrapper.find('button').length).toBeGreaterThan(0);
    expect(wrapper).toMatchSnapshot();
  });

  it('shows spinner in button on loading', () => {
    const wrapper = component();
    wrapper.setProps({ isLoading: true });
    expect(wrapper.find(Spinner).length).toEqual(1);
    expect(wrapper).toMatchSnapshot();
  });

  it('simulates click', () => {
    const wrapper = component();
    wrapper.find('button').simulate('click');
    expect(defaultProps.onClick).toHaveBeenCalled();
  });
});
