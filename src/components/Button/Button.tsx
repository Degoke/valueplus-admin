import { default as styles } from './Button.module.scss';
import * as React from 'react';
import classnames from 'classnames';
import Spinner from 'src/components/Spinner';

export interface ButtonProps {
  disabled?: boolean;
  onClick?: (e: any) => void;
  className?: string;
  isLoading?: boolean;
  size?: 'normal' | 'small';
  type?: 'submit' | 'button';
  solid?: boolean;
}

export const Button: React.FunctionComponent<ButtonProps> = ({
  onClick,
  disabled,
  type,
  size,
  isLoading,
  className,
  children,
  solid,
  ...rest
}) => (
  <div className={classnames(className, styles.btn)} {...rest}>
    <button onClick={onClick} disabled={disabled} type={type}>
      <span style={{ display: 'flex', justifyContent: 'center' }}>
        {(isLoading && <Spinner />) || children}
      </span>
    </button>
  </div>
);

export default Button;
