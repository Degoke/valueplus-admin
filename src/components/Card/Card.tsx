import { default as styles } from './Card.module.scss';
import * as React from 'react';
import { Card } from 'antd';
import { CardProps } from 'antd/lib/card';
import classnames from 'classnames';

export interface CustomCardProps extends CardProps {
  width?: number;
  className?: string;
  title?: React.ReactNode;
  children: React.ReactNode;
}

const CustomCard: React.FunctionComponent<CustomCardProps> = ({
  width,
  children,
  className,
  ...rest
}) => (
  <Card
    className={classnames(className, styles.card)}
    style={{ width }}
    {...rest}
  >
    {children}
  </Card>
);

export default CustomCard;
