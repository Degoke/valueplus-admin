import { default as styles } from './Copy.module.scss';
import * as React from 'react';
import { CopyOutlined } from '@ant-design/icons';
import classnames from 'classnames';

export interface CopyProps {
  information: string;
  classname?: string;
}

const Copy: React.FunctionComponent<CopyProps> = ({ information, classname }) => (
  <div
    onClick={() => navigator.clipboard.writeText(information)}
    className={classnames(classname, styles.copy)}
  >
    <div>Copy</div>
    <CopyOutlined />
  </div>
);

export default Copy;
