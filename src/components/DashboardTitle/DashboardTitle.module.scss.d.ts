// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'subtitle': string;
  'title': string;
  'wrapper': string;
}
export const cssExports: CssExports;
export default cssExports;
