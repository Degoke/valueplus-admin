import { default as styles } from './DashboardTitle.module.scss';
import * as React from 'react';
import classnames from 'classnames';

export interface DashboardTitleProps {
  classname?: string;
  title?: string;
}

const DashboardTitle: React.FunctionComponent<DashboardTitleProps> = ({
  title,
  classname,
}) => (
  <div className={classnames(classname, styles.wrapper)}>
    <h5 className={styles.title}>{title}</h5>
  </div>
);

export default DashboardTitle;
