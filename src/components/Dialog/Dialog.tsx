import { default as styles } from './Dialog.module.scss';
import * as React from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import constants from 'src/utils/constants';

export interface DialogProps {
  /**
   * Pass true to mount Dialog
   */
  isOpen: boolean;
  /**
   * Handle clicks outside of dialog. Usually you would want to close dialog.
   */
  onClose: () => void;
  /**
   * Contents of the dialog
   */
  children: React.ReactNode | React.ReactNodeArray;
}

const Dialog: React.FunctionComponent<DialogProps> = ({
  isOpen,
  onClose,
  children,
}) => {
  React.useEffect(() => {
    if (isOpen) {
      addKeydownListener();
    }

    if (!isOpen) {
      removeKeydownListener();
    }
    return removeKeydownListener();
  }, [isOpen]);

  const handleKeyboardEvent = (e: KeyboardEvent | Partial<KeyboardEvent>) => {
    if (e.code === constants.ESC_KEY_CODE) {
      onClose();
    }
  };

  const removeKeydownListener = () => {
    document.removeEventListener('keydown', handleKeyboardEvent, false);
  };

  const addKeydownListener = () => {
    document.addEventListener('keydown', handleKeyboardEvent, false);
  };

  const preventTouchEventDefault = (e: React.TouchEvent<HTMLDivElement>) =>
    e.preventDefault();
  return (
    <AnimatePresence>
      {isOpen && (
        <motion.div
          key="backdrop"
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
          transition={{ duration: constants.ANIMATION_TIME }}
          className={styles.dialogBackdrop}
          tabIndex={0}
          onClick={onClose}
          onTouchMove={preventTouchEventDefault}
        />
      )}
      {isOpen && (
        <motion.div
          key="dialog"
          initial={{ opacity: 0 }}
          animate={{ translateY: [3000, -20, 10, -5, 0], opacity: 1 }}
          transition={{
            ease: [0.215, 0.61, 0.355, 1],
            duration: constants.ANIMATION_TIME,
            times: [0, 0.6, 0.7, 0.9, 1],
          }}
          exit={{
            scale: 0,
            opacity: 0,
          }}
          className={styles.dialog}
          tabIndex={-1}
          role="dialog"
        >
          {children}
        </motion.div>
      )}
    </AnimatePresence>
  );
};

export default Dialog;
