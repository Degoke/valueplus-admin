/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import { default as styles } from './ErrorBoundary.module.scss';
import React from 'react';

interface ErrorBoundaryProps {
  message?: React.ReactNode;
  description?: React.ReactNode;
}
class ErrorBoundary extends React.Component<
  ErrorBoundaryProps,
  {
    error?: Error | null;
    info: {
      componentStack?: string;
    };
  }
> {
  state: {
    error: undefined;
    hasErrored: boolean;
    info: {
      componentStack: string;
    };
  };

  static getDerivedStateFromError(_error: Error | null) {
    // process the error
    return { hasErrored: true };
  }

  componentDidCatch(error: Error | null, _info: object): void {
    console.log(error);
  }

  render() {
    if (this.state?.hasErrored) {
      return (
        <div className={styles.errorImageOverlay}>
          <div
            className={styles.errorImageContainer}
            style={{
              backgroundImage: "url('https://i.imgur.com/yW2W9SC.png')",
            }}
          />
          <div className={styles.errorImageText}>Sorry this page is broken</div>
        </div>
      );
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
