import { default as styles } from './ErrorPage.module.scss';
import * as React from 'react';
import Button from 'src/components/Button';
import { Link } from 'react-router-dom';
import { Result } from 'antd';
import { ResultProps } from 'antd/lib/result';
import { Routes } from 'src/routes';

export interface ErrorPageProps extends ResultProps {
  fallbackRoute: Routes;
}

const ErrorPage: React.FunctionComponent<ErrorPageProps> = ({
  fallbackRoute,
  ...rest
}) => (
  <Result
    status="404"
    title="404"
    subTitle="Sorry, the page you visited does not exist."
    extra={
      <Button className={styles.button}>
        <Link to={fallbackRoute ?? Routes.HOME}>Back Home</Link>
      </Button>
    }
    {...rest}
  />
);

export default ErrorPage;
