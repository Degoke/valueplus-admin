import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import ErrorState from 'src/components/ErrorState';

describe('<ErrorState />', () => {
  let defaultProps: any;
  let component: () => ShallowWrapper;
  const curriedComponent = (props: any) => () => shallow(<ErrorState {...props} />);

  beforeEach(() => {
    defaultProps = {
      children: 'lol',
    };
    component = curriedComponent(defaultProps);
  });

  it('renders without crashing', () => {
    const wrapper = component();
    expect(wrapper.find('div').length).toBeGreaterThan(0);
    expect(wrapper).toMatchSnapshot();
  });

  it('renders children', () => {
    const wrapper = component();
    wrapper.setProps({
      children: 'has error',
    });
    expect(wrapper.find('div').text()).toEqual('has error');
  });
});
