import { default as styles } from './ErrorState.module.scss';
import * as React from 'react';

const ErrorState: React.FunctionComponent = ({ children, ...props }) => (
  <small className={styles.error} {...props}>{children}</small>
);

export default ErrorState;
