import * as React from 'react';
import Card from 'src/components/Card';
import Accordion from 'src/components/Accordion';
import FAQS from './Faqs';

const FAQ: React.FunctionComponent = () => {
  return (
    <Card title={'Frequently Asked Questions'}>
      <Accordion contents={FAQS} />
    </Card>
  );
};

export default FAQ;
