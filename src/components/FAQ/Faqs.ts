const FAQS = [
  {
    header: 'What does Value Plus do?',
    body:
      'Value Plus is an exclusive network of marketing agents across Nigeria. Through carefully thought out strategies, we work as a team to ensure the growth and profitability of client-brands. Our agents solve the marketing challenges faced by businesses and ensure each business experiences massive growth and profitability. ',
  },
  {
    header: 'Is it a registered business? ',
    body: 'Value Plus is a legal and fully registered Business. ',
  },
  {
    header: 'What do Value Plus agents do?',
    body:
      '  Value Plus Agents are the creators of demand. We are excellent field marketing experts who are dedicated to helping clients boost their businesses and maximize profit by increasing sales through well-executed marketing strategies. We serve as a bridge between a brand and its customers.  ',
  },
  {
    header: 'Do I need special qualifications to become an Agent? ',
    body:
      'To become a Value Plus agent, one needs to be vibrant, hardworking, and goal-driven. However, having experience and relevant qualifications in Marketing and Sales Promotion and also a track record of conversion are added bonuses.',
  },
  {
    header: 'Do Value Plus agents get paid salaries? ',
    body:
      'Value Plus Agents get paid on a commission basis. Once orders are completed and verified by Value Plus the Agent receives his or her commission.  Earning with Value Plus is easy, the more work you put into completing your orders, the more results you get, the higher your total commissions! ',
  },
  {
    header: 'Why become a Value Plus Agent? ',
    body:
      'Value Plus Agents get paid on a commission basis. Once orders are completed and verified by Value Plus the Agent receives his or her commission.  Earning with Value Plus is easy, the more work you put into completing your orders, the more results you get, the higher your total commissions! ',
  },
];

export default FAQS;
