import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import { FormInput, FormInputProps } from './FormInput';

describe('<FormInput />', () => {
  let defaultProps: FormInputProps;
  let component: () => ShallowWrapper;
  const curriedComponent = (props: FormInputProps) => () => shallow(<FormInput {...props} />);

  beforeEach(() => {
    defaultProps = {
      className: '',
      onChange: jest.fn(),
      value: 'lol',
    };
    component = curriedComponent(defaultProps);
  });

  it('renders without crashing', () => {
    const wrapper = component();
    expect(wrapper.find('input').length).toBeGreaterThan(0);
    expect(wrapper).toMatchSnapshot();
  });

  it('should set classnames', () => {
    const wrapper = component();
    wrapper.setProps({ className: 'test' });
    expect(wrapper.hasClass('test')).toEqual(true);
  });
});
