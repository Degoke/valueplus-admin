import { default as styles } from './FormInput.module.scss';
import * as React from 'react';
import classnames from 'classnames';

export interface OwnProps {
  /** Custom classname on input */
  className?: string;
}
export type FormInputProps = OwnProps & React.InputHTMLAttributes<HTMLInputElement>
& React.DOMAttributes<HTMLInputElement>;

export const FormInput: React.FunctionComponent<FormInputProps> = ({
  className, ...props
}) => (
    <input className={classnames(styles.input, className)} {...props} />
);

export default FormInput;
