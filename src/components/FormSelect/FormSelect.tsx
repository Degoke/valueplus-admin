import { default as styles } from './FormSelect.module.scss';
import * as React from 'react';
import { Select } from 'antd';
import classnames from 'classnames';
import { SelectProps } from 'antd/lib/select';

export interface OwnProps extends SelectProps<OwnProps> {
  /** Custom classname on input */
  className?: string;
  placeholder?: string;
  children?: React.ReactNode;
  onChange?: (value: any) => void;
}
export type FormSelectProps = OwnProps &
  React.InputHTMLAttributes<HTMLInputElement> &
  React.DOMAttributes<HTMLInputElement>;

export const FormSelect: React.FunctionComponent<FormSelectProps> = ({
  className,
  placeholder,
  onChange,
  children,
  ...rest
}) => (
  <Select
    className={classnames(styles.input, className)}
    showSearch
    filterOption={true}
    placeholder={placeholder}
    optionFilterProp="children"
    onChange={onChange}
    onBlur={rest.onBlur}
    onSearch={rest.onSearch}
    defaultValue={rest.defaultValue as any}
    disabled={rest.disabled}
  >
    {children}
  </Select>
);

export default FormSelect;
