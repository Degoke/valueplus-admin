import { default as styles } from './OverviewCard.module.scss';
import * as React from 'react';
import classnames from 'classnames';
import Card from 'src/components/Card';
import { OverviewCardBackground, OverviewCardBackgroundImage } from './utils';
import { valueFormatter } from 'src/utils';

export interface OverviewCardProps {
  background?: OverviewCardBackground;
  title?: string;
  value?: number;
  prefixIcon?: JSX.Element;
  classname?: string;
}

const OverviewCard: React.FunctionComponent<OverviewCardProps> = ({
  background = OverviewCardBackground.LIGHT_GREEN,
  title,
  value,
  prefixIcon,
  classname,
}) => (
  <Card
    style={{
      backgroundImage: `url(${OverviewCardBackgroundImage.get(background)})`,
    }}
    className={classnames(classname, styles.card, {
      [styles.red]: background === 'red',
      [styles.blue]: background === 'blue',
      [styles.green]: background === 'green',
      [styles.lightGreen]: background === 'lightGreen',
    })}
  >
    <div className={styles.contentWrapper}>
      <p className={styles.title}>{title}</p>
      <div className={styles.value}>
        {prefixIcon}
        <p className={styles.subtitle}>{value && valueFormatter(value)}</p>
      </div>
    </div>
  </Card>
);

export default OverviewCard;
