export enum OverviewCardBackground {
  RED = 'red',
  BLUE = 'blue',
  GREEN = 'green',
  LIGHT_GREEN = 'lightGreen',
}

export const OverviewCardBackgroundImage = new Map<OverviewCardBackground, string>([
  [OverviewCardBackground.RED, '/images/svg/double-triangle.svg'],
  [OverviewCardBackground.BLUE, '/images/svg/double-circle.svg'],
  [OverviewCardBackground.GREEN, '/images/svg/double-rectangle.svg'],
  [OverviewCardBackground.LIGHT_GREEN, '/images/svg/double-arrow.svg'],
]);
