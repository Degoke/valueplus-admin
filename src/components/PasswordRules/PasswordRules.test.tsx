import * as React from 'react';
import { ReactWrapper, mount } from 'enzyme';
import { PasswordRulesProps, PasswordRules } from './PasswordRules';
import { default as styles } from './PasswordRules.module.scss';

describe('<PasswordRules/>', () => {
  let defaultProps: PasswordRulesProps;
  let component: () => ReactWrapper;
  const curriedComponent = (props: PasswordRulesProps) =>
    () => mount(<PasswordRules {...props} />);

  beforeEach(() => {
    defaultProps = {
      password: 'wrong',
      onValidation: jest.fn(),
      isTouched: true,
      rules: [{
        message: 'a',
        validation: () => true,
      }],
    };
    component = curriedComponent(defaultProps);
  });

  it('renders without crashing', () => {
    const wrapper = component();
    expect(wrapper.exists()).toBe(true);
    expect(wrapper).toMatchSnapshot();
  });
  describe('rule render', () => {
    it('should have rule valid classes', () => {
      const wrapper = component();
      expect(wrapper
        .find(`.${styles.rule}`)
        .hasClass(styles.invalid),
      ).toBe(false);
      expect(wrapper
        .find(`.${styles.rule}`)
        .hasClass(styles.valid),
      ).toBe(true);
    });
  });
});
