import * as React from 'react';
import classnames from 'classnames';
import { default as styles } from './PasswordRules.module.scss';
import { ruleList } from 'src/utils/PasswordUtils/utils';
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';

export interface PasswordRule {
  // Message of the rule
  message: string;
  // Validation function of the rule
  validation: (val: string) => boolean;
}

export interface PasswordStrength {
  // Password to test strength of
  password: string;
  // Callback triggered on validation
  onValidation: (isValid: boolean) => void;
}

export interface PasswordRulesProps extends PasswordStrength {
  // Password was touched
  isTouched: boolean;
  // Password validation rule list
  rules?: PasswordRule[];
}

const getRuleClasses = (isValid: boolean, isTouched: boolean): string =>
  classnames(
    styles.rule,
    { [styles.isTouched]: isTouched },
    { [styles.valid]: isValid },
    { [styles.invalid]: !isValid },
  );

export const PasswordRules: React.FunctionComponent<PasswordRulesProps> = ({
  password,
  onValidation,
  rules,
  isTouched,
}) => {
  const validateRules = (value: string) =>
    rules!.every((rule) => rule.validation(value));

  React.useEffect(() => {
    onValidation(validateRules(password));
  }, [password]);

  return (
    <div className={styles.rulesBlock}>
      <small className={styles.blockTitle}>Password requirements</small>
      <div>
        {rules!.map((rule) => {
          const isValid = rule.validation(password);

          return (
            <div
              key={rule.message}
              className={getRuleClasses(isValid, isTouched)}
            >
              {(isTouched &&
                (isValid ? (
                  <CheckCircleOutlined size={12} style={{ marginRight: 5 }} />
                ) : (
                  <CloseCircleOutlined size={12} style={{ marginRight: 5 }} />
                ))) || <div></div>}
              {rule.message}
            </div>
          );
        })}
      </div>
    </div>
  );
};

PasswordRules.defaultProps = {
  rules: ruleList,
};

export default PasswordRules;
