import { default as styles } from './ProfileCardHeader.module.scss';
import * as React from 'react';
import Button from 'src/components/Button';
import classnames from 'classnames';
import hideable from 'src/utils/hideable';

export interface ControlsProps {
  handleSubmit?: () => void;
  handleReset?: () => void;
  isUpdating: boolean;
  isLoading: boolean;
  setIsUpdating: React.Dispatch<React.SetStateAction<boolean>>;
}

const Controls: React.FunctionComponent<ControlsProps> = ({
  isLoading,
  isUpdating,
  setIsUpdating,
  handleReset,
  handleSubmit,
}) => (
  <div className={styles.titleToolbar}>
    <Button
      onClick={() => setIsUpdating(true)}
      className={classnames(styles.titleToolbarButton, {
        [styles.hide]: isUpdating,
      })}
    >
      Update
    </Button>
    <Button
      onClick={() => (setIsUpdating(false), handleReset())}
      className={classnames(styles.titleToolbarButton, {
        [styles.hide]: !isUpdating || isLoading,
      })}
    >
      Cancel
    </Button>
    <Button
      type="submit"
      isLoading={isLoading}
      onClick={handleSubmit}
      className={classnames(styles.titleToolbarButton, {
        [styles.hide]: !isUpdating,
      })}
    >
      Save
    </Button>
  </div>
);

export default hideable(Controls);
