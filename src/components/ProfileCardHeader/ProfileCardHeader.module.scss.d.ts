// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'hide': string;
  'titleToolbar': string;
  'titleToolbarButton': string;
  'titleWrapper': string;
}
export const cssExports: CssExports;
export default cssExports;
