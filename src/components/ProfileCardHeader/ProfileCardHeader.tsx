import { default as styles } from './ProfileCardHeader.module.scss';
import * as React from 'react';
import classnames from 'classnames';
import useIsMobile from '../../hooks/useIsMobile';
import { Alert } from 'antd';
import Controls from './Controls';

export interface ProfileCardHeaderProps {
  title?: string;
  handleSubmit?: () => void;
  handleReset?: () => void;
  isUpdating: boolean;
  isLoading: boolean;
  isButtonVisible: boolean;
  hasError?: string;
  isSuccess?: string;
  setIsUpdating: React.Dispatch<React.SetStateAction<boolean>>;
}

const ProfileCardHeader: React.FunctionComponent<ProfileCardHeaderProps> = ({
  title,
  isUpdating,
  hasError,
  isSuccess,
  isButtonVisible,
  ...rest
}) => {
  const isMobile = useIsMobile();
  return (
    <div className={styles.titleWrapper}>
      <div className={classnames({ [styles.hide]: isMobile && isUpdating })}>
        {title}
      </div>
      {!!hasError && (
        <Alert showIcon closable message={hasError} type="error" />
      )}
      {!!isSuccess && (
        <Alert showIcon closable message={isSuccess} type="success" />
      )}
      <Controls isVisible={isButtonVisible} isUpdating={isUpdating} {...rest} />
    </div>
  );
};

export default ProfileCardHeader;
