import { default as styles } from './ProfilePicture.module.scss';
import * as React from 'react';
import { Upload, Alert } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { RootState } from 'src/reducers/index';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import {
  uploadPicture,
  currentUser,
  isProfilePictureLoading,
  isProfilePictureSuccess,
  isProfilePictureError,
} from 'src/actions/profile';
import { getBase64, beforeUpload } from 'src/utils';
import { User } from 'src/actions/users/types';
import { UploadChangeParam } from 'antd/lib/upload';

export interface OwnProps {}
interface StateProps {
  hasErrored: boolean;
  IsSuccess: boolean;
  isLoading: boolean;
  user: User;
}

interface ActionProps {
  uploadImg: (img: string) => void;
}

export type ProfilePictureProps = OwnProps & StateProps & ActionProps;
const ProfilePicture: React.FunctionComponent<ProfilePictureProps> = ({
  user,
  uploadImg,
  isLoading,
  hasErrored,
  IsSuccess,
}) => {
  const [imageUrl, setImageUrl] = React.useState<string>('');

  React.useEffect(() => {
    setImageUrl(user.photo);
  }, [user]);

  const onChange = (info: UploadChangeParam) => {
    if (info.file.status === 'error') {
      getBase64(info.file.originFileObj, (imageUrl: string) => {
        return setImageUrl(imageUrl), uploadImg(imageUrl);
      });
    }
  };

  const uploadButton = (
    <div>
      {isLoading ? <LoadingOutlined /> : <PlusOutlined />}
      <div className="ant-upload-text">Upload</div>
    </div>
  );

  return (
    <div>
      <Upload
        name="avatar"
        listType="picture-card"
        showUploadList={false}
        beforeUpload={beforeUpload}
        onChange={onChange}
      >
        {imageUrl ? (
          <img src={imageUrl} alt="avatar" className={styles.image} />
        ) : (
          uploadButton
        )}
      </Upload>
      {IsSuccess && (
        <Alert
          showIcon
          closable
          message="Photo updated successfully!"
          type="success"
        />
      )}
      {hasErrored && (
        <Alert
          showIcon
          closable
          message="Error Uploading Image! Try again!"
          type="error"
        />
      )}
    </div>
  );
};

const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = (
  state,
) => ({
  user: currentUser(state),
  isLoading: isProfilePictureLoading(state),
  hasErrored: isProfilePictureError(state),
  IsSuccess: isProfilePictureSuccess(state),
});

const mapDispatchToProps: MapDispatchToProps<ActionProps, OwnProps> = (
  dispatch,
) => {
  return {
    uploadImg: (img: string) => dispatch(uploadPicture.request(img)),
  };
};

const connected = connect(mapStateToProps, mapDispatchToProps)(ProfilePicture);

export default connected;
