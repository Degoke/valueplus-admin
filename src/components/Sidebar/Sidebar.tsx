import { default as styles } from './Sidebar.module.scss';
import * as React from 'react';
import { connect, MapStateToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import { Layout, Menu } from 'antd';
import { Routes } from 'src/routes';
import { Link, useLocation, useRouteMatch } from 'react-router-dom';
import { sidebarMenu } from './SidebarRoutes';
import classnames from 'classnames';
import useIsMobile from 'src/hooks/useIsMobile';
import { intersection } from 'src/utils';

const { Sider } = Layout;

const BIG_LOGO = '/images/raw/dashboard-logo.png';
const SMALL_LOGO = '/images/raw/dashboard-logo-small.jpg';

interface StateProps {
  authorities: string[];
}

type TSidebar = StateProps;
const Sidebar: React.FunctionComponent<TSidebar> = ({ authorities }) => {
  const { url } = useRouteMatch();
  const urlLocation = useLocation();
  const isMobile = useIsMobile();

  const [collapse, setCollapse] = React.useState<boolean>(false);
  const [location, setLocation] = React.useState<string>(null);

  React.useEffect(() => {
    setLocation(urlLocation.pathname);
  }, [urlLocation]);

  return (
    <Sider
      className={styles.wrapper}
      breakpoint="lg"
      width={300}
      collapsedWidth={0}
      collapsed={collapse}
      onCollapse={setCollapse}
    >
      <div className={styles.logo}>
        <Link to={Routes.DASHBOARD}>
          <img src={collapse ? SMALL_LOGO : BIG_LOGO} alt="logo" />
        </Link>
      </div>
      <Menu
        theme="light"
        defaultSelectedKeys={[location]}
        selectedKeys={[location]}
      >
        {authorities &&
          sidebarMenu.map(({ title, link, icon, disabled, access }) => {
            const hasAccess =
              intersection(authorities, access).length !== 0 ||
              access.includes('ALL');
            return hasAccess ? (
              <Menu.Item
                disabled={disabled}
                className={classnames(styles.menuItem, {
                  [styles.active]: `${url}${link}` === location,
                })}
                key={`${url}${link}`}
                icon={icon}
                onClick={() => setCollapse(isMobile ?? true)}
              >
                <Link to={`${url}${link}`}>{title}</Link>
              </Menu.Item>
            ) : null;
          })}
      </Menu>
    </Sider>
  );
};

const mapStateToProps: MapStateToProps<StateProps, {}, RootState> = ({
  profile,
}) => ({
  authorities: profile?.user?.authorities,
});

const connected = connect(mapStateToProps)(Sidebar);

export default connected;
