import * as React from 'react';
import { Routes } from 'src/routes';
import {
  HomeOutlined,
  UserOutlined,
  CrownOutlined,
  QrcodeOutlined,
  SettingOutlined,
  UserAddOutlined,
  LineChartOutlined,
  FieldTimeOutlined,
  UsergroupAddOutlined,
  UnorderedListOutlined,
} from '@ant-design/icons';

type Menu = {
  link: Routes;
  title: string;
  icon: JSX.Element;
  disabled?: boolean;
  access?: string[];
};

export const sidebarMenu: Menu[] = [
  {
    link: Routes.EMPTY,
    title: 'Home',
    icon: <HomeOutlined />,
    access: ['ALL'],
  },
  {
    link: Routes.TRANSACTIONS,
    title: 'Transactions',
    icon: <LineChartOutlined />,
    access: ['VIEW_ALL_TRANSACTIONS'],
  },
  {
    link: Routes.WALLETS,
    title: 'Wallets',
    icon: <FieldTimeOutlined />,
    access: ['VIEW_ADMIN_WALLET_HISTORY'],
  },
  {
    link: Routes.MANAGE_ORDERS,
    title: 'Manage Orders',
    icon: <FieldTimeOutlined />,
    access: ['ALL'],
  },
  {
    link: Routes.MANAGE_PRODUCTS,
    title: 'Manage Products',
    icon: <QrcodeOutlined />,
    access: ['ALL'],
  },
  {
    link: Routes.MANAGE_USERS,
    title: 'Manage Users',
    icon: <UsergroupAddOutlined />,
    access: ['VIEW_ALL_USERS'],
  },
  {
    link: Routes.COMMISSIONS,
    title: 'Commissions',
    icon: <SettingOutlined />,
    access: ['ALL'],
  },
  {
    link: Routes.ACTIVITY_LOG,
    title: 'Activity Log',
    icon: <UnorderedListOutlined />,
    access: ['VIEW_AUDIT_LOG'],
  },
  {
    link: Routes.CREATE_ADMIN,
    title: 'Sub Admins',
    icon: <UserAddOutlined />,
    access: ['ALL'],
  },
  {
    link: Routes.SUPERAGENTS,
    title: 'Super Agents',
    icon: <CrownOutlined />,
    access: ['ALL'],
  },
  {
    link: Routes.PROFILE,
    title: 'Profile',
    icon: <UserOutlined />,
    access: ['ALL'],
  },
];
