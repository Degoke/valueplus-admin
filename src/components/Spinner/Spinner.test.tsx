import * as React from 'react';
import { shallow } from 'enzyme';
import Spinner from './index';

describe('<Spinner />', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<Spinner />);
    expect(wrapper.find('div').length).toBeGreaterThan(0);
    expect(wrapper).toMatchSnapshot();
  });
});
