import * as React from 'react';
import { LoadingOutlined } from '@ant-design/icons';

export interface SpinnerProps {
  className?: string;
}
export const Spinner: React.FunctionComponent<SpinnerProps> = ({
  className,
  ...rest
}) => <LoadingOutlined className={className} {...rest} />;

export default Spinner;
