import * as React from 'react';
import { Button } from 'antd';
import { CloseCircleTwoTone } from '@ant-design/icons';
import hideable from 'src/utils/hideable';

interface OwnProps {
  onClick: () => void;
}
const TableDisableButton: React.FunctionComponent<OwnProps> = ({ onClick }) => (
  <Button
    danger
    icon={<CloseCircleTwoTone twoToneColor="#D47B6E" />}
    onClick={onClick}
  >
    Disable
  </Button>
);

export default hideable(TableDisableButton);
