import * as React from 'react';
import { Button } from 'antd';
import { CheckCircleTwoTone } from '@ant-design/icons';
import hideable from 'src/utils/hideable';

interface OwnProps {
  onClick: () => void;
}
const TableEnableButton: React.FunctionComponent<OwnProps> = ({ onClick }) => (
  <Button
    icon={<CheckCircleTwoTone twoToneColor="#D47B6E" />}
    onClick={onClick}
  >
    Enable
  </Button>
);

export default hideable(TableEnableButton);
