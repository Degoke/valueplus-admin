import * as React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import Title, { TitleProps } from 'src/components/Title';

describe('<Title />', () => {
  let defaultProps: TitleProps;
  let component: () => ShallowWrapper;
  const curriedComponent = (props: TitleProps) => () => shallow(<Title {...props} />);

  beforeEach(() => {
    defaultProps = {
      classname: 'title',
      children: 'lol',
    };
    component = curriedComponent(defaultProps);
  });

  it('renders without crashing', () => {
    const wrapper = component();
    expect(wrapper.find('h1').length).toBeGreaterThan(0);
    expect(wrapper.hasClass('title')).toEqual(true);
    expect(wrapper).toMatchSnapshot();
  });

  it('renders children', () => {
    const wrapper = component();
    wrapper.setProps({
      children: <div className="test-class">Title with sauce</div>,
    });
    expect(wrapper.find('.test-class').first().text()).toEqual('Title with sauce');
  });
});
