import { default as styles } from './Title.module.scss';
import * as React from 'react';
import classnames from 'classnames';

export interface TitleProps {
  classname?: string;
  children?: string;
}

const Title: React.FunctionComponent<TitleProps> = ({ classname, children }) => (
  <h1 className={classnames(classname, styles.title)}>{children}</h1>
);

export default Title;
