import { default as styles } from './WhatsappBadge.module.scss';
import * as React from 'react';

export interface WhatsappBadgeProps {
  phone?: number;
  text?: string;
}

const WhatsappBadge: React.FunctionComponent<WhatsappBadgeProps> = ({
  text,
  phone,
}) => (
  <a
    target="_blank"
    href={'https://wa.me/' + phone + '?text=' + text ?? 'Hello There'}
  >
    <div
      className={styles.badge}
      style={{ backgroundImage: 'url(/images/raw/whatsapp.png)' }}
    />
  </a>
);

export default WhatsappBadge;
