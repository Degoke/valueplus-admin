import { default as styles } from './ActivityLog.module.scss';
import * as React from 'react';
import { RootState } from 'src/reducers/index';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { Row, Col, Select, DatePicker } from 'antd';
import Button from 'src/components/Button';
import moment from 'moment';
import { getAudits, SEntities } from 'src/actions/audit';
import { AuditRequestPayload, Entities } from 'src/actions/audit/types';
import constants from 'src/utils/constants';
const { RangePicker } = DatePicker;
const { Option } = Select;

interface ActionProps {
  getAudits: (options: AuditRequestPayload) => void;
}

interface StateProps {
  entities: Entities;
}

export type FilterProps = ActionProps & StateProps;

const Filter: React.FunctionComponent<FilterProps> = ({
  entities,
  getAudits,
}) => {
  const [dates, setDates] = React.useState<[moment.Moment, moment.Moment]>(
    null,
  );
  const [entityType, setEntityType] = React.useState<keyof Entities>(null);
  const [actionType, setActionType] = React.useState<string>(null);

  const disabledDate = (current: moment.Moment) => {
    if (!dates) {
      return false;
    }
    const tooLate = dates[0] && current.diff(dates[0], 'days') > 14;
    const tooEarly = dates[1] && dates[1].diff(current, 'days') > 14;
    return tooEarly || tooLate;
  };

  const handleSearch = () => {
    getAudits({
      size: constants.TRANSACTION_PAGE_SIZE,
      startDate: dates[0]?.format('YYYY-MM-DD'),
      endDate: dates[1]?.format('YYYY-MM-DD'),
      action: actionType,
      entityType,
    });
  };

  const showSearch = dates || entityType || actionType;
  return (
    <Row gutter={24} justify="center">
      <Col xs={24} xl={6} className={styles.gutter}>
        <div className={styles.title}>Module</div>
        <Select onChange={(value) => setEntityType(value as keyof Entities)}>
          {Object.keys(entities ?? []).map((entity) => (
            <Option value={entity} key={entity}>
              {entity}
            </Option>
          ))}
        </Select>
      </Col>
      <Col xs={24} xl={6} className={styles.gutter}>
        <div className={styles.title}>Action</div>
        <Select onChange={(value) => setActionType(value as string)}>
          {entities?.[entityType]?.map((value) => (
            <Option value={value} key={value}>
              {value}
            </Option>
          ))}
        </Select>
      </Col>
      <Col xs={24} xl={6} className={styles.gutter}>
        <div className={styles.title}>Filter by Date</div>
        <RangePicker
          value={dates}
          disabledDate={disabledDate}
          onChange={(dates) => setDates(dates)}
        />
      </Col>
      <Col
        xs={8}
        xl={2}
        className={styles.gutter}
        style={{ justifyContent: 'center', marginTop: 15 }}
      >
        {showSearch && <Button onClick={handleSearch}>Search</Button>}
      </Col>
    </Row>
  );
};

const mapStateToProps: MapStateToProps<StateProps, {}, RootState> = (
  state,
) => ({
  entities: SEntities(state),
});

const mapDispatchToProps: MapDispatchToProps<ActionProps, {}> = (dispatch) => ({
  getAudits: (options) => dispatch(getAudits.request(options)),
});

const connected = connect(mapStateToProps, mapDispatchToProps)(Filter);

export default connected;
