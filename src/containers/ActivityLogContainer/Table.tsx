import * as React from 'react';
import format from 'date-fns/format';
import { RootState } from 'src/reducers/index';
import { connect, MapStateToProps } from 'react-redux';
import { Row, Col, Avatar, Timeline, Empty } from 'antd';
import { SAudits, SAuditsIsLoading } from 'src/actions/audit';
import { Audits } from 'src/actions/audit/types';
import { LoadingOutlined } from '@ant-design/icons';

export interface StateProps {
  isLoading: boolean;
  audits: Audits;
}

type TLogTable = StateProps;

const LogTable: React.FunctionComponent<TLogTable> = ({
  audits,
  isLoading,
}) => (
  <Row gutter={24} justify="center" style={{ marginTop: 30 }}>
    <Col xs={24} offset={2}>
      {isLoading ? (
        <LoadingOutlined />
      ) : (
        <Timeline>
          {audits?.content.length === 0 ? (
            <Empty />
          ) : (
            audits?.content.map((item, index) => (
              <Timeline.Item key={index}>
                <Avatar src={item?.actor?.photo ?? ''} />
                {'   '}
                <span>{item?.description}</span> by{' '}
                <span>{item?.actor?.email}</span> at{' '}
                <span>{format(new Date(item?.createdAt), 'Pp')}</span>
              </Timeline.Item>
            ))
          )}
        </Timeline>
      )}
    </Col>
  </Row>
);

const mapStateToProps: MapStateToProps<StateProps, {}, RootState> = (
  state,
) => ({
  audits: SAudits(state),
  isLoading: SAuditsIsLoading(state),
});

const connected = connect(mapStateToProps)(LogTable);

export default connected;
