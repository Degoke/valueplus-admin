import { Audit } from 'src/actions/audit/types';
import format from 'date-fns/format';

export const ColumnsKey = [
  {
    title: 'Name',
    dataIndex: 'actor',
    key: 'actor',
    render: (_value: string, record: Audit) =>
      `${record?.actor.firstname} ${record?.actor.lastname}`,
  },
  {
    title: 'Date',
    dataIndex: 'createdAt',
    key: 'createdAt',
    render: (date: Date) => format(new Date(date), 'Pp'),
  },
];
