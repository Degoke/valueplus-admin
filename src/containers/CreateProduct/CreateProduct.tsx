import * as React from 'react';
import { connect, MapStateToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import { Link } from 'react-router-dom';
import { Routes } from 'src/routes';
import Card from 'src/components/Card';
import Button from 'src/components/Button';
import { hasAuthority } from 'src/actions/profile';
import hideable from 'src/utils/hideable';

const CreateProduct: React.FunctionComponent = () => (
  <Card title={'Create Product'} style={{ marginBottom: 20 }}>
    <Link to={`${Routes.DASHBOARD}${Routes.MANAGE_PRODUCTS_CREATE}`}>
      <Button>Add products</Button>
    </Link>
  </Card>
);

const mapStateToProps: MapStateToProps<{}, {}, RootState> = (state) => ({
  isVisible: hasAuthority('CREATE_PRODUCT')(state),
});

const connected = connect(mapStateToProps, null)(hideable(CreateProduct));

export default connected;
