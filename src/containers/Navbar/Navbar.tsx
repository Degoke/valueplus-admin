import { default as styles } from './Navbar.module.scss';
import * as React from 'react';
import { connect, MapStateToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import { Link, useRouteMatch } from 'react-router-dom';
import { Routes } from 'src/routes';
import { Layout } from 'antd';
import { LogoutOutlined } from '@ant-design/icons';
import Avatar from 'src/components/Avatar';
import { User } from 'src/actions/users/types';

const { Header } = Layout;

interface OwnProps {
  user: User;
}

interface StateProps {
  isLoading: boolean;
}

export type NavbarProps = OwnProps & StateProps;
const Navbar: React.FunctionComponent<NavbarProps> = ({ user }) => {
  const { url } = useRouteMatch();
  return (
    <div className={styles.header}>
      <Header className={styles.header}>
        <div className={styles.container}>
          <div className={styles.profile}>
            <Link to={`${url}${Routes.PROFILE}`}>
              <Avatar
                size={'large'}
                gap={3}
                src={user?.photo}
                firstName={user?.firstname ?? ''}
                lastName={user?.lastname ?? ''}
              />
            </Link>
            <div className={styles.title}>
              {user?.firstname.toUpperCase()} {user?.lastname.toUpperCase()}
            </div>
          </div>
          <Link to={Routes.LOGOUT} className={styles.profile}>
            <LogoutOutlined className={styles.logout} />
            <div className={styles.title}>Logout</div>
          </Link>
        </div>
      </Header>
    </div>
  );
};
const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = ({
  auth,
}) => ({
  isLoading: auth.isLoading,
});

const connected = connect(mapStateToProps, null)(Navbar);

export default connected;
