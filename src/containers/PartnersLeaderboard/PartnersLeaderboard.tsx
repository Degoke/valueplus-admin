import * as React from 'react';
import Card from 'src/components/Card';
import { Table } from 'antd';
import { TableProps } from 'antd/lib/table';
import { Transaction } from 'src/actions/transactions/types';
import { columnsKey } from './constants';

export interface PartnersLeaderboardProps extends TableProps<any> {
  transactions: Transaction[];
}

const PartnersLeaderboard: React.FunctionComponent<PartnersLeaderboardProps> = ({
  transactions,
  ...rest
}) => {
  return (
    <Card title={`Partner's Leaderboard`}>
      <Table
        columns={columnsKey}
        scroll={{ x: true }}
        dataSource={transactions?.map((transaction) => ({
          ...transaction,
          key: transaction.id,
        }))}
        {...rest}
        pagination={{
          position: ['bottomLeft'],
          hideOnSinglePage: true,
          ...rest.pagination,
        }}
      />
    </Card>
  );
};

export default PartnersLeaderboard;
