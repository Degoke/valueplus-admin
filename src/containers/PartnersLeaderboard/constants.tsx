import * as React from 'react';
import { Tag } from 'antd';
import format from 'date-fns/format';
import { Status, StatusColor } from 'src/utils';

export const columnsKey = [
  {
    title: 'Date',
    dataIndex: 'createdAt',
    key: 'createdAt',
    render: (date: Date) => format(new Date(date), 'PPpp'),
  },
  {
    title: 'Amount',
    dataIndex: 'amount',
    key: 'amount',
    render: (amount: number) => <div>NGN {amount.toFixed(2)}</div>,
  },
  {
    title: 'Account number',
    dataIndex: 'accountNumber',
    key: 'accountNumber',
  },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
    render: (status: Status) => (
      <Tag color={StatusColor.get(status)} key={status}>
        {status.toUpperCase()}
      </Tag>
    ),
  },
];
