import * as React from 'react';
import { connect, MapDispatchToProps } from 'react-redux';
import { Row, Col } from 'antd';
import Card from 'src/components/Card';
import FormInput from 'src/components/FormInput';
import { UpdatePassword } from 'src/actions/profile/types';
import { updatePassword } from 'src/actions/profile/actions';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import ProfileCardHeader from 'src/components/ProfileCardHeader';
import ErrorState from 'src/components/ErrorState';

interface OwnProps {
  isLoading: boolean;
}

interface ActionProps {
  updatePassword: (crendential: UpdatePassword) => void;
}

const validationSchema = Yup.object({
  oldPassword: Yup.string().required('Provide current password'),
  newPassword: Yup.string()
    .required('No password provided.')
    .min(8, 'Password is too short - should be 8 chars minimum.'),
  confirmPassword: Yup.string().oneOf(
    [Yup.ref('newPassword'), null],
    'Passwords must match',
  ),
});

export type PasswordChangeProps = OwnProps & ActionProps;
const PasswordChange: React.FunctionComponent<PasswordChangeProps> = ({
  updatePassword,
  isLoading,
}) => {
  const [isUpdating, setIsUpdating] = React.useState<boolean>(false);

  const formik = useFormik({
    validationSchema,
    initialValues: {
      oldPassword: '',
      newPassword: '',
      confirmPassword: '',
    },
    onSubmit: (values, { resetForm }) => {
      const credentials: UpdatePassword = {
        oldPassword: values.oldPassword,
        newPassword: values.newPassword,
      };
      updatePassword(credentials);
      resetForm();
      setIsUpdating(false);
    },
  });
  const {
    errors,
    handleSubmit,
    handleChange,
    touched,
    values,
    resetForm,
  } = formik;
  const { oldPassword, newPassword, confirmPassword } = values;

  return (
    <Card
      title={
        <ProfileCardHeader
          isUpdating={isUpdating}
          isLoading={isLoading}
          setIsUpdating={setIsUpdating}
          title={'Update Password'}
          handleSubmit={handleSubmit}
          handleReset={resetForm}
          isButtonVisible
        />
      }
    >
      <Row gutter={24}>
        <Col xs={24}>
          <form onSubmit={handleSubmit}>
            <Row gutter={24}>
              <Col xs={24} sm={24} md={12} xl={8}>
                <FormInput
                  type={'password'}
                  value={oldPassword}
                  onChange={handleChange}
                  name="oldPassword"
                  placeholder="Current Password"
                  disabled={!isUpdating || isLoading}
                />
                {errors.oldPassword && touched.oldPassword && (
                  <ErrorState>{errors.oldPassword}</ErrorState>
                )}
              </Col>
              <Col xs={24} sm={24} md={12} xl={8}>
                <FormInput
                  type={'password'}
                  value={newPassword}
                  onChange={handleChange}
                  name="newPassword"
                  placeholder="New Password"
                  disabled={!isUpdating || isLoading}
                />
                {errors.newPassword && touched.newPassword && (
                  <ErrorState>{errors.newPassword}</ErrorState>
                )}
              </Col>
              <Col xs={24} sm={24} md={12} xl={8}>
                <FormInput
                  type={'password'}
                  value={confirmPassword}
                  onChange={handleChange}
                  name="confirmPassword"
                  placeholder="Confirm Password"
                  disabled={!isUpdating || isLoading}
                />
                {errors.confirmPassword && touched.confirmPassword && (
                  <ErrorState>{errors.confirmPassword}</ErrorState>
                )}
              </Col>
            </Row>
          </form>
        </Col>
      </Row>
    </Card>
  );
};

const mapDispatchToProps: MapDispatchToProps<ActionProps, OwnProps> = (
  dispatch,
) => {
  return {
    updatePassword: (crendential: UpdatePassword) =>
      dispatch(updatePassword.request(crendential)),
  };
};

const connected = connect(null, mapDispatchToProps)(PasswordChange);

export default connected;
