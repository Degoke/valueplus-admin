import * as React from 'react';
import Card from 'src/components/Card';
import { Table } from 'antd';
import { TableProps } from 'antd/lib/table';
import { Transaction } from 'src/actions/transactions/types';
import { columnsKey } from './constants';

export interface PaymentHistoryProps extends TableProps<any> {
  transactions: Transaction[];
  refreshTransaction?: (key: string) => void;
}

const PaymentHistory: React.FunctionComponent<PaymentHistoryProps> = ({
  transactions,
  refreshTransaction,
  ...rest
}) => (
  <Card title={'Recent Transactions'}>
    <Table
      columns={columnsKey(refreshTransaction)}
      scroll={{ x: true }}
      dataSource={transactions?.map((transaction) => ({
        ...transaction,
        key: transaction.id,
      }))}
      {...rest}
      pagination={{
        position: ['bottomLeft'],
        hideOnSinglePage: true,
        ...rest.pagination,
      }}
    />
  </Card>
);

export default PaymentHistory;
