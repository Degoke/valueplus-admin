import * as React from 'react';
import { connect, MapStateToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import { Button } from 'antd';
import { TransactionStatus } from 'src/utils';
import { hasAuthority } from 'src/actions/profile';
import hideable from 'src/utils/hideable';

interface OwnProps {
  onClick: () => void;
  status: TransactionStatus;
}
const RefreshTransaction: React.FunctionComponent<OwnProps> = ({
  onClick,
  status,
}) => (
  <div style={{ textAlign: 'center' }} onClick={onClick}>
    <Button type="link">
      {status !== TransactionStatus.SUCCESS && 'Refresh'}
    </Button>
  </div>
);

const mapStateToProps: MapStateToProps<{}, {}, RootState> = (state) => ({
  isVisible: hasAuthority('VERIFY_PENDING_TRANSACTIONS')(state),
});

const connected = connect(mapStateToProps, null)(hideable(RefreshTransaction));

export default connected;
