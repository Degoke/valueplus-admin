import * as React from 'react';
import { Tag } from 'antd';
import format from 'date-fns/format';
import { TransactionStatus, TransactionStatusColor } from 'src/utils';
import { Transaction } from 'src/actions/transactions/types';
import RefreshTransaction from './RefreshTransaction';

export const columnsKey = (refreshTransaction: (key: string) => void) => [
  {
    title: 'Date',
    dataIndex: 'createdAt',
    key: 'createdAt',
    render: (date: Date) => format(new Date(date), 'PPpp'),
  },
  {
    title: 'Amount',
    dataIndex: 'amount',
    key: 'amount',
    render: (amount: number) => <div>NGN {amount.toFixed(2)}</div>,
  },
  {
    title: 'Account number',
    dataIndex: 'accountNumber',
    key: 'accountNumber',
  },
  {
    title: 'Status',
    dataIndex: 'paystackStatus',
    key: 'paystackStatus',
    render: (status: TransactionStatus) => (
      <Tag color={TransactionStatusColor.get(status)} key={status}>
        {status.toUpperCase()}
      </Tag>
    ),
  },
  {
    title: '',
    key: 'action',
    render: (record: Transaction) => (
      <RefreshTransaction
        status={record.paystackStatus}
        onClick={() => refreshTransaction(record.reference)}
      />
    ),
  },
];
