import * as React from 'react';
import { Row, Col } from 'antd';
import { connect, MapDispatchToProps } from 'react-redux';
import Card from 'src/components/Card';
import FormInput from 'src/components/FormInput';
import ProfilePicture from 'src/components/ProfilePicture';
import { updateProfile } from 'src/actions/profile/actions';
import { useFormik } from 'formik';
import ProfileCardHeader from 'src/components/ProfileCardHeader';
import { User } from 'src/actions/users/types';

interface OwnProps {
  isLoading: boolean;
  user: User;
}

interface ActionProps {
  updateProfile: (crendential: User) => void;
}
export type PersonalDetailProps = OwnProps & ActionProps;

const PersonalDetails: React.FunctionComponent<PersonalDetailProps> = ({
  user,
  updateProfile,
  isLoading,
}) => {
  const [isUpdating, setIsUpdating] = React.useState<boolean>(false);

  const formik = useFormik({
    initialValues: {
      email: user.email ?? '',
      firstname: user.firstname ?? '',
      lastname: user.lastname ?? '',
      phone: user.phone ?? '',
      address: user.address ?? '',
    },
    onSubmit: (values) => {
      updateProfile({ ...user, ...values });
      setIsUpdating(false);
    },
  });

  const { handleSubmit, handleChange, values, resetForm } = formik;
  const { email, firstname, lastname, phone, address } = values;

  return (
    <Card
      title={
        <ProfileCardHeader
          isUpdating={isUpdating}
          isLoading={isLoading}
          setIsUpdating={setIsUpdating}
          title={'Personal Details'}
          handleSubmit={handleSubmit}
          handleReset={resetForm}
          isButtonVisible
        />
      }
    >
      <Row gutter={24}>
        <Col xs={24} sm={24} md={6} xl={6}>
          <ProfilePicture />
        </Col>
        <Col xs={24} sm={24} md={18} xl={18}>
          <form onSubmit={handleSubmit}>
            <Row gutter={24}>
              <Col xs={24} sm={24} md={12} xl={8}>
                <FormInput
                  name={'firstname'}
                  value={firstname}
                  onChange={handleChange}
                  placeholder="First Name"
                  disabled={true}
                />
              </Col>
              <Col xs={24} sm={24} md={12} xl={8}>
                <FormInput
                  name={'lastname'}
                  value={lastname}
                  onChange={handleChange}
                  placeholder="Last Name"
                  disabled={true}
                />
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={24} sm={24} md={12} xl={8}>
                <FormInput
                  name={'email'}
                  value={email}
                  onChange={handleChange}
                  placeholder="Email"
                  disabled={true}
                />
              </Col>
              <Col xs={24} sm={24} md={12} xl={8}>
                <FormInput
                  name={'phone'}
                  value={phone}
                  onChange={handleChange}
                  placeholder="Mobile Number"
                  disabled={!isUpdating || isLoading}
                />
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={24} sm={24} md={12} xl={8}>
                <FormInput
                  name={'address'}
                  value={address}
                  onChange={handleChange}
                  placeholder="Address"
                  disabled={!isUpdating || isLoading}
                />
              </Col>
            </Row>
          </form>
        </Col>
      </Row>
    </Card>
  );
};

const mapDispatchToProps: MapDispatchToProps<ActionProps, OwnProps> = (
  dispatch,
) => {
  return {
    updateProfile: (crendential) =>
      dispatch(updateProfile.request(crendential)),
  };
};

const connected = connect(null, mapDispatchToProps)(PersonalDetails);

export default connected;
