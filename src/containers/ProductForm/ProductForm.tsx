import * as React from 'react';
import { Button as AntButton, Row, Col, Upload } from 'antd';
import { IProduct } from 'src/actions/products/types';
import FormInput from 'src/components/FormInput';
import { useFormik } from 'formik';
import TextArea from 'antd/lib/input/TextArea';
import Button from 'src/components/Button';
import { getBase64, beforeUpload } from 'src/utils';
import { UploadOutlined } from '@ant-design/icons';

export interface OwnProps {
  isLoading: boolean;
  buttonMsg: React.ReactNode;
  product?: IProduct;
  handleFormSubmit: (product: IProduct) => void;
}
const ProductForm: React.FunctionComponent<OwnProps> = ({
  product,
  buttonMsg,
  isLoading,
  handleFormSubmit,
}) => {
  const [image, setImage] = React.useState<string>(product?.image);
  const onChange = (info: any) => {
    if (info.file.status === 'error') {
      getBase64(info.file.originFileObj, (imageUrl: string) => {
        return setImage(imageUrl);
      });
    }
  };
  const formik = useFormik({
    initialValues: {
      name: product?.name,
      description: product?.description,
      price: product?.price,
    },
    onSubmit: (values) => {
      handleFormSubmit({ ...product, image, ...values });
    },
  });

  const { handleSubmit, handleChange, values } = formik;
  const { name, description, price } = values;

  return (
    <Row gutter={24} justify={'center'} align={'middle'}>
      <Col xs={24} sm={24}>
        <form onSubmit={handleSubmit}>
          <Row>
            <Col xs={24} sm={24} style={{ marginBottom: 20 }}>
              <Upload
                showUploadList={false}
                beforeUpload={beforeUpload}
                onChange={onChange}
              >
                <AntButton icon={<UploadOutlined />}>
                  Upload product image
                </AntButton>
              </Upload>

              <img src={image} width="50" height="50" />
            </Col>
            <Col xs={24} sm={24}>
              <FormInput
                name={'name'}
                value={name}
                onChange={handleChange}
                placeholder="Product Name"
              />
            </Col>
            <Col xs={24} sm={24}>
              <TextArea
                style={{ marginBottom: 20 }}
                autoSize={{ minRows: 2, maxRows: 2 }}
                name={'description'}
                value={description}
                onChange={handleChange}
                placeholder="Description"
              />
            </Col>
            <Col xs={24} sm={24}>
              <FormInput
                name={'price'}
                value={price}
                onChange={handleChange}
                placeholder="Price"
              />
            </Col>
          </Row>
          <Button type={'submit'} isLoading={isLoading}>
            {buttonMsg}
          </Button>
        </form>
      </Col>
    </Row>
  );
};

export default ProductForm;
