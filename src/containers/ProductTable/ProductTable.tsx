import { default as styles } from './ProductTable.module.scss';
import * as React from 'react';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import {
  getProducts,
  enableProduct,
  disableProduct,
} from 'src/actions/products';
import { Row, Col, Table } from 'antd';
import { columnsKey } from './constants';
import * as types from 'src/actions/products/types';
import { hasAuthority } from 'src/actions/profile';

export interface OwnProps {
  handleProductEdit: (id: number) => void;
}

export interface ActionProps {
  getProducts: (options: types.ProductOrdersOptions) => void;
  enableProduct: (id: types.IProduct['id']) => void;
  disableProduct: (id: types.IProduct['id']) => void;
}

export interface StateProps {
  isEnableButtonVisible: boolean;
  isDisableButtonVisible: boolean;
  isLoading: boolean;
  products: types.Products;
}

export type ProductTableProps = OwnProps & StateProps & ActionProps;

const ProductTable: React.FunctionComponent<ProductTableProps> = ({
  products,
  isLoading,
  getProducts,
  enableProduct,
  disableProduct,
  isEnableButtonVisible,
  isDisableButtonVisible,
  handleProductEdit,
}) => {
  return (
    <Row gutter={24} justify="center" className={styles.wrapper}>
      <Col xs={24}>
        <Table
          columns={columnsKey(
            isEnableButtonVisible,
            isDisableButtonVisible,
            enableProduct,
            disableProduct,
            handleProductEdit,
          )}
          scroll={{ x: true }}
          dataSource={products?.content?.map((product) => ({
            ...product,
            key: product.id,
          }))}
          loading={isLoading}
          onChange={(page) =>
            getProducts({
              size: page.pageSize,
              page: page.current - 1,
            })
          }
          pagination={{
            position: ['bottomLeft'],
            hideOnSinglePage: true,
            showSizeChanger: true,
            total: products?.totalElements,
            current: products?.number + 1,
            pageSize: products?.size,
            defaultPageSize: products?.size,
          }}
        />
      </Col>
    </Row>
  );
};

const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = (
  state,
) => ({
  isEnableButtonVisible: hasAuthority('ENABLE_PRODUCT')(state),
  isDisableButtonVisible: hasAuthority('DISABLE_PRODUCT')(state),
  products: state.products.products,
  isLoading: state.products.loading.products,
});

const mapDispatchToProps: MapDispatchToProps<ActionProps, OwnProps> = (
  dispatch,
) => {
  return {
    getProducts: (options) => dispatch(getProducts.request(options)),
    enableProduct: (options) => dispatch(enableProduct.request(options)),
    disableProduct: (options) => dispatch(disableProduct.request(options)),
  };
};
const connected = connect(mapStateToProps, mapDispatchToProps)(ProductTable);

export default connected;
