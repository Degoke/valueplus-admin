import * as React from 'react';
import { connect, MapStateToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import { Button } from 'antd';
import { EditTwoTone } from '@ant-design/icons';
import { hasAuthority } from 'src/actions/profile';
import hideable from 'src/utils/hideable';

interface OwnProps {
  onClick: () => void;
}
const UpdateProduct: React.FunctionComponent<OwnProps> = ({ onClick }) => (
  <Button onClick={onClick} icon={<EditTwoTone />}>
    Edit
  </Button>
);

const mapStateToProps: MapStateToProps<{}, {}, RootState> = (state) => ({
  isVisible: hasAuthority('UPDATE_PRODUCT')(state),
});

const connected = connect(mapStateToProps, null)(hideable(UpdateProduct));

export default connected;
