import * as React from 'react';
import format from 'date-fns/format';
import { Space } from 'antd';
import { IProduct } from 'src/actions/products/types';
import Avatar from 'src/components/Avatar';
import DisableButton from 'src/components/TableDisableButton';
import EnableButton from 'src/components/TableEnableButton';
import UpdateProduct from './UpdateProduct';

export const columnsKey = (
  isEnableButtonVisible: boolean,
  isDisableButtonVisible: boolean,
  enableProduct: (id: number) => void,
  disableProduct: (id: number) => void,
  handleProductEdit: (id: number) => void,
) => [
  {
    title: 'Created Date',
    dataIndex: 'createdAt',
    key: 'createdAt',
    render: (date: Date) => format(new Date(date), 'PP'),
  },
  {
    title: 'Product Image',
    dataIndex: 'image',
    key: 'image',
    render: (image: string) => (
      <Avatar size={'large'} gap={3} src={image} firstName={''} lastName={''} />
    ),
  },
  {
    title: 'Product Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Price',
    dataIndex: 'price',
    key: 'price',
    render: (price: string) => `₦ ${price.toLocaleString()}`,
  },
  {
    title: 'Updated Date',
    dataIndex: 'updatedAt',
    key: 'updatedAt',
    render: (date: Date) => format(new Date(date), 'PP'),
  },
  {
    title: 'Action',
    key: 'action',
    render: (_text: any, record: IProduct) => (
      <Space size="middle">
        <UpdateProduct onClick={() => handleProductEdit(record.id)} />
        {record.disabled && (
          <EnableButton
            isVisible={isEnableButtonVisible}
            onClick={() => enableProduct(record.id)}
          />
        )}
        {!record.disabled && (
          <DisableButton
            isVisible={isDisableButtonVisible}
            onClick={() => disableProduct(record.id)}
          />
        )}
      </Space>
    ),
  },
];
