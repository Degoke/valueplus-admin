import { default as styles } from './SalesHistoryFilter.module.scss';
import * as React from 'react';
import { Row, Col, Input, Radio, DatePicker } from 'antd';
import { Status } from 'src/utils';
import { ProductOrdersOptions } from 'src/actions/products/types';
import { RadioChangeEvent } from 'antd/lib/radio';
import constants from 'src/utils/constants';
import Button from 'src/components/Button';

const { RangePicker } = DatePicker;
const { Search } = Input;

export interface SalesHistoryFilterProps {
  filterBy: (options: ProductOrdersOptions) => void;
  getProductOrders: (options: ProductOrdersOptions) => void;
}

const SalesHistoryFilter: React.FunctionComponent<SalesHistoryFilterProps> = ({
  filterBy,
  getProductOrders,
}) => {
  const [dates, setDates] = React.useState<[moment.Moment, moment.Moment]>(
    null,
  );
  const [query, setQuery] = React.useState<string>('');
  const [status, setStatus] = React.useState<Status>(null);

  const disabledDate = (current: moment.Moment) => {
    if (!dates) {
      return false;
    }
    const tooLate = dates[0] && current.diff(dates[0], 'days') > 14;
    const tooEarly = dates[1] && dates[1].diff(current, 'days') > 14;
    return tooEarly || tooLate;
  };

  const handleDate = (dates: [any, any]) => {
    filterBy({
      startDate: dates[0]?.format('YYYY-MM-DD'),
      endDate: dates[1]?.format('YYYY-MM-DD'),
    }),
      setDates(dates);
  };
  const handleStatusChange = (e: RadioChangeEvent) => {
    filterBy({ status: e.target.value });
    setStatus(e.target.value);
  };
  const handleSearch = (query: string) => {
    query
      ? filterBy({ customerName: query })
      : getProductOrders({ size: constants.PRODUCTS_ORDERS_SIZE });
  };

  const handleClear = () => {
    setQuery('');
    setDates(null);
    setStatus(null);
    getProductOrders({ size: constants.PRODUCTS_ORDERS_SIZE });
  };
  const showClear = dates || query || status;
  return (
    <Row gutter={24} justify="center">
      <Col xs={24} xl={6} className={styles.gutter}>
        <div className={styles.title}>Filter by Customer name</div>
        <Search
          placeholder="Search History"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
          onSearch={handleSearch}
        />
      </Col>
      <Col xs={24} xl={6} className={styles.gutter}>
        <div className={styles.title}>Filter by Date</div>
        <RangePicker
          value={dates}
          disabledDate={disabledDate}
          onChange={handleDate}
        />
      </Col>
      <Col xs={24} xl={10} className={styles.gutter}>
        <div className={styles.title}>Filter by Status</div>
        <Radio.Group onChange={handleStatusChange} value={status}>
          <Radio value={Status.PENDING}>Pending</Radio>
          <Radio value={Status.IN_PROGRESS}>In Progress</Radio>
          <Radio value={Status.COMPLETED}>Completed</Radio>
          <Radio value={Status.CANCELLED}>Cancelled</Radio>
          <Radio value={Status.FAILED}>Failed</Radio>
        </Radio.Group>
      </Col>
      <Col
        xs={8}
        xl={2}
        className={styles.gutter}
        style={{ justifyContent: 'center' }}
      >
        {showClear && <Button onClick={handleClear}>Clear</Button>}
      </Col>
    </Row>
  );
};

export default SalesHistoryFilter;
