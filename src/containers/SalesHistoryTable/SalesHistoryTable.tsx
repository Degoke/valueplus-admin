import { default as styles } from './SalesHistoryTable.module.scss';
import * as React from 'react';
import { Row, Col, Menu, Table, Popconfirm } from 'antd';
import { TableProps } from 'antd/lib/table';
import { Status, StatusColor } from 'src/utils';
import * as types from 'src/actions/products/types';
import { columnsKey } from './constants';

const status = [...StatusColor.keys()];

export interface SalesHistoryTableProps extends TableProps<any> {
  orders: types.ProductOrderItem[];
  updateProductStatus: (update: types.ProductStatusUpdate) => void;
}

const SalesHistoryTable: React.FunctionComponent<SalesHistoryTableProps> = ({
  orders,
  updateProductStatus,
  ...rest
}) => {
  const menu = (id: string) => (
    <Menu>
      <>
        {status
          .filter(
            (status) =>
              status !== Status.COMPLETED && status !== Status.CANCELLED,
          )
          .map((status) => (
            <Menu.Item
              key={status}
              onClick={() =>
                updateProductStatus({
                  id,
                  status,
                })
              }
            >
              <a>{status.toLowerCase()}</a>
            </Menu.Item>
          ))}
        {[Status.COMPLETED].map((status) => (
          <Menu.Item key={status}>
            <Popconfirm
              title="Sure?"
              onConfirm={() =>
                updateProductStatus({
                  id,
                  status,
                })
              }
            >
              <a>{status.toLowerCase()}</a>
            </Popconfirm>
          </Menu.Item>
        ))}
      </>
    </Menu>
  );

  return (
    <Row gutter={24} justify="center" className={styles.wrapper}>
      <Col xs={24}>
        <Table
          columns={columnsKey(menu)}
          scroll={{ x: true }}
          dataSource={orders?.map((order) => ({
            ...order,
            key: order.id,
          }))}
          {...rest}
          pagination={{
            position: ['bottomLeft'],
            hideOnSinglePage: true,
            ...rest.pagination,
          }}
        />
      </Col>
    </Row>
  );
};

export default SalesHistoryTable;
