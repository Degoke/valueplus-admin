import * as React from 'react';
import { connect, MapStateToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import { Dropdown } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import { hasAuthority } from 'src/actions/profile';
import hideable from 'src/utils/hideable';

interface OwnProps {
  overlay: any;
}
const UpdateStatus: React.FunctionComponent<OwnProps> = ({ overlay }) => (
  <Dropdown overlay={overlay}>
    <a>
      Change Status <DownOutlined />
    </a>
  </Dropdown>
);

const mapStateToProps: MapStateToProps<{}, {}, RootState> = (state) => ({
  isVisible: hasAuthority('UPDATE_PRODUCT_ORDER_STATUS')(state),
});

const connected = connect(mapStateToProps, null)(hideable(UpdateStatus));

export default connected;
