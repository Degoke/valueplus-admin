import * as React from 'react';
import format from 'date-fns/format';
import { Tag } from 'antd';
import { Status, StatusColor } from 'src/utils';
import { ProductOrderItem } from 'src/actions/products/types';
import UpdateStatus from './UpdateStatus';

export const columnsKey = (menu: (id: string) => JSX.Element) => [
  {
    title: 'Date',
    dataIndex: 'createdAt',
    key: 'createdAt',
    render: (date: Date) => format(new Date(date), 'PP'),
  },
  {
    title: 'Product Name',
    dataIndex: 'productName',
    key: 'productName',
    ellipsis: {
      showTitle: false,
    },
  },
  {
    title: 'Customer Name',
    dataIndex: 'customerName',
    key: 'customerName',
    ellipsis: {
      showTitle: false,
    },
  },
  {
    title: 'Customer Address',
    dataIndex: 'address',
    key: 'address',
    ellipsis: {
      showTitle: false,
    },
  },
  {
    title: 'Customer Phone',
    dataIndex: 'phoneNumber',
    key: 'phoneNumber',
    ellipsis: {
      showTitle: false,
    },
  },
  {
    title: 'Agent Email',
    dataIndex: 'agentEmail',
    key: 'agentEmail',
    ellipsis: {
      showTitle: false,
    },
  },
  {
    title: 'Actual Price',
    dataIndex: 'productPrice',
    key: 'productPrice',
    render: (price: string) => `₦${price.toLocaleString()}`,
  },
  {
    title: 'Price sold',
    dataIndex: 'sellingPrice',
    key: 'sellingPrice',
    render: (price: string) => `₦${price.toLocaleString()}`,
  },
  {
    title: 'Qty',
    dataIndex: 'quantity',
    key: 'quantity',
  },
  {
    title: 'Profit',
    dataIndex: 'totalProfit',
    key: 'totalProfit',
    render: (price: string) => `₦${price.toLocaleString()}`,
  },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
    render: (status: Status) => (
      <Tag color={StatusColor.get(status)} key={status}>
        {status.toUpperCase()}
      </Tag>
    ),
  },
  {
    title: 'Action',
    dataIndex: 'operation',
    key: 'operation',
    render: (_key: number, data: ProductOrderItem) =>
      data.status !== Status.COMPLETED &&
      data.status !== Status.CANCELLED && (
        <UpdateStatus overlay={() => menu(String(data.id))} />
      ),
  },
];
