import * as React from 'react';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { registerUser } from 'src/actions/auth';
import { SignupCredentials } from 'src/actions/auth/types';
import { RootState } from 'src/reducers/index';
import FormInput from 'src/components/FormInput';
import Button from 'src/components/Button';
import ErrorState from 'src/components/ErrorState';
import constants from 'src/utils/constants';
import Card from 'src/components/Card';
import { Row, Col, Alert, Checkbox } from 'antd';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import SignUpSuccess from 'src/pages/SignUpSuccess';
import { User } from 'src/actions/users/types';
import hideable from 'src/utils/hideable';
import { hasAuthority } from 'src/actions/profile';
import { SAllAuthorities } from 'src/actions/authorities/selectors';
import { Authorities } from 'src/actions/authorities/types';

interface StateProps {
  isLoading: boolean;
  hasErrored: boolean;
  subAdmin: User;
  privileges: Authorities;
}

interface ActionProps {
  onSignup: (credentials: SignupCredentials) => void;
}

const validationSchema = Yup.object({
  firstname: Yup.string().required('Field cannot be empty'),
  lastname: Yup.string().required('Field cannot be empty'),
  phone: Yup.string().required('Field cannot be empty'),
  email: Yup.string()
    .matches(constants.emailRegExp, 'Must be valid email')
    .required('Must be valid email'),
});
export type SignupProps = StateProps & ActionProps;

export const Signup: React.FunctionComponent<SignupProps> = ({
  onSignup,
  isLoading,
  hasErrored,
  subAdmin,
  privileges,
}) => {
  const [selectedAuthoritiesId, setSelectedAuthoritiesId] = React.useState<
    number[]
  >([]);

  const handleAuthorityChange = (checkedValue: string[]) => {
    const transformCheckedValue = checkedValue.map(Number);
    setSelectedAuthoritiesId(transformCheckedValue);
  };

  const formik = useFormik({
    validationSchema,
    initialValues: {
      email: '',
      firstname: '',
      lastname: '',
      phone: '',
    },
    onSubmit: ({ email, firstname, lastname, phone }, { resetForm }) => {
      const credentials: SignupCredentials = {
        email,
        firstname,
        lastname,
        phone,
      };
      onSignup({ ...credentials, authorityIds: selectedAuthoritiesId });
      resetForm();
    },
  });
  const { errors, handleSubmit, handleChange, touched, values } = formik;
  const { firstname, lastname, phone, email } = values;
  return (
    <>
      <SignUpSuccess isVisible={!!subAdmin} user={subAdmin} />
      <Card title={'Create Sub Admin'}>
        {hasErrored && (
          <Alert
            showIcon
            closable
            message="Something bad happened! At least one authoritiy must be selected"
            type="error"
          />
        )}
        <form onSubmit={handleSubmit}>
          <Row gutter={24} justify="center">
            <Col xs={24} sm={24} md={12}>
              <Row gutter={24} justify="center">
                <Col xs={24} sm={24} md={12}>
                  <FormInput
                    value={firstname}
                    onChange={handleChange}
                    type="text"
                    name="firstname"
                    placeholder="First name"
                  />
                  {errors.firstname && touched.firstname && (
                    <ErrorState>{errors.firstname}</ErrorState>
                  )}
                </Col>
                <Col xs={24} sm={24} md={12}>
                  <FormInput
                    value={lastname}
                    onChange={handleChange}
                    type="text"
                    name="lastname"
                    placeholder="Last name"
                  />
                  {errors.lastname && touched.lastname && (
                    <ErrorState>{errors.lastname}</ErrorState>
                  )}
                </Col>
              </Row>
            </Col>
            <Col xs={24} sm={24} md={12}>
              <Row gutter={24} justify="center">
                <Col xs={24} sm={24} md={12}>
                  <FormInput
                    value={email}
                    onChange={handleChange}
                    type="email"
                    name="email"
                    placeholder="Email"
                  />
                  {errors.email && touched.email && (
                    <ErrorState>{errors.email}</ErrorState>
                  )}
                </Col>
                <Col xs={24} sm={24} md={12}>
                  <FormInput
                    value={phone}
                    onChange={handleChange}
                    type="text"
                    name="phone"
                    placeholder="Phone number"
                  />
                  {errors.phone && touched.phone && (
                    <ErrorState>{errors.phone}</ErrorState>
                  )}
                </Col>
              </Row>
            </Col>
            <Col xs={24} sm={24} style={{ marginBottom: 20 }}>
              <div style={{ marginBottom: 15 }}>Assign Authorities</div>
              <Checkbox.Group onChange={handleAuthorityChange}>
                <Row>
                  {privileges?.map(({ value, label }) => (
                    <Col
                      xs={24}
                      sm={6}
                      style={{ marginBottom: 10 }}
                      key={value}
                    >
                      <Checkbox value={value}>{label}</Checkbox>
                    </Col>
                  ))}
                </Row>
              </Checkbox.Group>
            </Col>
            <Button type={'submit'} isLoading={isLoading}>
              Create
            </Button>
          </Row>
        </form>
      </Card>
    </>
  );
};

const mapStateToProps: MapStateToProps<StateProps, {}, RootState> = (
  state,
) => ({
  isLoading: state.auth.isLoading,
  hasErrored: state.auth.hasErrored,
  subAdmin: state.auth.registrationSuccessful,
  isVisible: hasAuthority('CREATE_ADMIN')(state),
  privileges: SAllAuthorities(state),
});

const mapDispatchToProps: MapDispatchToProps<ActionProps, {}> = (dispatch) => {
  return {
    onSignup: (credentials) => dispatch(registerUser.request(credentials)),
  };
};

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(hideable(Signup));

export default connected;
