import * as React from 'react';
import { RootState } from 'src/reducers/index';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { Row, Col, Table, Select } from 'antd';
import {
  User,
  Users,
  FilterOptions,
  UpdateUserAuthorities,
} from 'src/actions/users/types';
import { ROLE_TYPE } from 'src/utils';
import {
  getFilteredUsers,
  getAllUsers,
  enableUser,
  disableUser,
  isUsersLoading,
  updateUserAuthorities,
} from 'src/actions/users';
import { userColumnsKey } from '../UserTable/constants';
import { hasAuthority } from 'src/actions/profile';
import { SAllAuthorities } from 'src/actions/authorities';
import { Authorities } from 'src/actions/authorities/types';
import { EditOutlined } from '@ant-design/icons';

const { Option } = Select;

export interface StateProps {
  isLoading: boolean;
  users: Users;
  hasUpdateAuthority: boolean;
  allAuthorities: Authorities;
  isEnableButtonVisible: boolean;
  isDisableButtonVisible: boolean;
}
interface ActionProps {
  enableUser: (key: number) => void;
  disableUser: (key: number) => void;
  getAdmins: (options: FilterOptions) => void;
  updateAuthorities: (options: UpdateUserAuthorities) => void;
}
type TAdminTable = ActionProps & StateProps;

const AdminTable: React.FunctionComponent<TAdminTable> = ({
  users,
  allAuthorities,
  isLoading,
  getAdmins,
  enableUser,
  disableUser,
  updateAuthorities,
  hasUpdateAuthority,
  isEnableButtonVisible,
  isDisableButtonVisible,
}) => {
  const [expandedRowKeys, setExpandedRowKeys] = React.useState<any[]>([]);
  const [selectedAuthorities, setSelectedAuthorities] = React.useState<
    number[]
  >([]);

  const onTableRowExpand = (expanded: boolean, record: User) => {
    console.log(expanded);
    console.log(record);
    const keys = [];
    if (expanded) {
      keys.push(record.key);
    }
    setExpandedRowKeys(keys);
  };

  const handleChange = (_: any, option: any) => {
    const ids: number[] = option.map((value: any) => Number(value.key));
    setSelectedAuthorities(ids);
  };

  const expandedRowRender = ({ id, authorities }: User) => (
    <Select
      disabled={!hasUpdateAuthority}
      mode="multiple"
      style={{ width: '100%' }}
      onBlur={() =>
        updateAuthorities({ userId: id, authorities: selectedAuthorities })
      }
      onChange={handleChange}
      defaultValue={authorities}
    >
      {allAuthorities?.map(({ value, label }) => (
        <Option value={label} key={value}>
          {label}
        </Option>
      ))}
    </Select>
  );

  return (
    <Row gutter={24} justify="center">
      <Col xs={24}>
        <Table
          columns={userColumnsKey(
            isEnableButtonVisible,
            isDisableButtonVisible,
            enableUser,
            disableUser,
          )}
          scroll={{ x: true }}
          dataSource={users?.content?.map((user, index) => ({
            ...user,
            key: index,
          }))}
          onChange={(page) =>
            getAdmins({
              roleType: ROLE_TYPE.ADMIN,
              size: page.pageSize,
              page: page.current - 1,
            })
          }
          loading={isLoading}
          pagination={{
            showSizeChanger: true,
            total: users?.totalElements,
            current: users?.number + 1,
            pageSize: users?.size,
            defaultPageSize: users?.size,
            position: ['bottomLeft'],
            hideOnSinglePage: true,
          }}
          expandable={{
            expandedRowRender,
            expandedRowKeys,
            expandIcon: ({ expanded, record }) => (
              <EditOutlined
                onClick={() => onTableRowExpand(!expanded, record)}
              />
            ),
          }}
        />
      </Col>
    </Row>
  );
};

const mapStateToProps: MapStateToProps<StateProps, {}, RootState> = (
  state,
) => ({
  allAuthorities: SAllAuthorities(state),
  users: getAllUsers(state),
  isLoading: isUsersLoading(state),
  isEnableButtonVisible: hasAuthority('ENABLE_USER')(state),
  isDisableButtonVisible: hasAuthority('DISABLE_USER')(state),
  hasUpdateAuthority: hasAuthority('UPDATE_ADMIN_AUTHORITY')(state),
});

const mapDispatchToProps: MapDispatchToProps<ActionProps, {}> = (dispatch) => {
  return {
    enableUser: (options) => dispatch(enableUser.request(options)),
    disableUser: (options) => dispatch(disableUser.request(options)),
    getAdmins: (options) => dispatch(getFilteredUsers.request(options)),
    updateAuthorities: (options) =>
      dispatch(updateUserAuthorities.request(options)),
  };
};
const connected = connect(mapStateToProps, mapDispatchToProps)(AdminTable);

export default connected;
