import * as React from 'react';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import {
  createSuperAgent,
  SSuperAgent,
  SSuperAgentLoading,
  SSuperAgentSuccess,
} from 'src/actions/superAgents';
import { SignupCredentials } from 'src/actions/auth/types';
import { RootState } from 'src/reducers/index';
import FormInput from 'src/components/FormInput';
import Button from 'src/components/Button';
import ErrorState from 'src/components/ErrorState';
import constants from 'src/utils/constants';
import Card from 'src/components/Card';
import { Row, Col } from 'antd';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import SignUpSuccess from 'src/pages/SignUpSuccess';
import { User } from 'src/actions/users/types';
import hideable from 'src/utils/hideable';
import { hasAuthority } from 'src/actions/profile';

interface OwnProps {}

interface StateProps {
  isLoading: boolean;
  isSuccess: boolean;
  superAgent: User;
}

interface ActionProps {
  onSignup: (credentials: SignupCredentials) => void;
}

const validationSchema = Yup.object({
  firstname: Yup.string().required('Field cannot be empty'),
  lastname: Yup.string().required('Field cannot be empty'),
  phone: Yup.string().required('Field cannot be empty'),
  email: Yup.string()
    .matches(constants.emailRegExp, 'Must be valid email')
    .required('Must be valid email'),
});
export type SignupProps = OwnProps & StateProps & ActionProps;

export const Signup: React.FunctionComponent<SignupProps> = ({
  onSignup,
  isLoading,
  isSuccess,
  superAgent,
}) => {
  const formik = useFormik({
    validationSchema,
    initialValues: {
      email: '',
      firstname: '',
      lastname: '',
      phone: '',
    },
    onSubmit: (values, { resetForm }) => {
      const credentials: SignupCredentials = {
        email: values.email,
        firstname: values.firstname,
        lastname: values.lastname,
        phone: values.phone,
      };
      onSignup(credentials);
      resetForm();
    },
  });
  const { errors, handleSubmit, handleChange, touched, values } = formik;
  const { firstname, lastname, phone, email } = values;

  return (
    <>
      <SignUpSuccess isVisible={isSuccess} user={superAgent} />
      <Card title={'Create Super Agent'}>
        <form onSubmit={handleSubmit}>
          <Row gutter={24} justify="center">
            <Col xs={24} sm={24} md={12}>
              <Row gutter={24} justify="center">
                <Col xs={24} sm={24} md={12}>
                  <FormInput
                    value={firstname}
                    onChange={handleChange}
                    type="text"
                    name="firstname"
                    placeholder="First name"
                  />
                  {errors.firstname && touched.firstname && (
                    <ErrorState>{errors.firstname}</ErrorState>
                  )}
                </Col>
                <Col xs={24} sm={24} md={12}>
                  <FormInput
                    value={lastname}
                    onChange={handleChange}
                    type="text"
                    name="lastname"
                    placeholder="Last name"
                  />
                  {errors.lastname && touched.lastname && (
                    <ErrorState>{errors.lastname}</ErrorState>
                  )}
                </Col>
              </Row>
            </Col>
            <Col xs={24} sm={24} md={12}>
              <Row gutter={24} justify="center">
                <Col xs={24} sm={24} md={12}>
                  <FormInput
                    value={email}
                    onChange={handleChange}
                    type="email"
                    name="email"
                    placeholder="Email"
                  />
                  {errors.email && touched.email && (
                    <ErrorState>{errors.email}</ErrorState>
                  )}
                </Col>
                <Col xs={24} sm={24} md={12}>
                  <FormInput
                    value={phone}
                    onChange={handleChange}
                    type="text"
                    name="phone"
                    placeholder="Phone number"
                  />
                  {errors.phone && touched.phone && (
                    <ErrorState>{errors.phone}</ErrorState>
                  )}
                </Col>
              </Row>
            </Col>
            <Button type={'submit'} isLoading={isLoading}>
              Create
            </Button>
          </Row>
        </form>
      </Card>
    </>
  );
};

const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = (
  state,
) => ({
  superAgent: SSuperAgent(state),
  isLoading: SSuperAgentLoading(state),
  isSuccess: SSuperAgentSuccess(state),
  isVisible: hasAuthority('CREATE_SUPER_AGENT')(state),
});
const mapDispatchToProps: MapDispatchToProps<ActionProps, OwnProps> = (
  dispatch,
) => {
  return {
    onSignup: (credentials) => dispatch(createSuperAgent.request(credentials)),
  };
};

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(hideable(Signup));

export default connected;
