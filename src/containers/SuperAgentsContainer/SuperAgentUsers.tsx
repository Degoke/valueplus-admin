import * as React from 'react';
import { Row, Col, Table } from 'antd';
import { TableProps } from 'antd/lib/table';
import { superAgentUsersColumnsKey } from './constants';
import { User } from 'src/actions/users/types';
import AgentUsersFilters from './SuperAgentUsersTableFilter';
import { FilterOptions } from 'src/actions/superAgents/types';

export interface SuperAgentUsersTableProps extends TableProps<any> {
  agents: User[];
  filterBy: (options: FilterOptions) => void;
}

const SuperAgentUsersTable: React.FunctionComponent<SuperAgentUsersTableProps> = ({
  agents,
  filterBy,
  ...rest
}) => {
  return (
    <Row gutter={24} justify="center">
      <Col xs={24}>
        <AgentUsersFilters filterBy={filterBy} />
        <Table
          columns={superAgentUsersColumnsKey}
          scroll={{ x: true }}
          dataSource={agents?.map((agent, index) => ({
            ...agent,
            key: index,
          }))}
          {...rest}
          pagination={{
            position: ['bottomLeft'],
            hideOnSinglePage: true,
            ...rest.pagination,
          }}
        />
      </Col>
    </Row>
  );
};

export default SuperAgentUsersTable;
