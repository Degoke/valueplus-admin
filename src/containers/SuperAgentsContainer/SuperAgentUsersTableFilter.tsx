import { default as styles } from './SuperAgents.module.scss';
import * as React from 'react';
import { Row, Col, Input, DatePicker } from 'antd';
import { FilterOptions } from 'src/actions/superAgents/types';
import Button from 'src/components/Button';

const { RangePicker } = DatePicker;

export interface AgentUsersFiltersProps {
  filterBy: (options: FilterOptions) => void;
}

const AgentUsersFilters: React.FunctionComponent<AgentUsersFiltersProps> = ({
  filterBy,
}) => {
  const [dates, setDates] = React.useState<[moment.Moment, moment.Moment]>(
    null,
  );
  const [referralCode, setReferralCode] = React.useState<string>('');

  const disabledDate = (current: moment.Moment) => {
    if (!dates) {
      return false;
    }
    const tooLate = dates[0] && current.diff(dates[0], 'days') > 14;
    const tooEarly = dates[1] && dates[1].diff(current, 'days') > 14;
    return tooEarly || tooLate;
  };

  const handleDate = (dates: [any, any]) => {
    setDates(dates);
  };

  const handleClear = () => {
    setReferralCode('');
    setDates(null);
  };

  const handleSubmit = () => {
    filterBy({
      startDate: dates ? dates[0]?.format('YYYY-MM-DD') : '',
      endDate: dates ? dates[1]?.format('YYYY-MM-DD') : '',
      superAgentCode: referralCode,
    });
  };

  const showClear = dates && referralCode;
  return (
    <>
      <Row gutter={24} justify="center" className={styles.filter}>
        <Col xs={24} xl={6} className={styles.gutter}>
          <div className={styles.title}>Super Agent Code</div>
          <Input
            placeholder="Agent Referral Code"
            value={referralCode}
            onChange={(e) => setReferralCode(e.target.value)}
          />
        </Col>
        <Col xs={24} xl={6} className={styles.gutter}>
          <div className={styles.title}>Date Range</div>
          <RangePicker
            value={dates}
            disabledDate={disabledDate}
            onChange={handleDate}
          />
        </Col>
      </Row>
      <Row gutter={24} justify="center" className={styles.filter}>
        <Col
          xs={8}
          xl={2}
          className={styles.gutter}
          style={{ justifyContent: 'center' }}
        >
          <Button size={'small'} onClick={handleSubmit}>
            Submit
          </Button>
        </Col>
        <Col
          xs={8}
          xl={2}
          className={styles.gutter}
          style={{ justifyContent: 'center' }}
        >
          {showClear && <Button onClick={handleClear}>Clear</Button>}
        </Col>
      </Row>
    </>
  );
};

export default AgentUsersFilters;
