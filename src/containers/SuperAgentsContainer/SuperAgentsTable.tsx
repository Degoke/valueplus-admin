import * as React from 'react';
import { RootState } from 'src/reducers/index';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { Row, Col, Table } from 'antd';
import { superAgentsColumnsKey } from './constants';
import { Users } from 'src/actions/users/types';
import { PaginationPageOptions } from 'src/utils';
import {
  getSuperAgents,
  SSuperAgents,
  SSuperAgentsLoading,
} from 'src/actions/superAgents';
import hideable from 'src/utils/hideable';
import { hasAuthority } from 'src/actions/profile';
export interface StateProps {
  isLoading: boolean;
  agents: Users;
}

interface ActionProps {
  getSuperAgents: (options: PaginationPageOptions) => void;
}
type TSuperAgentsTable = ActionProps & StateProps;

const SuperAgentsTable: React.FunctionComponent<TSuperAgentsTable> = ({
  agents,
  isLoading,
  getSuperAgents,
}) => (
  <Row gutter={24} justify="center">
    <Col xs={24}>
      <Table
        columns={superAgentsColumnsKey}
        scroll={{ x: true }}
        dataSource={agents?.content?.map((agent, index) => ({
          ...agent,
          key: index,
        }))}
        onChange={(page) =>
          getSuperAgents({
            size: page.pageSize,
            page: page.current - 1,
          })
        }
        loading={isLoading}
        pagination={{
          showSizeChanger: true,
          total: agents?.totalElements,
          current: agents?.number + 1,
          pageSize: agents?.size,
          defaultPageSize: agents?.size,
          position: ['bottomLeft'],
          hideOnSinglePage: true,
        }}
      />
    </Col>
  </Row>
);

const mapStateToProps: MapStateToProps<StateProps, {}, RootState> = (
  state,
) => ({
  agents: SSuperAgents(state),
  isLoading: SSuperAgentsLoading(state),
  isVisible: hasAuthority('VIEW_SUPER_AGENTS')(state),
});

const mapDispatchToProps: MapDispatchToProps<ActionProps, {}> = (dispatch) => {
  return {
    getSuperAgents: (options) => dispatch(getSuperAgents.request(options)),
  };
};

const connected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(hideable(SuperAgentsTable));

export default connected;
