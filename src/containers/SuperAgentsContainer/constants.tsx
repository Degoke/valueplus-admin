export const superAgentsColumnsKey = [
  {
    title: 'Name',
    dataIndex: 'firstname',
    key: 'firstname',
    render: (_value: string, record: any) =>
      `${record?.firstname} ${record?.lastname}`,
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: 'Email',
    dataIndex: 'email',
    key: 'email',
  },
  {
    title: 'Referal Code',
    dataIndex: 'referralCode',
    key: 'referralCode',
  },
  {
    title: 'Role',
    dataIndex: 'roleType',
    key: 'roleType',
  },
];
export const superAgentUsersColumnsKey = [
  {
    title: 'Name',
    dataIndex: 'firstname',
    key: 'firstname',
    render: (_value: string, record: any) =>
      `${record?.firstname} ${record?.lastname}`,
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: 'Email',
    dataIndex: 'email',
    key: 'email',
  },
  {
    title: 'Referred By',
    dataIndex: 'superAgentCode',
    key: 'superAgentCode',
  },
];
