import * as React from 'react';
import { Row, Col } from 'antd';
import ErrorState from 'src/components/ErrorState';
import { FormikErrors, FormikTouched } from 'formik';
import { DatePicker, Input } from 'antd';
import { PercentageOutlined } from '@ant-design/icons';

const dateFormat = 'YYYY-MM-DD';
interface OwnProps {
  isDisabled: boolean;
  errors: FormikErrors<{
    commissionPercentage: string;
    commissionEffectiveDate: any;
  }>;
  handleChange: (e: any) => void;
  touched: FormikTouched<{
    commissionPercentage: string;
    commissionEffectiveDate: any;
  }>;
  values: { commissionPercentage: string };
  setFieldValue: (field: string, value: any, shouldValidate?: boolean) => any;
}

export type TCommissionForm = OwnProps;

const CommissionForm: React.FunctionComponent<TCommissionForm> = ({
  isDisabled,
  handleChange,
  errors,
  touched,
  values: { commissionPercentage },
  setFieldValue,
}) => (
  <Row gutter={24}>
    <Col xs={24} sm={24} md={12}>
      <Input
        addonAfter={<PercentageOutlined />}
        name={'commissionPercentage'}
        value={commissionPercentage}
        onChange={handleChange}
        placeholder="Commission Percentage"
        disabled={isDisabled}
      />
      <div style={{ marginTop: 20 }}>
        {errors.commissionPercentage && touched.commissionPercentage && (
          <ErrorState>{errors.commissionPercentage}</ErrorState>
        )}
      </div>
    </Col>
    <Col xs={24} sm={24} md={12}>
      <DatePicker
        // style={{ width: 200 }}
        format={dateFormat}
        onChange={(_value, dateString) =>
          setFieldValue('commissionEffectiveDate', dateString)
        }
        name={'commissionEffectiveDate'}
        placeholder="Effective Date"
        disabled={isDisabled}
      />
      <div style={{ marginTop: 20 }}>
        {errors.commissionEffectiveDate && touched.commissionEffectiveDate && (
          <ErrorState>{errors.commissionEffectiveDate}</ErrorState>
        )}
      </div>
    </Col>
  </Row>
);

export default CommissionForm;
