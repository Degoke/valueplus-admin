import { default as styles } from './TransactionCommission.module.scss';
import * as React from 'react';
import { Row, Col, Table } from 'antd';
import { TableProps } from 'antd/lib/table';
import * as types from 'src/actions/settings/types';
import { schedulesColumnsKey } from './TableColumnKey';

export interface UserTableProps extends TableProps<any> {
  commissions: types.Schedule[];
}

const ScheduledCommissions: React.FunctionComponent<UserTableProps> = ({
  commissions,
  ...rest
}) => {
  return (
    <Row gutter={24}>
      <Col xs={24}>
        <h3 className={styles.titleGutter}>Commission Logs</h3>
        <Table
          columns={schedulesColumnsKey}
          scroll={{ x: true }}
          dataSource={commissions?.map((commision, key) => ({
            ...commision,
            key,
          }))}
          {...rest}
          pagination={{
            position: ['bottomLeft'],
            hideOnSinglePage: true,
            ...rest.pagination,
          }}
        />
      </Col>
    </Row>
  );
};

export default ScheduledCommissions;
