import * as React from 'react';
import { Row, Col, Table } from 'antd';
import { TableProps } from 'antd/lib/table';
import hideable from 'src/utils/hideable';

export interface UserTableProps extends TableProps<any> {
  data: any[];
  columnKeys: any[];
}

const CommisionTable: React.FunctionComponent<UserTableProps> = ({
  data,
  columnKeys,
  ...rest
}) => {
  return (
    <Row gutter={24} style={{ marginBottom: 40 }}>
      <Col xs={24}>
        <Table
          columns={columnKeys}
          scroll={{ x: true }}
          dataSource={data?.map((data, key) => ({
            ...data,
            key,
          }))}
          {...rest}
          pagination={{
            position: ['bottomLeft'],
            hideOnSinglePage: true,
            ...rest.pagination,
          }}
        />
      </Col>
    </Row>
  );
};

export default hideable(CommisionTable);
