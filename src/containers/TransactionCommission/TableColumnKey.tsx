import format from 'date-fns/format';
import React from 'react';
import { Tag } from 'antd';

export const commissionLogsColumnsKey = [
  {
    title: 'Percentage Commission (Current)',
    dataIndex: 'commissionPercentage',
    key: 'commissionPercentage',
    render: (value: string) => <strong>{value}%</strong>,
  },
  {
    title: 'Percentage Commission (Previous)',
    dataIndex: 'prevCommissionPercentage',
    key: 'prevCommissionPercentage',
    render: (value: string) => <strong>{value}%</strong>,
  },
  {
    title: 'Initiator',
    dataIndex: 'initiator',
    key: 'initiator',
  },
  {
    title: 'Created Date',
    dataIndex: 'createdAt',
    key: 'createdAt',
    render: (date: Date) => format(new Date(date), 'PPpp'),
  },
];

export const schedulesColumnsKey = [
  {
    title: 'Commission',
    dataIndex: 'commissionPercentage',
    key: 'commissionPercentage',
    render: (value: string) => <strong>{value}%</strong>,
  },
  {
    title: 'Effective Date',
    dataIndex: 'effectiveDate',
    key: 'effectiveDate',
    render: (date: any) => format(new Date(date), 'PPpp'),
  },
  {
    title: 'Initiator',
    dataIndex: 'initiator',
    key: 'initiator',
  },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
    render: (value: string) => {
      return (
        <>
          {value === 'EFFECTED' && <Tag color="success">{value}</Tag>}
          {value === 'SCHEDULED' && <Tag color="processing">{value}</Tag>}
          {value !== 'SCHEDULED' && value !== 'EFFECTED' && (
            <Tag color="default">{value}</Tag>
          )}
        </>
      );
    },
  },
  {
    title: 'Created Date',
    dataIndex: 'createdAt',
    key: 'createdAt',
    render: (date: Date) => format(new Date(date), 'PPpp'),
  },
];
