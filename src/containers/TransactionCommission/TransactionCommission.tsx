import { default as styles } from './TransactionCommission.module.scss';
import * as React from 'react';
import { Row, Col, Statistic } from 'antd';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import Card from 'src/components/Card';
import {
  updateCommission,
  getCommissionLogs,
  getScheduledCommissions,
} from 'src/actions/settings';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import ProfileCardHeader from 'src/components/ProfileCardHeader';
import * as types from 'src/actions/settings/types';
import {
  SCommissionLogs,
  SScheduledCommisions,
  SCommissionError,
  SCommissionLoading,
  SCommissionSuccess,
  SCommissionLogsLoading,
  SCommissionScheduleLoading,
} from 'src/actions/settings';
import CommissionForm from './Form';
import CommissionTable from './Table';
import { hasAuthority } from 'src/actions/profile';
import {
  schedulesColumnsKey,
  commissionLogsColumnsKey,
} from './TableColumnKey';

schedulesColumnsKey;
interface StateProps {
  isLoading: boolean;
  isSuccess: string;
  isError: string;
  schedules: types.Schedules;
  hasUpdateAuthority: boolean;
  hasScheduleAuthority: boolean;
  commissionLogs: types.CommissionLogs;
  isLogsLoading: boolean;
  isScheduleLoading: boolean;
}

interface ActionProps {
  getCommissionLogs: (options: types.CommissionPageOptions) => void;
  updateCommission: (settings: types.CommissionPayload) => void;
  getScheduledCommissions: (options: types.CommissionPageOptions) => void;
}
// The Effective date of the commission has to be in the future
const validationSchema = Yup.object({
  commissionPercentage: Yup.number().required(
    "Commission percentage can't be empty",
  ),
  commissionEffectiveDate: Yup.string().required(
    "Commission Effective date can't be empty",
  ),
});
export type TCommission = StateProps & ActionProps;

const Commission: React.FunctionComponent<TCommission> = ({
  isLoading,
  isSuccess,
  isError,
  schedules,
  updateCommission,
  getCommissionLogs,
  commissionLogs,
  isLogsLoading,
  isScheduleLoading,
  hasUpdateAuthority,
  hasScheduleAuthority,
  getScheduledCommissions,
}) => {
  const [isUpdating, setIsUpdating] = React.useState<boolean>(false);

  React.useEffect(() => {
    getCommissionLogs({ size: 5 });
    getScheduledCommissions({ size: 5 });
  }, []);

  const formik = useFormik({
    validationSchema,
    initialValues: {
      commissionPercentage: '',
      commissionEffectiveDate: '',
    },
    onSubmit: (values: types.CommissionPayload) => {
      updateCommission(values);
      setIsUpdating(false);
    },
  });

  const {
    errors,
    touched,
    handleSubmit,
    handleChange,
    values,
    resetForm,
    setFieldValue,
  } = formik;
  return (
    <Card
      title={
        <ProfileCardHeader
          isUpdating={isUpdating}
          isLoading={false}
          setIsUpdating={setIsUpdating}
          title={'Percentage Comission'}
          handleSubmit={handleSubmit}
          handleReset={resetForm}
          hasError={isError}
          isSuccess={isSuccess}
          isButtonVisible={hasUpdateAuthority}
        />
      }
    >
      <Row gutter={24} style={{ marginBottom: 40 }}>
        <Col xs={24} sm={24} md={12}>
          <form onSubmit={handleSubmit}>
            <h3 className={styles.titleGutter}>Set new commission</h3>
            <CommissionForm
              errors={errors}
              handleChange={handleChange}
              touched={touched}
              values={values}
              setFieldValue={setFieldValue}
              isDisabled={!isUpdating || isLoading}
            />
          </form>
        </Col>
        <Col xs={24} sm={24} md={10}>
          <Row gutter={24}>
            <Col xs={24} sm={24} md={12} style={{ marginBottom: 30 }}>
              <Statistic
                title="Current Commission"
                value={
                  schedules?.content?.length > 0 &&
                  schedules?.content[0].status === 'EFFECTED'
                    ? schedules?.content[0]?.commissionPercentage
                    : 0
                }
                suffix={'%'}
              />
            </Col>
            <Col xs={24} sm={24} md={12}>
              <Statistic
                title="Scheduled Commission"
                value={
                  schedules?.content?.length > 0 &&
                  schedules?.content[0].status === 'SCHEDULED'
                    ? schedules?.content[0]?.commissionPercentage
                    : 0
                }
                suffix={'%'}
              />
            </Col>
          </Row>
        </Col>
      </Row>
      <div>
        <h3 className={styles.titleGutter}>Commission Logs</h3>
        <CommissionTable
          data={commissionLogs?.content}
          loading={isLogsLoading}
          columnKeys={commissionLogsColumnsKey}
          isVisible
          onChange={(page: { pageSize: number; current: number }) =>
            getCommissionLogs({
              size: page.pageSize,
              page: page.current - 1,
            })
          }
          pagination={{
            showSizeChanger: true,
            total: commissionLogs?.totalElements,
            current: commissionLogs?.number + 1,
            pageSize: commissionLogs?.size,
            defaultPageSize: commissionLogs?.size,
          }}
        />
      </div>
      <div>
        <h3 className={styles.titleGutter}>Scheduled Commisions</h3>
        <CommissionTable
          data={schedules?.content}
          columnKeys={schedulesColumnsKey}
          isVisible={hasScheduleAuthority}
          onChange={(page: { pageSize: number; current: number }) =>
            getScheduledCommissions({
              size: page.pageSize,
              page: page.current - 1,
            })
          }
          loading={isScheduleLoading}
          pagination={{
            showSizeChanger: true,
            total: schedules?.totalElements,
            current: schedules?.number + 1,
            pageSize: schedules?.size,
            defaultPageSize: schedules?.size,
          }}
        />
      </div>
    </Card>
  );
};

const mapStateToProps: MapStateToProps<StateProps, {}, RootState> = (
  state,
) => ({
  commissionLogs: SCommissionLogs(state),
  isLogsLoading: SCommissionLogsLoading(state),
  isScheduleLoading: SCommissionScheduleLoading(state),
  schedules: SScheduledCommisions(state),
  isLoading: SCommissionLoading(state),
  isSuccess: SCommissionSuccess(state) ? 'Commission has been scheduled' : '',
  isError: SCommissionError(state) ? 'Error updating' : '',
  hasUpdateAuthority: hasAuthority('UPDATE_SETTINGS')(state),
  hasScheduleAuthority: hasAuthority('VIEW_SETTINGS_SCHEDULE')(state),
});

const mapDispatchToProps: MapDispatchToProps<ActionProps, {}> = (dispatch) => {
  return {
    updateCommission: (payload) => dispatch(updateCommission.request(payload)),
    getCommissionLogs: (payload) =>
      dispatch(getCommissionLogs.request(payload)),
    getScheduledCommissions: (payload) =>
      dispatch(getScheduledCommissions.request(payload)),
  };
};

const connected = connect(mapStateToProps, mapDispatchToProps)(Commission);

export default connected;
