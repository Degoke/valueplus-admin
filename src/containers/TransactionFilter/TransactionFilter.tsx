import { default as styles } from './TransactionFilter.module.scss';
import * as React from 'react';
import { Row, Col, Radio, DatePicker } from 'antd';
import { TransactionStatus } from 'src/utils';
import { RadioChangeEvent } from 'antd/lib/radio';
import constants from 'src/utils/constants';
import Button from 'src/components/Button';
import {
  TransactionFilterOptions,
  TransactionsPageOptions,
} from 'src/actions/transactions/types';

const { RangePicker } = DatePicker;

export interface TransactionFilterProps {
  filterBy: (options: TransactionFilterOptions) => void;
  getTransactions: (options: TransactionsPageOptions) => void;
}

const TransactionFilter: React.FunctionComponent<TransactionFilterProps> = ({
  filterBy,
  getTransactions,
}) => {
  const [dates, setDates] = React.useState<[moment.Moment, moment.Moment]>(
    null,
  );
  const [status, setStatus] = React.useState<TransactionStatus>(null);

  const disabledDate = (current: moment.Moment) => {
    if (!dates) {
      return false;
    }
    const tooLate = dates[0] && current.diff(dates[0], 'days') > 14;
    const tooEarly = dates[1] && dates[1].diff(current, 'days') > 14;
    return tooEarly || tooLate;
  };

  const handleDate = (dates: [any, any]) => {
    filterBy({
      startDate: dates[0]?.format('YYYY-MM-DD'),
      endDate: dates[1]?.format('YYYY-MM-DD'),
    }),
      setDates(dates);
  };
  const handleStatusChange = (e: RadioChangeEvent) => {
    filterBy({ status: e.target.value });
    setStatus(e.target.value);
  };

  const handleClear = () => {
    setDates(null);
    setStatus(null);
    getTransactions({ size: constants.TRANSACTION_PAGE_SIZE });
  };

  const showClear = dates || status;
  React.useEffect(() => {
    return handleClear;
  }, []);

  return (
    <Row gutter={24} justify="center" className={styles.wrapper}>
      <Col xs={24} xl={8} className={styles.gutter}>
        <div className={styles.title}>Filter by Date</div>
        <RangePicker
          value={dates}
          disabledDate={disabledDate}
          onChange={handleDate}
        />
      </Col>
      <Col xs={24} xl={14} className={styles.gutter}>
        <div className={styles.title}>Filter by Status</div>
        <Radio.Group onChange={handleStatusChange} value={status}>
          <Radio value={TransactionStatus.OTP}>Pending OTP</Radio>
          <Radio value={TransactionStatus.ABANDONED}>Abandoned</Radio>
          <Radio value={TransactionStatus.PENDING}>Processing</Radio>
          <Radio value={TransactionStatus.SUCCESS}>Completed</Radio>
          <Radio value={TransactionStatus.FAILED}>Failed</Radio>
          <Radio value={TransactionStatus.ERROR}>Error Processing</Radio>
        </Radio.Group>
      </Col>
      <Col
        xs={8}
        xl={2}
        className={styles.gutter}
        style={{ justifyContent: 'center' }}
      >
        {showClear && <Button onClick={handleClear}>Clear</Button>}
      </Col>
    </Row>
  );
};

export default TransactionFilter;
