import { default as styles } from './UserTable.module.scss';
import * as React from 'react';
import { Row, Col, Table } from 'antd';
import { TableProps } from 'antd/lib/table';
import { UserPageOptions, Users } from 'src/actions/users/types';
import { userColumnsKey } from './constants';
import { UserOrderFilter } from 'src/actions/products/types';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import {
  enableUser,
  disableUser,
  getUsers,
  getAllUsers,
  isUsersLoading,
} from 'src/actions/users';
import { hasAuthority } from 'src/actions/profile';
import { getUserOrders } from 'src/actions/products';
import { ROLE_TYPE } from 'src/utils';

export interface OwnProps extends TableProps<any> {}
export interface ActionProps {
  enableUser: (key: number) => void;
  disableUser: (key: number) => void;
  getUsers: (options: UserPageOptions) => void;
  getUserOrders: (options: UserOrderFilter) => void;
}
export interface StateProps {
  isLoading: boolean;
  users: Users;
  isEnableButtonVisible: boolean;
  isDisableButtonVisible: boolean;
}

export type UserTableProps = OwnProps & StateProps & ActionProps;
const UserTable: React.FunctionComponent<UserTableProps> = ({
  users,
  getUsers,
  getUserOrders,
  enableUser,
  disableUser,
  isLoading,
  isEnableButtonVisible,
  isDisableButtonVisible,
  ...rest
}) => (
  <Row gutter={24} justify="center" className={styles.wrapper}>
    <Col xs={24}>
      <Table
        {...rest}
        onRow={(record) => ({
          onClick: () => getUserOrders({ agentId: record.id }),
        })}
        columns={userColumnsKey(
          isEnableButtonVisible,
          isDisableButtonVisible,
          enableUser,
          disableUser,
        )}
        scroll={{ x: true }}
        dataSource={users?.content
          ?.filter((user) => user.roleType === ROLE_TYPE.AGENT)
          .map((user, key) => ({
            ...user,
            key,
          }))}
        loading={isLoading}
        onChange={(page: { pageSize: number; current: number }) =>
          getUsers({
            size: page.pageSize,
            page: page.current - 1,
          })
        }
        pagination={{
          position: ['bottomLeft'],
          hideOnSinglePage: true,
          showSizeChanger: true,
          total: users?.totalElements,
          current: users?.number + 1,
          pageSize: users?.size,
          defaultPageSize: users?.size,
        }}
      />
    </Col>
  </Row>
);

const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = (
  state,
) => ({
  users: getAllUsers(state),
  isLoading: isUsersLoading(state),
  isEnableButtonVisible: hasAuthority('ENABLE_USER')(state),
  isDisableButtonVisible: hasAuthority('DISABLE_USER')(state),
});

const mapDispatchToProps: MapDispatchToProps<ActionProps, OwnProps> = (
  dispatch,
) => {
  return {
    enableUser: (options) => dispatch(enableUser.request(options)),
    disableUser: (options) => dispatch(disableUser.request(options)),
    getUsers: (options) => dispatch(getUsers.request(options)),
    getUserOrders: (options) => dispatch(getUserOrders.request(options)),
  };
};
const connected = connect(mapStateToProps, mapDispatchToProps)(UserTable);

export default connected;
