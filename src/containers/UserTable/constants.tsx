import * as React from 'react';
import { User } from 'src/actions/users/types';
import DisableButton from 'src/components/TableDisableButton';
import EnableButton from 'src/components/TableEnableButton';
import { ROLE_TYPE } from 'src/utils';

export const userColumnsKey = (
  isEnableButtonVisible: boolean,
  isDisableButtonVisible: boolean,
  enableUser: (key: number) => void,
  disableUser: (key: number) => void,
) => [
  {
    title: 'Name',
    dataIndex: 'firstname',
    key: 'firstname',
    render: (_value: string, record: any) =>
      `${record?.firstname} ${record?.lastname}`,
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: 'Phone',
    dataIndex: 'phone',
    key: 'phone',
  },
  {
    title: 'Email',
    dataIndex: 'email',
    key: 'email',
  },
  {
    title: 'Verified',
    dataIndex: 'emailVerified',
    key: 'emailVerified',
    render: (value: boolean, record: User) =>
      record.roleType === ROLE_TYPE.ADMIN ? '-' : value ? 'Yes' : 'No',
  },
  {
    title: 'Role',
    dataIndex: 'roleType',
    key: 'roleType',
  },
  {
    title: 'Action',
    key: 'action',
    render: (_text: any, record: User) => (
      <>
        <EnableButton
          isVisible={!record.enabled && isEnableButtonVisible}
          onClick={() => enableUser(record.id)}
        />
        <DisableButton
          isVisible={record.enabled && isDisableButtonVisible}
          onClick={() => disableUser(record.id)}
        />
      </>
    ),
  },
];
