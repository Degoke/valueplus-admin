import { default as styles } from './UserFilter.module.scss';
import * as React from 'react';
import { Row, Col, Input } from 'antd';
import constants from 'src/utils/constants';
import Button from 'src/components/Button';
import { FilterOptions, UserPageOptions } from 'src/actions/users/types';

const { Search } = Input;

export interface OwnProps {
  filterBy: (options: FilterOptions) => void;
  getUsers: (options: UserPageOptions) => void;
}

const UserFilter: React.FunctionComponent<OwnProps> = ({
  filterBy,
  getUsers,
}) => {
  const [email, setEmail] = React.useState<string>(null);
  const [firstname, setFirstName] = React.useState<string>(null);
  const [lastname, setLastName] = React.useState<string>(null);

  const handleClear = () => {
    setEmail('');
    setFirstName('');
    setLastName('');
    getUsers({ size: constants.TRANSACTION_PAGE_SIZE });
  };

  const showClear = email || firstname || lastname;
  return (
    <Row gutter={24} justify="center" className={styles.wrapper}>
      <Col xs={24} xl={6} className={styles.gutter}>
        <Search
          placeholder="Search By Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          onSearch={() => filterBy({ email })}
        />
      </Col>
      <Col xs={24} xl={6} className={styles.gutter}>
        <Search
          placeholder="Search By First name"
          value={firstname}
          onChange={(e) => setFirstName(e.target.value)}
          onSearch={() => filterBy({ firstname })}
        />
      </Col>
      <Col xs={24} xl={6} className={styles.gutter}>
        <Search
          placeholder="Search By Last name"
          value={lastname}
          onChange={(e) => setLastName(e.target.value)}
          onSearch={() => filterBy({ lastname })}
        />
      </Col>
      <Col
        xs={8}
        xl={2}
        className={styles.gutter}
        style={{ justifyContent: 'center' }}
      >
        {showClear && <Button onClick={handleClear}>Clear</Button>}
      </Col>
    </Row>
  );
};

export default UserFilter;
