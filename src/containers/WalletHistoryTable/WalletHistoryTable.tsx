import * as React from 'react';
import format from 'date-fns/format';
import { Table } from 'antd';
import { TableProps } from 'antd/lib/table';
import * as types from 'src/actions/wallet/types';
import { Tag } from 'antd';
import Card from 'src/components/Card';

export interface WalletHistoryTableProps extends TableProps<{}> {
  walletHistory: types.WalletOverview[];
}
const WalletHistoryTable: React.FunctionComponent<WalletHistoryTableProps> = ({
  walletHistory,
  ...rest
}) => {
  const columns = [
    {
      title: 'Date',
      dataIndex: 'createdAt',
      key: 'createdAt',
      render: (date: Date) => format(new Date(date), 'PPpp'),
    },
    {
      title: 'Amount',
      dataIndex: 'amount',
      key: 'amount',
      render: (amount: number) => <div>NGN {amount.toFixed(2)}</div>,
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Type',
      dataIndex: 'type',
      key: 'type',
      render: (type: types.WalletReport) => (
        <Tag
          color={type === types.WalletReport.CREDIT ? 'green' : 'red'}
          key={status}
        >
          {type}
        </Tag>
      ),
    },
  ];
  return (
    <Card title={'Wallet History'}>
      <Table
        columns={columns}
        scroll={{ x: true }}
        dataSource={walletHistory?.map((wallet, key) => ({
          ...wallet,
          key,
        }))}
        {...rest}
        pagination={{
          position: ['bottomLeft'],
          hideOnSinglePage: true,
          ...rest.pagination,
        }}
      />
    </Card>
  );
};

export default WalletHistoryTable;
