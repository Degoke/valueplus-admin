import * as React from 'react';
export const useInfiniteScroll = (callback: () => void, last: boolean, ...args: any) => {
  const prevY = React.useRef<number>(0);
  const [ref, setRef] = React.useState(null);
  React.useEffect(() => {
    const observer = new IntersectionObserver(
      (entries) => {
        const firstEntry = entries[0];
        const y = firstEntry.boundingClientRect.y;
        if (prevY.current > y && last) {
          callback()
        }

        prevY.current = y;
      },
      { threshold: 1 },
    );
    if (ref) {
      observer.observe(ref);
    }
    return () => {
      if (ref) {
        observer.unobserve(ref);
      }
    };
  }, [ref, ...args]);
  return setRef;
}
