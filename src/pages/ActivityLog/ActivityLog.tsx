import * as React from 'react';
import { connect, MapDispatchToProps } from 'react-redux';
import DashboardPageTitle from 'src/components/DashboardTitle';
import { Helmet } from 'react-helmet';
import { destroy } from 'src/actions/audit/actions';
import Filter from 'src/containers/ActivityLogContainer/Filter';
import LogTable from 'src/containers/ActivityLogContainer/Table';

interface ActionProps {
  destroy: () => void;
}
type TActivityLog = ActionProps;
const ActivityLog: React.FunctionComponent<TActivityLog> = () => (
  <>
    <Helmet>
      <title>Dashboard | Activity Log</title>
    </Helmet>
    <DashboardPageTitle title={'Activity Log'} />
    <div>
      <Filter />
      <LogTable />
    </div>
  </>
);

const mapDispatchToProps: MapDispatchToProps<ActionProps, {}> = (dispatch) => {
  return {
    destroy: () => dispatch(destroy()),
  };
};
const connected = connect(null, mapDispatchToProps)(ActivityLog);

export default connected;
