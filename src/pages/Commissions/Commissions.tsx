import * as React from 'react';
import { connect, MapDispatchToProps } from 'react-redux';
import DashboardPageTitle from 'src/components/DashboardTitle';
import { Helmet } from 'react-helmet';
import Commission from 'src/containers/TransactionCommission';
import { destroy } from 'src/actions/settings/actions';

interface ActionProps {
  destroy: () => void;
}
type TCommissions = ActionProps;
const Commissions: React.FunctionComponent<TCommissions> = ({ destroy }) => {
  React.useEffect(() => {
    return destroy;
  }, []);
  return (
    <>
      <Helmet>
        <title>Dashboard | Commissions</title>
      </Helmet>
      <DashboardPageTitle title={'Commissions'} />
      <Commission />
    </>
  );
};

const mapDispatchToProps: MapDispatchToProps<ActionProps, {}> = (dispatch) => {
  return {
    destroy: () => dispatch(destroy()),
  };
};
const connected = connect(null, mapDispatchToProps)(Commissions);

export default connected;
