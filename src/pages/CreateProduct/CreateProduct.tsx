import * as React from 'react';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import { Helmet } from 'react-helmet';
import Card from 'src/components/Card';
import { IProduct } from 'src/actions/products/types';
import { createProduct } from 'src/actions/products';
import DashboardPageTitle from 'src/components/DashboardTitle';
import ProductForm from 'src/containers/ProductForm';
import { Row, Col, Alert } from 'antd';
import Button from 'src/components/Button';
import { Link } from 'react-router-dom';
import { Routes } from 'src/routes';

export interface OwnProps {}

export interface ActionProps {
  createProduct: (product: IProduct) => void;
}

export interface StateProps {
  isLoading: boolean;
  hasErrored: boolean;
  isSuccess: boolean;
  product: IProduct;
}

export type CreateProductProps = OwnProps & StateProps & ActionProps;
const CreateProduct: React.FunctionComponent<CreateProductProps> = ({
  product,
  hasErrored,
  isSuccess,
  isLoading,
  createProduct,
}) => {
  return (
    <div>
      <Helmet>
        <title>Dashboard | Create-Product</title>
      </Helmet>
      <DashboardPageTitle title={'Product'} />
      {hasErrored && (
        <Alert
          showIcon
          closable
          message={
            <>
              <div>image must not be blank</div>
              <div>product name must not be blank</div>
              <div>description must not be blank</div>
              <div>price must be greater than or equal to 1</div>
            </>
          }
          type="error"
        />
      )}
      {isSuccess && !product && (
        <Alert
          showIcon
          closable
          message="Product added successfully"
          type="success"
        />
      )}
      <Card
        title={
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <div>Create Product</div>
            <Link to={`${Routes.DASHBOARD}${Routes.MANAGE_PRODUCTS}`}>
              <Button>Back</Button>
            </Link>
          </div>
        }
      >
        <Row gutter={24} justify={'center'} align={'middle'}>
          <Col xs={24} sm={24} md={12}>
            <ProductForm
              buttonMsg={isSuccess ? 'Add another product' : 'Create product'}
              isLoading={isLoading}
              handleFormSubmit={createProduct}
            />
          </Col>
        </Row>
      </Card>
    </div>
  );
};

const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = ({
  products,
}) => ({
  isLoading: products.loading.product,
  isSuccess: products.success.product,
  hasErrored: products.error.product,
  product: products.product,
});

const mapDispatchToProps: MapDispatchToProps<ActionProps, OwnProps> = (
  dispatch,
) => {
  return {
    createProduct: (options) => dispatch(createProduct.request(options)),
  };
};
const connected = connect(mapStateToProps, mapDispatchToProps)(CreateProduct);

export default connected;
