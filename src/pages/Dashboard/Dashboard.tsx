import { default as styles } from './Dashboard.module.scss';
import * as React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import Sidebar from 'src/components/Sidebar';
import Navbar from 'src/containers/Navbar';
import { Layout } from 'antd';
import { Routes } from 'src/routes';
import ErrorPage from 'src/components/ErrorPage';
import Home from 'src/pages/Home';
import Profile from 'src/pages/Profile';
import { Helmet } from 'react-helmet';
import { getUser, currentUser } from 'src/actions/profile';
import { getTransactions } from 'src/actions/transactions';
import { TransactionsPageOptions } from 'src/actions/transactions/types';
import constants from 'src/utils/constants';
import Transactions from 'src/pages/Transactions';
import Orders from 'src/pages/Orders';
import SubAdmin from 'src/pages/SubAdmin';
import Users from 'src/pages/Users';
import Wallets from 'src/pages/Wallets';
import Products from 'src/pages/Products';
import Commissions from 'src/pages/Commissions';
import SuperAgents from 'src/pages/SuperAgents';
import ActivityLog from 'src/pages/ActivityLog';
import CreateProduct from 'src/pages/CreateProduct';
import { getWallet } from 'src/actions/wallet';
import { getSummary } from 'src/actions/summary';
import { getAuthorities } from 'src/actions/authorities';
import { getWalletHistoryFilter } from 'src/actions/wallet';
import { getEntityActionMap } from 'src/actions/audit';
import { User } from 'src/actions/users/types';
const { Content } = Layout;
interface ActionProps {
  getUser: () => void;
  getWallet: () => void;
  getSummary: () => void;
  getAuthorities: () => void;
  getEntityActionMap: () => void;
  getWalletHistory: (options: TransactionsPageOptions) => void;
  getTransactions: (options: TransactionsPageOptions) => void;
}

interface StateProps {
  user: User;
}

export type DashboardProps = StateProps & ActionProps;
export const Dashboard: React.FunctionComponent<DashboardProps> = ({
  user,
  getUser,
  getWallet,
  getSummary,
  getAuthorities,
  getTransactions,
  getWalletHistory,
  getEntityActionMap,
}) => {
  const { url } = useRouteMatch();

  React.useEffect(() => {
    getUser();
    getWallet();
    getSummary();
    getEntityActionMap();
    getAuthorities();
    getWalletHistory({ size: constants.TRANSACTION_PAGE_SIZE });
    getTransactions({ size: constants.TRANSACTION_PAGE_SIZE });
  }, []);

  return (
    <Layout>
      <Helmet>
        <title>Dashboard | Home</title>
      </Helmet>
      <Sidebar />
      <Layout>
        <Navbar user={user} />
        <Content className={styles.wrapper}>
          <Switch>
            <Route exact path={Routes.DASHBOARD}>
              <Home />
            </Route>
            <Route exact path={`${url}${Routes.TRANSACTIONS}`}>
              <Transactions />
            </Route>
            <Route exact path={`${url}${Routes.WALLETS}`}>
              <Wallets />
            </Route>
            <Route exact path={`${url}${Routes.MANAGE_ORDERS}`}>
              <Orders />
            </Route>
            <Route exact path={`${url}${Routes.MANAGE_USERS}`}>
              <Users />
            </Route>
            <Route exact path={`${url}${Routes.PROFILE}`}>
              <Profile />
            </Route>
            <Route exact path={`${url}${Routes.MANAGE_PRODUCTS}`}>
              <Products />
            </Route>
            <Route exact path={`${url}${Routes.MANAGE_PRODUCTS_CREATE}`}>
              <CreateProduct />
            </Route>
            <Route exact path={`${url}${Routes.CREATE_ADMIN}`}>
              <SubAdmin />
            </Route>
            <Route exact path={`${url}${Routes.SUPERAGENTS}`}>
              <SuperAgents />
            </Route>
            <Route exact path={`${url}${Routes.COMMISSIONS}`}>
              <Commissions />
            </Route>
            <Route exact path={`${url}${Routes.ACTIVITY_LOG}`}>
              <ActivityLog />
            </Route>
            <Route
              component={() => <ErrorPage fallbackRoute={Routes.DASHBOARD} />}
            />
          </Switch>
        </Content>
      </Layout>
    </Layout>
  );
};

const mapStateToProps: MapStateToProps<StateProps, {}, RootState> = (
  state,
) => ({
  user: currentUser(state),
});
const mapDispatchToProps: MapDispatchToProps<ActionProps, {}> = (dispatch) => ({
  getUser: () => dispatch(getUser.request()),
  getWallet: () => dispatch(getWallet.request()),
  getSummary: () => dispatch(getSummary.request()),
  getAuthorities: () => dispatch(getAuthorities.request()),
  getEntityActionMap: () => dispatch(getEntityActionMap.request()),
  getWalletHistory: (options) =>
    dispatch(getWalletHistoryFilter.request(options)),
  getTransactions: (options) => dispatch(getTransactions.request(options)),
});

const connected = connect(mapStateToProps, mapDispatchToProps)(Dashboard);

export default connected;
