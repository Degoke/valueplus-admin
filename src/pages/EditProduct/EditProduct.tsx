import { default as styles } from './EditProduct.module.scss';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import * as React from 'react';
import { Alert } from 'antd';
import Card from 'src/components/Card';
import { CloseOutlined } from '@ant-design/icons';
import { IProduct } from 'src/actions/products/types';
import ProductForm from 'src/containers/ProductForm';
import { updateProduct } from 'src/actions/products';

interface OwnProps {
  setIsOpen: (e: any) => void;
}

export interface ActionProps {
  updateProduct: (product: IProduct) => void;
}

export interface StateProps {
  isLoading: boolean;
  hasErrored: boolean;
  isSuccess: boolean;
  product: IProduct;
}

export type EditProductProps = OwnProps & StateProps & ActionProps;
export const EditProduct: React.FunctionComponent<EditProductProps> = ({
  hasErrored,
  product,
  isSuccess,
  isLoading,
  setIsOpen,
  updateProduct,
}) => {
  return (
    <Card
      title={
        <div className={styles.cardHeader}>
          <div>Update product</div>
          <CloseOutlined
            style={{ fontSize: 20 }}
            onClick={() => setIsOpen(false)}
          />
        </div>
      }
      className={styles.card}
    >
      {isSuccess && product && (
        <Alert
          showIcon
          closable
          message="Product updated successfully"
          type="success"
        />
      )}
      {hasErrored && (
        <Alert
          showIcon
          closable
          message="Error updating product"
          type="error"
        />
      )}
      {isLoading ? (
        'loading'
      ) : (
        <ProductForm
          product={product}
          buttonMsg={'Update product'}
          isLoading={isLoading}
          handleFormSubmit={updateProduct}
        />
      )}
    </Card>
  );
};

const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = ({
  products,
}) => ({
  isLoading: products.loading.product,
  isSuccess: products.success.product,
  hasErrored: products.error.product,
  product: products.product,
});

const mapDispatchToProps: MapDispatchToProps<ActionProps, OwnProps> = (
  dispatch,
) => ({ updateProduct: (options) => dispatch(updateProduct.request(options)) });
const connected = connect(mapStateToProps, mapDispatchToProps)(EditProduct);

export default connected;
