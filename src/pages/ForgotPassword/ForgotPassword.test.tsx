import * as React from 'react';
import { ForgotPassword, ForgotPasswordProps } from './ForgotPassword';
import { shallow, ShallowWrapper } from 'enzyme';
import FormInput from 'src/components/FormInput';
import Button from 'src/components/Button';
import ErrorState from 'src/components/ErrorState';

describe('<ForgotPassword /> ', () => {
  let defaultProps: ForgotPasswordProps;
  let component: () => ShallowWrapper;
  const curriedComponent = (props: ForgotPasswordProps) => () =>
    shallow(<ForgotPassword {...props} />);

  beforeEach(() => {
    defaultProps = {
      destroy: jest.fn(),
      onPasswordRecovery: jest.fn(),
      isLoading: false,
      hasErrored: false,
      passwordResetSuccessful: false,
    };
    component = curriedComponent(defaultProps);
  });

  it('renders without crashing', () => {
    const wrapper = component();
    expect(wrapper.exists()).toEqual(true);
    expect(wrapper).toMatchSnapshot();
  });

  it('renders without crashing on error', () => {
    const wrapper = component();
    wrapper.setProps({ hasErrored: true });
    expect(wrapper.find(ErrorState).length).toEqual(1);
    expect(wrapper).toMatchSnapshot();
  });

  it('Should have 1 input and 1 button', () => {
    const wrapper = component();
    expect(wrapper.find(FormInput).length).toEqual(1);
    expect(wrapper.find(Button).length).toEqual(1);
  });

  it('should have the recover password button disabled by default', () => {
    const wrapper = component();
    expect(wrapper.find(Button).prop('disabled')).toBeTruthy();
  });

  it('Should call onSubmit when form is submitted', () => {
    const wrapper = component();
    expect(
      wrapper.find('form').simulate('submit', { preventDefault: jest.fn() }),
    );
    expect(defaultProps.onPasswordRecovery).toHaveBeenCalled();
  });
});
