import { default as styles } from './ForgotPassword.module.scss';
import * as React from 'react';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { resetPassword, destroy } from 'src/actions/auth';
import { PasswordReset } from 'src/actions/auth/types';
import { RootState } from 'src/reducers/index';
import FormInput from 'src/components/FormInput';
import Button from 'src/components/Button';
import ErrorState from 'src/components/ErrorState';
import Title from 'src/components/Title';
import AuthBoard from 'src/components/AuthBoard';
import constants from 'src/utils/constants';
import { Routes } from 'src/routes';
import { Link } from 'react-router-dom';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Helmet } from 'react-helmet';
import { Alert } from 'antd';

const BACKGROUNDIMAGE = '/images/raw/password.jpg';

interface OwnProps {}

interface StateProps {
  isLoading: boolean;
  hasErrored: boolean;
  passwordResetSuccessful: boolean;
}

interface ActionProps {
  destroy: () => void;
  onPasswordRecovery: (crendential: PasswordReset) => void;
}

const validationSchema = Yup.object({
  email: Yup.string()
    .matches(constants.emailRegExp, 'Must be valid email')
    .required(),
});

export type ForgotPasswordProps = OwnProps & StateProps & ActionProps;

export const ForgotPassword: React.FunctionComponent<ForgotPasswordProps> = ({
  destroy,
  isLoading,
  hasErrored,
  onPasswordRecovery,
  passwordResetSuccessful,
}) => {
  React.useEffect(() => {
    return destroy;
  }, []);

  const formik = useFormik({
    validationSchema,
    initialValues: {
      email: '',
    },
    onSubmit: (values) => {
      onPasswordRecovery(values);
    },
  });
  const {
    errors,
    handleSubmit,
    handleChange,
    touched,
    values: { email },
  } = formik;

  return (
    <AuthBoard backgroundimage={BACKGROUNDIMAGE}>
      <Helmet>
        <title>Forgot Password</title>
      </Helmet>
      <div className={styles.mainContent}>
        {hasErrored && (
          <Alert
            showIcon
            closable
            message="Invalid Email or doesn't exists!"
            type="error"
          />
        )}
        <Title>Forgot Password</Title>
        <div className={styles.formContainer}>
          {!passwordResetSuccessful ? (
            <form onSubmit={handleSubmit} className={styles.form}>
              <FormInput
                value={email}
                onChange={handleChange}
                type="email"
                name="email"
                placeholder="Email"
              />
              {errors.email && touched.email && (
                <ErrorState>{errors.email}</ErrorState>
              )}
              <Button type={'submit'} isLoading={isLoading}>
                Recover password
              </Button>
            </form>
          ) : (
            <Alert
              showIcon
              message="Confirmation Sent"
              description={
                <div>
                  <strong>{email}</strong> Please check your email for password
                  reset instructions.
                </div>
              }
              type="success"
            />
          )}
          <p className={styles.login}>
            <Link to={Routes.HOME}>Login</Link>
          </p>
        </div>
      </div>
    </AuthBoard>
  );
};

const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = ({
  auth,
}) => ({
  isLoading: auth.isLoading,
  hasErrored: auth.hasErrored,
  passwordResetSuccessful: auth.passwordResetSuccessful,
});
const mapDispatchToProps: MapDispatchToProps<ActionProps, OwnProps> = (
  dispatch,
) => ({
  destroy: () => dispatch(destroy()),
  onPasswordRecovery: (crendential) =>
    dispatch(resetPassword.request(crendential)),
});

const connected = connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);

export default connected;
