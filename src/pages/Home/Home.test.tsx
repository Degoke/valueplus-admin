import * as React from 'react';
import { Home, HomeProps } from './Home';
import { shallow, ShallowWrapper } from 'enzyme';
import { ROLE_TYPE } from 'src/utils';

jest.dontMock('antd');

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: jest.fn(),
}));

describe('<Home /> ', () => {
  let defaultProps: HomeProps;
  let component: () => ShallowWrapper;
  const curriedComponent = (props: HomeProps) => () =>
    shallow(<Home {...props} />);
  const data = {
    id: 1,
    address: 'null',
    agentCode: 'null',
    authorities: ['blaa'],
    email: 'hkdvjhfj@gail.com',
    emailVerified: true,
    firstname: 'Dapo',
    lastname: 'Olowoeyo',
    link: 'null',
    phone: '860669427',
    photo: 'null',
    referralCode: 'jndjfhkjdhgkjsdf',
    roleType: ROLE_TYPE.SUPER_AGENT,
    superAgentCode: 'null',
    transactionTokenSet: false,
  };

  beforeEach(() => {
    defaultProps = {
      refreshTransaction: jest.fn(),
      transactions: null,
      transactionsLoading: false,
      user: data,
      wallet: null,
      summary: null,
      walletHistoryisLoading: false,
      walletHistory: null,
    };
    component = curriedComponent(defaultProps);
  });

  it('renders without crashing', () => {
    const wrapper = component();
    expect(wrapper.exists()).toEqual(true);
    expect(wrapper).toMatchSnapshot();
  });
});
