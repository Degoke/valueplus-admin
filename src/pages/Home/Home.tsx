import { default as styles } from './Home.module.scss';
import * as React from 'react';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import { Row, Col } from 'antd';
import DashboardPageTitle from 'src/components/DashboardTitle';
import OverviewCard from 'src/components/OverviewCard';
import { OverviewCardBackground } from 'src/components/OverviewCard/utils';
import { Helmet } from 'react-helmet';
import PaymentHistory from 'src/containers/PaymentHistory';
import { Transactions } from 'src/actions/transactions/types';
import { Wallet, WalletHistory } from 'src/actions/wallet/types';
import { Summary } from 'src/actions/summary/types';
import { SSummary } from 'src/actions/summary';
import WalletHistoryTable from 'src/containers/WalletHistoryTable';
import {
  refreshTransaction,
  STransactions,
  STransactionsLoading,
} from 'src/actions/transactions';
import { User } from 'src/actions/users/types';
import { currentUser } from 'src/actions/profile';
import {
  userWallet,
  userWalletHistory,
  isWalletHistoryLoading,
} from 'src/actions/wallet';

export interface OwnProps {}

interface StateProps {
  wallet: Wallet;
  summary: Summary;
  user: User;
  transactionsLoading: boolean;
  walletHistoryisLoading: boolean;
  walletHistory: WalletHistory;
  transactions: Transactions;
}

interface ActionProps {
  refreshTransaction: (ref: string) => void;
}

export type HomeProps = OwnProps & ActionProps & StateProps;
export const Home: React.FunctionComponent<HomeProps> = ({
  summary,
  wallet,
  transactions,
  walletHistory,
  refreshTransaction,
  transactionsLoading,
  walletHistoryisLoading,
}) => (
  <div>
    <Helmet>
      <title>Dashboard | Home</title>
    </Helmet>
    <DashboardPageTitle title={'Overview'} />
    <Row gutter={24}>
      <Col xs={24} sm={12} md={12} xl={4} className={styles.separator}>
        <OverviewCard
          title={'Wallet'}
          prefixIcon={
            <img src={'/images/svg/naira-white.svg'} alt="no-transaction" />
          }
          value={wallet?.amount}
        />
      </Col>
      <Col xs={24} sm={12} md={12} xl={4} className={styles.separator}>
        <OverviewCard
          background={OverviewCardBackground.GREEN}
          title={'Total No. of Agents'}
          value={summary?.totalAgents}
        />
      </Col>
      <Col xs={24} sm={12} md={12} xl={4} className={styles.separator}>
        <OverviewCard
          background={OverviewCardBackground.BLUE}
          title={'Total Payout'}
          prefixIcon={
            <img src={'/images/svg/naira-white.svg'} alt="no-transaction" />
          }
          value={summary?.totalApprovedWithdrawals}
        />
      </Col>
      <Col xs={24} sm={12} md={12} xl={4} className={styles.separator}>
        <OverviewCard
          background={OverviewCardBackground.RED}
          title={'Outstanding Payout'}
          prefixIcon={
            <img src={'/images/svg/naira-white.svg'} alt="no-transaction" />
          }
          value={summary?.totalPendingWithdrawal}
        />
      </Col>
      <Col xs={24} sm={12} md={12} xl={4} className={styles.separator}>
        <OverviewCard
          background={OverviewCardBackground.BLUE}
          title={'Total Product Sales'}
          prefixIcon={
            <img src={'/images/svg/naira-white.svg'} alt="no-transaction" />
          }
          value={summary?.totalProductSales}
        />
      </Col>
      <Col xs={24} sm={12} md={12} xl={4} className={styles.separator}>
        <OverviewCard
          background={OverviewCardBackground.GREEN}
          title={'Total Agent Profits'}
          prefixIcon={
            <img src={'/images/svg/naira-white.svg'} alt="no-transaction" />
          }
          value={summary?.totalProductAgentProfits}
        />
      </Col>
    </Row>
    <Row gutter={24}>
      <Col className={styles.separator} xs={24} xl={12}>
        <PaymentHistory
          scroll={{ x: true }}
          loading={transactionsLoading}
          pagination={false}
          refreshTransaction={refreshTransaction}
          transactions={transactions?.content.slice(0, 5)}
        />
      </Col>
      <Col className={styles.separator} xs={24} xl={12}>
        <WalletHistoryTable
          scroll={{ x: true }}
          loading={walletHistoryisLoading}
          pagination={false}
          walletHistory={walletHistory?.content.slice(0, 5)}
        />
      </Col>
    </Row>
  </div>
);
const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = (
  state,
) => ({
  user: currentUser(state),
  wallet: userWallet(state),
  summary: SSummary(state),
  walletHistory: userWalletHistory(state),
  walletHistoryisLoading: isWalletHistoryLoading(state),
  transactions: STransactions(state),
  transactionsLoading: STransactionsLoading(state),
});

const mapDispatchToProps: MapDispatchToProps<ActionProps, OwnProps> = (
  dispatch,
) => ({
  refreshTransaction: (ref) => dispatch(refreshTransaction.request(ref)),
});

const connected = connect(mapStateToProps, mapDispatchToProps)(Home);

export default connected;
