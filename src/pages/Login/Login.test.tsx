import * as React from 'react';
import { Login, LoginProps } from './Login';
import { shallow, ShallowWrapper } from 'enzyme';
import FormInput from 'src/components/FormInput';
import Button from 'src/components/Button';

describe('<Login /> ', () => {
  let defaultProps: LoginProps;
  let component: () => ShallowWrapper;
  const curriedComponent = (props: LoginProps) => () =>
    shallow(<Login {...props} />);

  beforeEach(() => {
    defaultProps = {
      destroy: jest.fn(),
      onLogin: jest.fn(),
      isLoading: false,
      hasErrored: false,
    };
    component = curriedComponent(defaultProps);
  });

  it('renders without crashing', () => {
    const wrapper = component();
    expect(wrapper.exists()).toEqual(true);
    expect(wrapper).toMatchSnapshot();
  });

  it('Should have 2 input and 1 button', () => {
    const wrapper = component();
    expect(wrapper.find(FormInput).length).toEqual(2);
    expect(wrapper.find(Button).length).toEqual(1);
  });

  it('should have the login button disabled by default', () => {
    const wrapper = component();
    expect(wrapper.find(Button).prop('disabled')).toBeTruthy();
  });

  it('Should call onSubmit when form is submitted', () => {
    const wrapper = component();
    expect(
      wrapper.find('form').simulate('submit', { preventDefault: jest.fn() }),
    );
    expect(defaultProps.onLogin).toHaveBeenCalled();
  });
});
