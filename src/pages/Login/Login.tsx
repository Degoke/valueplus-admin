import { default as styles } from './Login.module.scss';
import * as React from 'react';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { authenticateUser, destroy } from 'src/actions/auth';
import { LoginDetails } from 'src/actions/auth/types';
import { RootState } from 'src/reducers/index';
import FormInput from 'src/components/FormInput';
import Button from 'src/components/Button';
import ErrorState from 'src/components/ErrorState';
import Title from 'src/components/Title';
import { Link } from 'react-router-dom';
import { Routes } from 'src/routes';
import AuthBoard from 'src/components/AuthBoard';
import constants from 'src/utils/constants';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Helmet } from 'react-helmet';
import { Alert } from 'antd';

const BACKGROUNDIMAGE = '/images/raw/man-1.jpg';

interface OwnProps {}

interface StateProps {
  isLoading: boolean;
  hasErrored: boolean;
}

interface ActionProps {
  destroy: () => void;
  onLogin: (loginDetails: LoginDetails) => void;
}

const validationSchema = Yup.object({
  email: Yup.string()
    .matches(constants.emailRegExp, 'Must be valid email')
    .required('Must be valid email'),
  password: Yup.string()
    .min(8, 'Password cannot be less than 8 characters')
    .required('Password cannot be empty'),
});

export type LoginProps = OwnProps & StateProps & ActionProps;

export const Login: React.FunctionComponent<LoginProps> = ({
  onLogin,
  destroy,
  isLoading,
  hasErrored,
}) => {
  React.useEffect(() => {
    return destroy;
  }, []);

  const formik = useFormik({
    validationSchema,
    initialValues: {
      email: '',
      password: '',
    },
    onSubmit: (values: LoginDetails) => {
      onLogin(values);
    },
  });
  const { errors, handleSubmit, handleChange, touched, values } = formik;
  const { email, password } = values;
  return (
    <AuthBoard backgroundimage={BACKGROUNDIMAGE}>
      <Helmet>
        <title>Login</title>
      </Helmet>
      <div className={styles.mainContent}>
        {hasErrored && (
          <Alert
            showIcon
            closable
            message="Invalid Email or Password!"
            type="error"
          />
        )}
        <Title>Welcome Back Admin!</Title>
        <div className={styles.formContainer}>
          <form onSubmit={handleSubmit} className={styles.form}>
            <FormInput
              value={email}
              onChange={handleChange}
              name="email"
              placeholder="Email"
            />
            {errors.email && touched.email && (
              <ErrorState>{errors.email}</ErrorState>
            )}
            <FormInput
              value={password}
              autoComplete="password"
              onChange={handleChange}
              type="password"
              name="password"
              placeholder="Password"
            />
            {errors.password && touched.password && (
              <ErrorState>{errors.password}</ErrorState>
            )}
            <Button type={'submit'} isLoading={isLoading}>
              Login
            </Button>
          </form>
          <p className={styles.forgotPassword}>
            <Link to={Routes.FORGOT_PASSWORD}>Forgot Password ?</Link>
          </p>
        </div>
      </div>
    </AuthBoard>
  );
};

const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = ({
  auth,
}) => ({
  isLoading: auth.isLoading,
  hasErrored: auth.hasErrored,
});
const mapDispatchToProps: MapDispatchToProps<ActionProps, OwnProps> = (
  dispatch,
) => ({
  destroy: () => dispatch(destroy()),
  onLogin: (loginDetails) => dispatch(authenticateUser.request(loginDetails)),
});

const connected = connect(mapStateToProps, mapDispatchToProps)(Login);

export default connected;
