import * as React from 'react';
import { connect, MapDispatchToProps } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { authLogout } from 'src/actions/auth';
import { Routes } from 'src/routes';

interface OwnProps {}
interface ActionProps {
  onLogout: () => void;
}

export type LogoutProps = OwnProps & ActionProps;
export const Logout: React.FunctionComponent<LogoutProps> = ({ onLogout }) => {
  React.useEffect(() => {
    onLogout();
  });

  return <Redirect to={Routes.HOME} />;
};

const mapDispatchToProps: MapDispatchToProps<ActionProps, OwnProps> = (
  dispatch,
) => ({ onLogout: () => dispatch(authLogout()) });

const connected = connect(null, mapDispatchToProps)(Logout);

export default connected;
