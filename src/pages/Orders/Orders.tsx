import * as React from 'react';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import { Helmet } from 'react-helmet';
import Card from 'src/components/Card';
import DashboardPageTitle from 'src/components/DashboardTitle';
import SalesHistoryTable from 'src/containers/SalesHistoryTable';
import * as types from 'src/actions/products/types';
import {
  destroy,
  getProductOrders,
  updateProductOrderStatus,
  getFilteredProductOrders,
} from 'src/actions/products';
import { currentUser } from 'src/actions/profile';
import constants from 'src/utils/constants';
import SalesHistoryFilter from 'src/containers/SalesHistoryFilter';

export interface ActionProps {
  destroy: () => void;
  updateProductOrderStatus: (update: types.ProductStatusUpdate) => void;
  getFilteredProductOrders: (options: types.ProductOrdersOptions) => void;
  getProductOrders: (options: types.ProductOrdersOptions) => void;
}

export interface StateProps {
  isLoading: boolean;
  products: types.ProductOrderItems;
}

export type OrdersProps = StateProps & ActionProps;
const Orders: React.FunctionComponent<OrdersProps> = ({
  destroy,
  products,
  isLoading,
  getProductOrders,
  updateProductOrderStatus,
  getFilteredProductOrders,
}) => {
  React.useEffect(() => {
    getProductOrders({ size: constants.PRODUCTS_ORDERS_SIZE });
    return destroy;
  }, []);
  return (
    <div>
      <Helmet>
        <title>Dashboard | Orders</title>
      </Helmet>
      <DashboardPageTitle title={'Orders Overview'} />
      <Card title={'Orders'}>
        <SalesHistoryFilter
          filterBy={getFilteredProductOrders}
          getProductOrders={getProductOrders}
        />
        <SalesHistoryTable
          loading={isLoading}
          onChange={(page: { pageSize: number; current: number }) =>
            getProductOrders({
              size: page.pageSize,
              page: page.current - 1,
            })
          }
          pagination={{
            showSizeChanger: true,
            total: products?.totalElements,
            current: products?.number + 1,
            pageSize: products?.size,
            defaultPageSize: products?.size,
          }}
          orders={products?.content}
          updateProductStatus={updateProductOrderStatus}
        />
      </Card>
    </div>
  );
};

const mapStateToProps: MapStateToProps<StateProps, {}, RootState> = (
  state,
) => ({
  user: currentUser(state),
  isLoading: state.products.loading.productOrders,
  products: state.products.productOrders,
});

const mapDispatchToProps: MapDispatchToProps<ActionProps, {}> = (dispatch) => ({
  destroy: () => dispatch(destroy()),
  getProductOrders: (options) => dispatch(getProductOrders.request(options)),
  updateProductOrderStatus: (options) =>
    dispatch(updateProductOrderStatus.request(options)),
  getFilteredProductOrders: (options) =>
    dispatch(getFilteredProductOrders.request(options)),
});

const connected = connect(mapStateToProps, mapDispatchToProps)(Orders);

export default connected;
