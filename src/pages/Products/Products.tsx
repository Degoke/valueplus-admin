import * as React from 'react';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import { Helmet } from 'react-helmet';
import Card from 'src/components/Card';
import Dialog from 'src/components/Dialog';
import constants from 'src/utils/constants';
import * as types from 'src/actions/products/types';
import { destroy, getProducts, getProduct } from 'src/actions/products';
import ProductTable from 'src/containers/ProductTable';
import EditProduct from 'src/pages/EditProduct';
import DashboardPageTitle from 'src/components/DashboardTitle';
import CreateProductButton from 'src/containers/CreateProduct';

export interface OwnProps {}

export interface ActionProps {
  destroy: () => void;
  getProduct: (id: number) => void;
  getProducts: (options: types.ProductOrdersOptions) => void;
}

export interface StateProps {
  products: types.Products;
}

export type ProductsProps = OwnProps & StateProps & ActionProps;
const Products: React.FunctionComponent<ProductsProps> = ({
  destroy,
  getProduct,
  getProducts,
}) => {
  const [isOpen, setIsOpen] = React.useState<boolean>(false);
  React.useEffect(() => {
    getProducts({ size: constants.PRODUCTS_ORDERS_SIZE });
    return destroy;
  }, []);

  const handleProductEdit = React.useCallback((id: number) => {
    getProduct(id);
    setIsOpen(true);
  }, []);

  return (
    <div>
      <Helmet>
        <title>Dashboard | Product</title>
      </Helmet>
      <DashboardPageTitle title={'Product'} />
      <CreateProductButton />
      <Card title={'All Products'}>
        <ProductTable handleProductEdit={handleProductEdit} />
      </Card>
      <Dialog isOpen={isOpen} onClose={() => setIsOpen(false)}>
        <EditProduct setIsOpen={setIsOpen} />
      </Dialog>
    </div>
  );
};

const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = ({
  products,
}) => ({
  products: products.products,
  productsIsLoading: products.loading.products,
});

const mapDispatchToProps: MapDispatchToProps<ActionProps, OwnProps> = (
  dispatch,
) => {
  return {
    destroy: () => dispatch(destroy()),
    getProduct: (id) => dispatch(getProduct.request(id)),
    getProducts: (options) => dispatch(getProducts.request(options)),
  };
};
const connected = connect(mapStateToProps, mapDispatchToProps)(Products);

export default connected;
