import { default as styles } from './Profile.module.scss';
import * as React from 'react';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import DashboardPageTitle from 'src/components/DashboardTitle';
import PersonalDetails from 'src/containers/PersonalDetails';
import PasswordChange from 'src/containers/PasswordChange';
import { Helmet } from 'react-helmet';
import * as types from 'src/actions/profile/types';
import {
  destroy,
  currentUser,
  profileLoading,
  profileSuccess,
  profileError,
} from 'src/actions/profile';
import { Alert } from 'antd';
import { User } from 'src/actions/users/types';

interface OwnProps {}
interface StateProps {
  success: types.Success;
  loading: types.Loading;
  error: types.Error;
  user: User;
}

interface ActionProps {
  destroy: () => void;
}

export type ProfileProps = OwnProps & StateProps & ActionProps;

const Profile: React.FunctionComponent<ProfileProps> = ({
  loading,
  success,
  error,
  user,
  destroy,
}) => {
  React.useEffect(() => {
    return destroy;
  }, []);

  return (
    <>
      <Helmet>
        <title>Dashboard | Profile</title>
      </Helmet>
      {success.profileUpdate && (
        <Alert
          showIcon
          closable
          message="Profile updated successfully"
          type="success"
        />
      )}
      {success.passwordUpdate && (
        <Alert
          showIcon
          closable
          message="Password updated successfully"
          type="success"
        />
      )}
      {error.profileUpdate && (
        <Alert
          showIcon
          closable
          message="Error updating profile"
          type="error"
        />
      )}
      {error.passwordUpdate && (
        <Alert
          showIcon
          closable
          message="Error changing Password!"
          type="error"
        />
      )}
      <DashboardPageTitle title={'Your Profile'} />
      <div className={styles.gutter}>
        <PersonalDetails isLoading={loading.profileUpdate} user={user} />
      </div>
      <div className={styles.gutter}>
        <PasswordChange isLoading={loading.passwordUpdate} />
      </div>
    </>
  );
};

const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = (
  state,
) => ({
  user: currentUser(state),
  error: profileError(state),
  loading: profileLoading(state),
  success: profileSuccess(state),
});

const mapDispatchToProps: MapDispatchToProps<ActionProps, OwnProps> = (
  dispatch,
) => ({
  destroy: () => dispatch(destroy()),
});

const connected = connect(mapStateToProps, mapDispatchToProps)(Profile);

export default connected;
