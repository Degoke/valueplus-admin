import { default as styles } from './ResetPassword.module.scss';
import * as React from 'react';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { newPassword, destroy } from 'src/actions/auth';
import * as types from 'src/actions/auth/types';
import { RootState } from 'src/reducers/index';
import FormInput from 'src/components/FormInput';
import Button from 'src/components/Button';
import ErrorState from 'src/components/ErrorState';
import Title from 'src/components/Title';
import AuthBoard from 'src/components/AuthBoard';
import { Routes } from 'src/routes';
import { Link, useParams } from 'react-router-dom';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Helmet } from 'react-helmet';
import { Alert } from 'antd';

const BACKGROUNDIMAGE = '/images/raw/password.jpg';

interface OwnProps {}

interface ParamTypes {
  slug: string;
}

interface StateProps {
  isLoading: boolean;
  hasErrored: boolean;
  passwordResetSuccessful: boolean;
}

interface ActionProps {
  destroy: () => void;
  onPasswordReset: (crendential: types.NewPassword) => void;
}

const validationSchema = Yup.object({
  password: Yup.string()
    .min(8, 'Password cannot be less than 8 characters')
    .matches(/[a-z]/, 'Password must have lowercase')
    .matches(/[A-Z]/, 'Password must have at least one uppercase')
    .matches(/\d/, 'Password must have numbers')
    .matches(/[a-z]/, 'Password must have lowercase')
    .required('Password cannot be empty'),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('password'), null], 'Passwords must match')
    .required('Passwords must match'),
});

export type ResetPasswordProps = OwnProps & StateProps & ActionProps;

export const ResetPassword: React.FunctionComponent<ResetPasswordProps> = ({
  destroy,
  isLoading,
  hasErrored,
  onPasswordReset,
  passwordResetSuccessful,
}) => {
  const { slug } = useParams<ParamTypes>();
  React.useEffect(() => {
    return destroy;
  }, []);

  const formik = useFormik({
    validationSchema,
    initialValues: {
      password: '',
      confirmPassword: '',
    },
    onSubmit: (values, { resetForm }) => {
      onPasswordReset({
        newPassword: values.password,
        resetToken: slug,
      });
      resetForm();
    },
  });
  const {
    errors,
    handleSubmit,
    handleChange,
    touched,
    values: { password, confirmPassword },
  } = formik;

  return (
    <AuthBoard backgroundimage={BACKGROUNDIMAGE}>
      <Helmet>
        <title>Reset Password</title>
      </Helmet>
      <div className={styles.mainContent}>
        {hasErrored && (
          <Alert
            showIcon
            closable
            message="Link expired! Request to change password again."
            type="error"
          />
        )}
        <Title>Reset Password</Title>
        <div className={styles.formContainer}>
          {!passwordResetSuccessful ? (
            <form onSubmit={handleSubmit} className={styles.form}>
              <FormInput
                value={password}
                onChange={handleChange}
                autoComplete="password"
                type="password"
                name="password"
                placeholder="Password"
              />
              {errors.password && touched.password && (
                <ErrorState>{errors.password}</ErrorState>
              )}
              <FormInput
                type={'password'}
                value={confirmPassword}
                onChange={handleChange}
                autoComplete="confirm-password"
                name="confirmPassword"
                placeholder="Confirm Password"
              />
              {errors.confirmPassword && touched.confirmPassword && (
                <ErrorState>{errors.confirmPassword}</ErrorState>
              )}
              <Button type={'submit'} isLoading={isLoading}>
                Reset password
              </Button>
              <p className={styles.login}>
                <Link to={Routes.HOME}>Login</Link>
              </p>
            </form>
          ) : (
            <Alert
              showIcon
              message="Password reset successfully"
              description={
                <div>
                  Proceed to{' '}
                  <Link to={Routes.HOME}>
                    <strong>Login</strong>
                  </Link>
                </div>
              }
              type="success"
            />
          )}
        </div>
      </div>
    </AuthBoard>
  );
};

const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = ({
  auth,
}) => ({
  isLoading: auth.isLoading,
  hasErrored: auth.hasErrored,
  passwordResetSuccessful: auth.passwordResetSuccessful,
});
const mapDispatchToProps: MapDispatchToProps<ActionProps, OwnProps> = (
  dispatch,
) => {
  return {
    destroy: () => dispatch(destroy()),
    onPasswordReset: (crendential) =>
      dispatch(newPassword.request(crendential)),
  };
};

const connected = connect(mapStateToProps, mapDispatchToProps)(ResetPassword);

export default connected;
