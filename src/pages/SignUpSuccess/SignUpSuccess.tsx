import { default as styles } from './SignUpSuccess.module.scss';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { Routes } from 'src/routes';
import { Result, Button } from 'antd';
import { SmileOutlined } from '@ant-design/icons';
import { User } from 'src/actions/users/types';
import hideable from 'src/utils/hideable';

export interface OwnProps {
  user: User;
}

export const SignUpSuccess: React.FunctionComponent<OwnProps> = ({ user }) => {
  return (
    <div className={styles.wrapper}>
      <Result
        icon={<SmileOutlined />}
        title={
          <div>
            Account for <strong>{user?.firstname.toUpperCase()}!</strong> has
            been created Successfully. Login credentials has been sent to{' '}
            <strong>{user?.email}</strong>
          </div>
        }
        extra={
          <Link to={Routes.DASHBOARD}>
            <Button>OK</Button>
          </Link>
        }
      />
    </div>
  );
};

export default hideable(SignUpSuccess);
