import * as React from 'react';
import { connect, MapDispatchToProps } from 'react-redux';
import DashboardPageTitle from 'src/components/DashboardTitle';
import { Helmet } from 'react-helmet';
import Accordion from 'src/components/Accordion';
import { destroy, getFilteredUsers } from 'src/actions/users';
import AdminTable from 'src/containers/SubAdminContainer/SubAdminTable';
import CreateSubAdmin from 'src/containers/SubAdminContainer/CreateSubAdmin';
import { FilterOptions } from 'src/actions/users/types';
import { ROLE_TYPE } from 'src/utils';

interface ActionProps {
  destroy: () => void;
  getAdmins: (options: FilterOptions) => void;
}

type TSubAdmin = ActionProps;
const SubAdmin: React.FunctionComponent<TSubAdmin> = ({
  destroy,
  getAdmins,
}) => {
  React.useEffect(() => {
    getAdmins({ roleType: ROLE_TYPE.ADMIN, size: 10 });
    return destroy;
  }, []);

  return (
    <>
      <Helmet>
        <title>Dashboard | SubAdmin</title>
      </Helmet>
      <DashboardPageTitle title={'Sub Admins'} />
      <Accordion
        defaultActiveKey={[0]}
        contents={[
          {
            header: 'Admins',
            body: <AdminTable />,
          },
          {
            header: 'Create Admin',
            body: <CreateSubAdmin />,
          },
        ]}
      />
    </>
  );
};

const mapDispatchToProps: MapDispatchToProps<ActionProps, {}> = (dispatch) => {
  return {
    destroy: () => dispatch(destroy()),
    getAdmins: (options) => dispatch(getFilteredUsers.request(options)),
  };
};
const connected = connect(null, mapDispatchToProps)(SubAdmin);

export default connected;
