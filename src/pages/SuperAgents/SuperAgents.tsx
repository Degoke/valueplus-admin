import * as React from 'react';
import { RootState } from 'src/reducers/index';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import DashboardPageTitle from 'src/components/DashboardTitle';
import { Helmet } from 'react-helmet';
import {
  destroy,
  getSuperAgents,
  SSuperAgentActiveUsers,
  SSuperAgentUsersLoading,
  getSuperAgentActiveUsers,
} from 'src/actions/superAgents';
import CreateSuperAgent from 'src/containers/SuperAgentsContainer/CreateSuperAgent';
import constants from 'src/utils/constants';
import { PaginationPageOptions } from 'src/utils';
import Accordion from 'src/components/Accordion';
import SuperAgentsTable from 'src/containers/SuperAgentsContainer/SuperAgentsTable';
import { Users } from 'src/actions/users/types';
import SuperAgentUsersTable from 'src/containers/SuperAgentsContainer/SuperAgentUsers';
import { FilterOptions } from 'src/actions/superAgents/types';

export interface StateProps {
  isLoadingSuperAgentUsers: boolean;
  agentUsers: Users;
}

interface ActionProps {
  destroy: () => void;
  getSuperAgents: (options: PaginationPageOptions) => void;
  filterAgentUsers: (options: FilterOptions) => void;
}
type TSuperAgents = ActionProps & StateProps;
const SuperAgents: React.FunctionComponent<TSuperAgents> = ({
  destroy,
  agentUsers,
  getSuperAgents,
  filterAgentUsers,
  isLoadingSuperAgentUsers,
}) => {
  React.useEffect(() => {
    getSuperAgents({ size: constants.PRODUCTS_ORDERS_SIZE });
    return destroy;
  }, []);
  return (
    <>
      <Helmet>
        <title>Dashboard | SuperAgents</title>
      </Helmet>
      <DashboardPageTitle title={'Super Agents'} />
      <Accordion
        defaultActiveKey={[0]}
        contents={[
          {
            header: 'New Super Agent',
            body: <CreateSuperAgent />,
          },
          {
            header: 'View All Super Agents',
            body: <SuperAgentsTable />,
          },
          {
            header: 'View Users for a Super Agent',
            body: (
              <SuperAgentUsersTable
                filterBy={filterAgentUsers}
                loading={isLoadingSuperAgentUsers}
                agents={agentUsers?.content}
                onChange={(page: { pageSize: number; current: number }) =>
                  getSuperAgents({
                    size: page.pageSize,
                    page: page.current - 1,
                  })
                }
                pagination={{
                  showSizeChanger: true,
                  total: agentUsers?.totalElements,
                  current: agentUsers?.number + 1,
                  pageSize: agentUsers?.size,
                  defaultPageSize: agentUsers?.size,
                }}
              />
            ),
          },
        ]}
      />
    </>
  );
};

const mapStateToProps: MapStateToProps<StateProps, {}, RootState> = (
  state,
) => ({
  agentUsers: SSuperAgentActiveUsers(state),
  isLoadingSuperAgentUsers: SSuperAgentUsersLoading(state),
});

const mapDispatchToProps: MapDispatchToProps<ActionProps, {}> = (dispatch) => {
  return {
    destroy: () => dispatch(destroy()),
    getSuperAgents: (options) => dispatch(getSuperAgents.request(options)),
    filterAgentUsers: (options) =>
      dispatch(getSuperAgentActiveUsers.request(options)),
  };
};
const connected = connect(mapStateToProps, mapDispatchToProps)(SuperAgents);

export default connected;
