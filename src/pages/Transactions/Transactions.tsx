import * as React from 'react';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import { Row, Col } from 'antd';
import DashboardPageTitle from 'src/components/DashboardTitle';
import { Helmet } from 'react-helmet';
import PaymentHistory from 'src/containers/PaymentHistory';
import * as types from 'src/actions/transactions/types';
import {
  getTransactions,
  refreshTransaction,
  getFilteredTransactions,
  STransactions,
  STransactionsLoading,
} from 'src/actions/transactions';
import TransactionFilter from 'src/containers/TransactionFilter';

export interface OwnProps {}

interface StateProps {
  transactionsLoading: boolean;
  transactions: types.Transactions;
}

export interface ActionProps {
  getFilteredTransactions: (options: types.TransactionFilterOptions) => void;
  getTransactions: (options: types.TransactionsPageOptions) => void;
  refreshTransaction: (ref: string) => void;
}

export type TransactionsProps = OwnProps & ActionProps & StateProps;
export const Transactions: React.FunctionComponent<TransactionsProps> = ({
  transactions,
  getTransactions,
  refreshTransaction,
  transactionsLoading,
  getFilteredTransactions,
}) => (
  <div>
    <Helmet>
      <title>Dashboard | Transactions</title>
    </Helmet>
    <DashboardPageTitle title={'Transactions Overview'} />
    <Row gutter={24}>
      <Col xs={24}>
        <TransactionFilter
          filterBy={getFilteredTransactions}
          getTransactions={getTransactions}
        />
        <PaymentHistory
          loading={transactionsLoading}
          onChange={(page) =>
            getTransactions({
              size: page.pageSize,
              page: page.current - 1,
            })
          }
          refreshTransaction={refreshTransaction}
          transactions={transactions?.content}
          pagination={{
            showSizeChanger: true,
            total: transactions?.totalElements,
            current: transactions?.number + 1,
            pageSize: transactions?.size,
            defaultPageSize: transactions?.size,
          }}
        />
      </Col>
    </Row>
  </div>
);

const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = (
  state,
) => ({
  transactions: STransactions(state),
  transactionsLoading: STransactionsLoading(state),
});

const mapDispatchToProps: MapDispatchToProps<ActionProps, OwnProps> = (
  dispatch,
) => {
  return {
    getTransactions: (options) => dispatch(getTransactions.request(options)),
    refreshTransaction: (ref) => dispatch(refreshTransaction.request(ref)),
    getFilteredTransactions: (options) =>
      dispatch(getFilteredTransactions.request(options)),
  };
};

const connected = connect(mapStateToProps, mapDispatchToProps)(Transactions);

export default connected;
