import * as React from 'react';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import { Helmet } from 'react-helmet';
import Card from 'src/components/Card';
import DashboardPageTitle from 'src/components/DashboardTitle';
import UserTable from 'src/containers/UserTable';
import * as types from 'src/actions/users/types';
import {
  destroy,
  getUsers,
  getFilteredUsers,
  getAllUsers,
  isUsersLoading,
} from 'src/actions/users';
import constants from 'src/utils/constants';
import UserFilter from 'src/containers/UserTableFilter';
import SalesHistoryTable from 'src/containers/SalesHistoryTable';
import {
  ProductOrderItems,
  ProductStatusUpdate,
  ProductOrdersOptions,
  UserOrderFilter,
} from 'src/actions/products/types';
import {
  getProductOrders,
  getUserOrders,
  updateProductOrderStatus,
} from 'src/actions/products';

export interface ActionProps {
  destroy: () => void;
  getUsers: (options: types.UserPageOptions) => void;
  filterUser: (options: types.FilterOptions) => void;
  getUserOrders: (options: UserOrderFilter) => void;
  updateProductOrderStatus: (update: ProductStatusUpdate) => void;
  getProductOrders: (options: ProductOrdersOptions) => void;
}

export interface StateProps {
  isLoading: boolean;
  users: types.Users;
  products: ProductOrderItems;
}

export type UserProps = StateProps & ActionProps;
const User: React.FunctionComponent<UserProps> = ({
  destroy,
  getUsers,
  products,
  isLoading,
  filterUser,
  getUserOrders,
  updateProductOrderStatus,
}) => {
  React.useEffect(() => {
    getUsers({ size: constants.PRODUCTS_ORDERS_SIZE });
    return destroy;
  }, []);
  const [expandedRowKeys, setExpandedRowKeys] = React.useState<any[]>([]);

  const onTableRowExpand = (expanded: boolean, record: types.User) => {
    const keys = [];
    if (expanded) {
      keys.push(record.key);
    }
    setExpandedRowKeys(keys);
    getUserOrders({ agentId: record.id });
  };

  const expandedRowRender = (record: types.User) => (
    <SalesHistoryTable
      loading={isLoading}
      onChange={(page: { pageSize: number; current: number }) =>
        getUserOrders({
          agentId: record.id,
          size: page.pageSize,
          page: page.current - 1,
        })
      }
      pagination={{
        showSizeChanger: true,
        total: products?.totalElements,
        current: products?.number + 1,
        pageSize: products?.size,
        defaultPageSize: products?.size,
      }}
      orders={products?.content}
      updateProductStatus={updateProductOrderStatus}
    />
  );

  return (
    <div>
      <Helmet>
        <title>Dashboard | Users</title>
      </Helmet>
      <DashboardPageTitle title={'Users Overview'} />
      <UserFilter getUsers={getUsers} filterBy={filterUser} />
      <Card>
        <UserTable
          expandable={{
            expandedRowRender,
            expandedRowKeys,
            onExpand: onTableRowExpand,
          }}
        />
      </Card>
    </div>
  );
};

const mapStateToProps: MapStateToProps<StateProps, {}, RootState> = (
  state,
) => ({
  users: getAllUsers(state),
  isLoading: isUsersLoading(state),
  products: state.products.userOrders,
});
const mapDispatchToProps: MapDispatchToProps<ActionProps, {}> = (dispatch) => {
  return {
    destroy: () => dispatch(destroy()),
    getUsers: (options) => dispatch(getUsers.request(options)),
    updateProductOrderStatus: (options) =>
      dispatch(updateProductOrderStatus.request(options)),
    getProductOrders: (options) => dispatch(getProductOrders.request(options)),
    getUserOrders: (options) => dispatch(getUserOrders.request(options)),
    filterUser: (options) => dispatch(getFilteredUsers.request(options)),
  };
};
const connected = connect(mapStateToProps, mapDispatchToProps)(User);

export default connected;
