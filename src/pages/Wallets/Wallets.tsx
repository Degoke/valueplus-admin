import * as React from 'react';
import { connect, MapStateToProps, MapDispatchToProps } from 'react-redux';
import { RootState } from 'src/reducers/index';
import { Row, Col } from 'antd';
import DashboardPageTitle from 'src/components/DashboardTitle';
import { Helmet } from 'react-helmet';
import { TransactionsPageOptions } from 'src/actions/transactions/types';
import { WalletHistory } from 'src/actions/wallet/types';
import {
  getWalletHistoryFilter,
  userWalletHistory,
  isWalletHistoryLoading,
} from 'src/actions/wallet';
import WalletHistoryTable from 'src/containers/WalletHistoryTable';

export interface OwnProps {}

interface StateProps {
  walletHistory: WalletHistory;
  walletHistoryisLoading: boolean;
}

export interface ActionProps {
  getWalletHistory: (options: TransactionsPageOptions) => void;
}

export type WalletsProps = OwnProps & ActionProps & StateProps;
export const Wallets: React.FunctionComponent<WalletsProps> = ({
  walletHistory,
  getWalletHistory,
  walletHistoryisLoading,
}) => {
  return (
    <div>
      <Helmet>
        <title>Dashboard | Wallets</title>
      </Helmet>
      <DashboardPageTitle title={'Wallet History'} />
      <Row gutter={24}>
        <Col xs={24}>
          <WalletHistoryTable
            loading={walletHistoryisLoading}
            onChange={(page) =>
              getWalletHistory({
                size: page.pageSize,
                page: page.current - 1,
              })
            }
            walletHistory={walletHistory?.content}
            pagination={{
              showSizeChanger: true,
              total: walletHistory?.totalElements,
              current: walletHistory?.number + 1,
              pageSize: walletHistory?.size,
              defaultPageSize: walletHistory?.size,
            }}
          />
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps: MapStateToProps<StateProps, OwnProps, RootState> = (
  state,
) => ({
  walletHistory: userWalletHistory(state),
  walletHistoryisLoading: isWalletHistoryLoading(state),
});

const mapDispatchToProps: MapDispatchToProps<ActionProps, OwnProps> = (
  dispatch,
) => {
  return {
    getWalletHistory: (options) =>
      dispatch(getWalletHistoryFilter.request(options)),
  };
};

const connected = connect(mapStateToProps, mapDispatchToProps)(Wallets);

export default connected;
