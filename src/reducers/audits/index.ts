import * as types from 'src/actions/audit/types';
import { reqState, errState, resState } from 'src/utils';


export const initialState: types.State = {
  entityActionMap: null,
  audit: null,
  success: {
    audits: false,
  },
  loading: {
    audits: false,
  },
  error: {
    audits: false,
  }
};

export default (state = initialState, action: types.IActionAudits) => {
  switch (action.type) {
    case types.ATypes.DESTROY: {
      return {
        ...state,
        entityActionMap: null,
        audit: null,
        success: {
          audits: false,
        },
        loading: {
          audits: false,
        },
        error: {
          audits: false,
        },
      };
    }
    case types.ATypes.REQ_AUDITS: {
      return {
        ...state,
        ...reqState(state, 'audits'),
      };
    }
    case types.ATypes.ERR_AUDITS: {
      return {
        ...state,
        ...errState(state, 'audits')
      };
    }
    case types.ATypes.RES_AUDITS: {
      return {
        ...state,
        audit: action.payload,
        ...resState(state, 'audits'),
      };
    }
    case types.ATypes.REQ_ENTITY_ACTION_MAP: {
      return {
        ...state,
      };
    }
    case types.ATypes.ERR_ENTITY_ACTION_MAP: {
      return {
        ...state,
      };
    }
    case types.ATypes.RES_ENTITY_ACTION_MAP: {
      return {
        ...state,
        entityActionMap: action.payload,
      };
    }
    default:
      return state;
  }
};
