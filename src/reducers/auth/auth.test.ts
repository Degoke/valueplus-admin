import * as types from 'src/actions/auth/types';
import authReducer, { initialState } from './index';
import { authenticateUser, checkUserSessionSuccess } from 'src/actions/auth/actions';

describe('reducers -> Auth', () => {
  let defaultState = initialState;
  beforeEach(() => {
    defaultState = initialState;
  });

  it('REQ_LOGIN', () => {
    const userMock = { email: 'itunu', password: 'lol' };
    const action = authenticateUser.request(userMock) as any;
    const derivedState: types.State = {
      ...defaultState,
      hasErrored: false,
      isLoading: true,
    };
    expect(authReducer(defaultState, action)).toEqual(derivedState);
  });

  it('RES_LOGIN', () => {
    const action = authenticateUser.success(true) as any;
    const derivedState: types.State = {
      ...defaultState,
      isLoading: false,
      isAuthenticated: true,
    };
    expect(authReducer(defaultState, action)).toEqual(derivedState);
  });

  it('ERR_LOGIN', () => {
    const action = authenticateUser.failure('error') as any;
    const derivedState: types.State = {
      ...defaultState,
      isLoading: false,
      hasErrored: true,
    };
    expect(authReducer(defaultState, action)).toEqual(derivedState);
  });

  it('Validates user auth session', () => {
    const action = checkUserSessionSuccess(true);
    const derivedState: types.State = {
      ...defaultState,
      isAuthenticated: action.payload,
    };
    expect(authReducer(defaultState, action)).toEqual(derivedState);
  });
});
