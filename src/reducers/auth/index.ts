import * as types from 'src/actions/auth/types';

export const initialState: types.State = {
  isLoading: false,
  hasErrored: false,
  registrationSuccessful: null,
  passwordResetSuccessful: false,
  isAuthenticated: false,
};
export default (state = initialState, action: types.IActionAuth) => {
  switch (action.type) {
    case types.ATypes.DESTROY: {
      return {
        ...state,
        isLoading: false,
        hasErrored: false,
        registrationSuccessful: null,
        passwordResetSuccessful: false,
      };
    }
    case types.ATypes.REQ_NEW_PASSWORD:
    case types.ATypes.REQ_PASSWORD_RESET:
    case types.ATypes.REQ_SIGNUP:
    case types.ATypes.REQ_LOGIN: {
      return {
        ...state,
        hasErrored: false,
        isLoading: true,
        registrationSuccessful: null,
      };
    }
    case types.ATypes.ERR_NEW_PASSWORD:
    case types.ATypes.ERR_PASSWORD_RESET:
    case types.ATypes.ERR_SIGNUP:
    case types.ATypes.ERR_LOGIN: {
      return {
        ...state,
        isLoading: false,
        hasErrored: true,
      };
    }
    case types.ATypes.RES_LOGIN: {
      return {
        ...state,
        isLoading: false,
        hasErrored: false,
        isAuthenticated: action.payload,
      };
    }
    case types.ATypes.RES_SIGNUP: {
      return {
        ...state,
        isLoading: false,
        hasErrored: false,
        registrationSuccessful: action.payload,
      };
    }
    case types.ATypes.RES_NEW_PASSWORD:
    case types.ATypes.RES_PASSWORD_RESET: {
      return {
        ...state,
        isLoading: false,
        hasErrored: false,
        passwordResetSuccessful: action.payload,
      };
    }
    case types.ATypes.CHECK_USER_SESSION_SUCCESS: {
      return {
        ...state,
        isAuthenticated: action.payload,
      };
    }
    default:
      return state;
  }
};
