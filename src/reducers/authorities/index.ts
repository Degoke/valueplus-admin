import * as types from 'src/actions/authorities/types';

export const initialState: types.State = {
  privileges: [],
};

export default (state = initialState, action: types.IActionSummary) => {
  switch (action.type) {
    case types.ATypes.REQ_AUTHORITIES: {
      return {
        ...state,
      };
    }
    case types.ATypes.ERR_AUTHORITIES: {
      return {
        ...state,
      };
    }
    case types.ATypes.RES_AUTHORITIES: {
      return {
        ...state,
        privileges: action.payload,
      };
    }

    default:
      return state;
  }
};
