import { History } from 'history';
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import auth from './auth';
import users from './users';
import profile from './profile';
import transactions from './transactions';
import * as Auth from 'src/actions/auth/types';
import * as Users from 'src/actions/users/types';
import * as Profile from 'src/actions/profile/types';
import * as Transaction from 'src/actions/transactions/types';
import products from './products';
import * as Product from 'src/actions/products/types';
import wallet from './wallet';
import audits from './audits';
import summary from './summary';
import settings from './settings';
import superAgents from './superAgents';
import authorities from './authorities';
import * as Audit from 'src/actions/audit/types';
import * as Wallet from 'src/actions/wallet/types';
import * as Summary from 'src/actions/summary/types';
import * as Settings from 'src/actions/settings/types';
import * as Authorities from 'src/actions/authorities/types';
import * as SuperAgents from 'src/actions/superAgents/types';

export type RootState = {
  auth: Auth.State,
  users: Users.State,
  audits: Audit.State,
  wallet: Wallet.State,
  summary: Summary.State,
  profile: Profile.State,
  products: Product.State,
  settings: Settings.State;
  authorities: Authorities.State,
  transactions: Transaction.State,
  superAgents: SuperAgents.State
};

const rootReducer = (history: History<unknown>) =>
  combineReducers({
    auth,
    users,
    wallet,
    audits,
    summary,
    profile,
    settings,
    products,
    superAgents,
    authorities,
    transactions,
    router: connectRouter(history),
  });

export default rootReducer;
