import * as types from 'src/actions/products/types';
import { reqState, errState, resState } from 'src/utils';

export const initialState: types.State = {
  success: {
    product: false,
    products: false,
    userOrders: false,
    productOrders: false,
  },
  loading: {
    product: false,
    products: false,
    userOrders: false,
    productOrders: false,
  },
  error: {
    product: false,
    products: false,
    userOrders: false,
    productOrders: false,
  },
  product: null,
  products: null,
  productOrders: null,
  userOrders: null,
};

export default (state = initialState, action: types.IActionProducts) => {
  switch (action.type) {
    case types.ATypes.REQ_GET_PRODUCT:
    case types.ATypes.REQ_DISABLE_PRODUCT_STATUS:
    case types.ATypes.REQ_ENABLE_PRODUCT_STATUS:
    case types.ATypes.REQ_CREATE_PRODUCT:
    case types.ATypes.REQ_UPDATE_PRODUCT: {
      return {
        ...state,
        ...reqState(state, 'product'),
      };
    }

    case types.ATypes.REQ_GET_PRODUCTS: {
      return {
        ...state,
        product: null,
        ...reqState(state, 'products'),
      };
    }

    case types.ATypes.REQ_UPDATE_PRODUCT_ORDER_STATUS:
    case types.ATypes.REQ_GET_PRODUCT_ORDERS:
    case types.ATypes.REQ_GET_FILTERED_PRODUCT_ORDERS: {
      return {
        ...state,
        ...reqState(state, 'productOrders'),
      };
    }

    case types.ATypes.RES_GET_PRODUCT_ORDERS: {
      return {
        ...state,
        ...resState(state, 'productOrders'),
        productOrders: action.payload,
      };
    }

    case types.ATypes.RES_GET_FILTERED_PRODUCT_ORDERS: {
      return {
        ...state,
        ...resState(state, 'productOrders'),
        productOrders: action.payload,
      };
    }

    case types.ATypes.RES_UPDATE_PRODUCT_ORDER_STATUS: {
      const productOrdersCopy =
        state.productOrders === null ? [] : [...state.productOrders.content];
      const userOrdersCopy =
        state.userOrders === null ? [] : [...state.userOrders.content];

      const isProductOrdersEmpty = productOrdersCopy.length === 0;
      const isUserOrdersEmpty = userOrdersCopy.length === 0;

      const ordersToFilter = isProductOrdersEmpty
        ? userOrdersCopy
        : productOrdersCopy;

      const index = ordersToFilter.findIndex(
        (index) => index.id === action.payload?.id,
      );

      ordersToFilter.splice(index, 1, action.payload);

      const initialOrders = state.productOrders ?? state.userOrders

      const updatedProductOrders = Object.assign({}, initialOrders, {
        content: ordersToFilter,
      });

      return {
        ...state,
        ...resState(state, 'productOrders'),
        productOrders: isProductOrdersEmpty ? null : updatedProductOrders,
        userOrders: isUserOrdersEmpty ? null : updatedProductOrders,
      };
    }

    case types.ATypes.RES_GET_PRODUCTS: {
      return {
        ...state,
        ...resState(state, 'products'),
        products: action.payload,
      };
    }
    case types.ATypes.RES_CREATE_PRODUCT: {
      return {
        ...state,
        ...resState(state, 'product'),
      };
    }

    case types.ATypes.RES_GET_PRODUCT: {
      return {
        ...state,
        ...resState(state, 'product'),
        product: action.payload,
        success: {
          ...state.success,
          product: false,
        },
      };
    }

    case types.ATypes.RES_UPDATE_PRODUCT: {
      return {
        ...state,
        ...resState(state, 'product'),
        product: action.payload,
      };
    }

    case types.ATypes.ERR_GET_PRODUCT:
    case types.ATypes.ERR_UPDATE_PRODUCT:
    case types.ATypes.ERR_DISABLE_PRODUCT_STATUS:
    case types.ATypes.ERR_ENABLE_PRODUCT_STATUS:
    case types.ATypes.ERR_CREATE_PRODUCT: {
      return {
        ...state,
        ...errState(state, 'product'),
      };
    }

    case types.ATypes.ERR_GET_PRODUCTS: {
      return {
        ...state,
        ...errState(state, 'products'),
      };
    }
    case types.ATypes.ERR_UPDATE_PRODUCT_ORDER_STATUS:
    case types.ATypes.ERR_GET_PRODUCT_ORDERS:
    case types.ATypes.ERR_GET_FILTERED_PRODUCT_ORDERS: {
      return {
        ...state,
        ...errState(state, 'productOrders'),
      };
    }
    case types.ATypes.REQ_USER_ORDERS: {
      return {
        ...state,
        ...reqState(state, 'userOrders'),
      };
    }

    case types.ATypes.RES_USER_ORDERS: {
      return {
        ...state,
        ...resState(state, 'userOrders'),
        userOrders: action.payload,
      };
    }

    case types.ATypes.ERR_USER_ORDERS: {
      return {
        ...state,
        ...errState(state, 'userOrders'),
      };
    }

    case types.ATypes.DESTROY: {
      return {
        ...state,
        productOrders: null,
        userOrders: null,
        success: {
          product: false,
          products: false,
          productOrders: false,
        },
        loading: {
          product: false,
          products: false,
          productOrders: false,
        },
        error: {
          product: false,
          products: false,
          productOrders: false,
        },
        product: null,
        products: {
          content: [],
        },
      };
    }
    default:
      return state;
  }
};
