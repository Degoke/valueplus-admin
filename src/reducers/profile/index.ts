import * as types from 'src/actions/profile/types';
import * as Auth from 'src/actions/auth/types';
import { reqState, errState, resState } from 'src/utils';

export const initialState: types.State = {
  success: {
    user: false,
    profileUpdate: false,
    profilePicture: false,
    passwordUpdate: false,
  },
  loading: {
    user: false,
    profileUpdate: false,
    profilePicture: false,
    passwordUpdate: false,
  },
  error: {
    user: false,
    profileUpdate: false,
    profilePicture: false,
    passwordUpdate: false,
  },
  user: null,
};

//TODO Destroy should be able to clear all state in the store... maybe move user to AuthState so it will be easier to destroy profile on umount and won't affect user... then Auth can be destroyed safely onLogout
export default (state = initialState, action: types.IActionProfile) => {
  switch (action.type) {
    case Auth.ATypes.AUTH_LOGOUT: {
      return {
        ...state,
        user: null,
      }
    }
    case types.ATypes.DESTROY: {
      return {
        ...state,
        loading: {
          user: false,
          profileUpdate: false,
          profilePicture: false,
          passwordUpdate: false,
        },
        error: {
          user: false,
          profileUpdate: false,
          profilePicture: false,
          passwordUpdate: false,
        },
        success: {
          user: false,
          profileUpdate: false,
          profilePicture: false,
          passwordUpdate: false,
        },
      };
    }
    case types.ATypes.REQ_USER: {
      return {
        ...state,
        ...reqState(state, 'user'),
      };
    }
    case types.ATypes.ERR_USER: {
      return {
        ...state,
        ...errState(state, 'user'),
      };
    }
    case types.ATypes.RES_USER: {
      return {
        ...state,
        ...resState(state, 'user'),
        user: action.payload,
      };
    }
    case types.ATypes.REQ_PROFILE_UPDATE: {
      return {
        ...state,
        ...reqState(state, 'profileUpdate'),
      };
    }
    case types.ATypes.ERR_PROFILE_UPDATE: {
      return {
        ...state,
        ...errState(state, 'profileUpdate')
      };
    }
    case types.ATypes.RES_PROFILE_UPDATE: {
      const { photo, ...updateUser } = action.payload as any
      return {
        ...state,
        ...resState(state, 'profileUpdate'),
        user: Object.assign({}, state.user, updateUser),
      };
    }
    case types.ATypes.REQ_PASSWORD_UPDATE: {
      return {
        ...state,
        ...reqState(state, 'passwordUpdate'),
      };
    }
    case types.ATypes.ERR_PASSWORD_UPDATE: {
      return {
        ...state,
        ...errState(state, 'passwordUpdate')
      };
    }
    case types.ATypes.RES_PASSWORD_UPDATE: {
      const { photo, ...updateUser } = action.payload as any
      return {
        ...state,
        ...resState(state, 'passwordUpdate'),
        user: Object.assign({}, state.user, updateUser),
      };
    }
    case types.ATypes.REQ_PROFILE_IMAGE: {
      return {
        ...state,
        ...reqState(state, 'profilePicture'),
      };
    }
    case types.ATypes.ERR_PROFILE_IMAGE: {
      return {
        ...state,
        ...errState(state, 'profilePicture')
      };
    }
    case types.ATypes.RES_PROFILE_IMAGE: {
      return {
        ...state,
        ...resState(state, 'profilePicture'),
        user: Object.assign({}, state.user, action.payload),
      };
    }
    default:
      return state;
  }
};
