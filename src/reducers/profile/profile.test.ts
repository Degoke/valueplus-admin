import * as types from 'src/actions/profile/types';
import authReducer, { initialState } from './index';
import { getUser } from 'src/actions/profile/actions';
import { ROLE_TYPE } from 'src/utils';

const data = {
  id: 1,
  address: 'null',
  agentCode: 'null',
  authorities: ['blaa'],
  email: 'hkdvjhfj@gail.com',
  emailVerified: true,
  firstname: 'Dapo',
  lastname: 'Olowoeyo',
  link: 'null',
  phone: '860669427',
  photo: 'null',
  referralCode: 'jndjfhkjdhgkjsdf',
  roleType: ROLE_TYPE.SUPER_AGENT,
  superAgentCode: 'null',
  transactionTokenSet: false,
}
describe('reducers -> Profile', () => {
  let defaultState = initialState;
  beforeEach(() => {
    defaultState = initialState;
  });

  it('REQ_USER', () => {
    const action = getUser.request() as any;
    const derivedState: types.State = {
      ...defaultState,
      error: {
        user: false,
        profileUpdate: false,
        profilePicture: false,
        passwordUpdate: false,
      },
      loading: {
        user: true,
        profileUpdate: false,
        profilePicture: false,
        passwordUpdate: false,
      },
    };
    expect(authReducer(defaultState, action)).toEqual(derivedState);
  });

  it('RES_USER', () => {
    const action = getUser.success(data) as any;
    const derivedState: types.State = {
      ...defaultState,
      user: data,
      error: {
        user: false,
        profileUpdate: false,
        profilePicture: false,
        passwordUpdate: false,
      },
      loading: {
        user: false,
        profileUpdate: false,
        profilePicture: false,
        passwordUpdate: false,
      },
    };
    expect(authReducer(defaultState, action)).toEqual(derivedState);
  });

  it('ERR_USER', () => {
    const action = getUser.failure('error') as any;
    const derivedState: types.State = {
      ...defaultState,
      error: {
        user: true,
        profileUpdate: false,
        profilePicture: false,
        passwordUpdate: false,
      },
      loading: {
        user: false,
        profileUpdate: false,
        profilePicture: false,
        passwordUpdate: false,
      },
    };
    expect(authReducer(defaultState, action)).toEqual(derivedState);
  });
});
