import * as types from 'src/actions/settings/types';
import { errState, resState, reqState } from 'src/utils';

export const initialState: types.State = {
  commissionLogs: null,
  scheduledCommissions: null,
  success: {
    commission: false,
    schedule: false,
    commissionLogs: false,
  },
  loading: {
    commission: false,
    schedule: false,
    commissionLogs: false,
  },
  error: {
    commission: false,
    schedule: false,
    commissionLogs: false,
  },
};

export default (state = initialState, action: types.IActionSettings) => {
  switch (action.type) {
    case types.ATypes.DESTROY: {
      return {
        ...state,
        success: {
          commission: false,
        },
        loading: {
          commission: false,
        },
        error: {
          commission: false,
        },
      };
    }
    case types.ATypes.REQ_GET_COMMISSION_LOGS: {
      return {
        ...state,
        ...reqState(state, 'commissionLogs'),
      };
    }
    case types.ATypes.ERR_GET_COMMISSION_LOGS: {
      return {
        ...state,
        ...errState(state, 'commissionLogs'),
      };
    }
    case types.ATypes.RES_GET_COMMISSION_LOGS: {
      return {
        ...state,
        commissionLogs: action.payload,
        ...resState(state, 'commissionLogs'),
      };
    }
    case types.ATypes.REQ_GET_SCHEDULE_COMMISSION: {
      return {
        ...state,
        ...reqState(state, 'schedule'),
      };
    }
    case types.ATypes.ERR_GET_SCHEDULE_COMMISSION: {
      return {
        ...state,
        ...errState(state, 'schedule'),
      };
    }
    case types.ATypes.RES_GET_SCHEDULE_COMMISSION: {
      return {
        ...state,
        scheduledCommissions: action.payload,
        ...resState(state, 'schedule'),
      };
    }
    case types.ATypes.RES_UPDATE_COMMISSION: {
      return {
        ...state,
        ...resState(state, 'commission'),
      };
    }
    case types.ATypes.ERR_UPDATE_COMMISSION: {
      return {
        ...state,
        ...errState(state, 'commission'),
      };
    }

    default:
      return state;
  }
};
