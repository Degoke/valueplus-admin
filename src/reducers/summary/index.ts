import * as types from 'src/actions/summary/types';

export const initialState: types.State = {
  summary: null,
};

export default (state = initialState, action: types.IActionSummary) => {
  switch (action.type) {
    case types.ATypes.REQ_SUMMARY: {
      return {
        ...state,
      };
    }
    case types.ATypes.ERR_SUMMARY: {
      return {
        ...state,
      };
    }
    case types.ATypes.RES_SUMMARY: {
      return {
        ...state,
        summary: action.payload,
      };
    }

    default:
      return state;
  }
};
