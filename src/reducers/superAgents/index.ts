import * as types from 'src/actions/superAgents/types';
import { reqState, errState, resState } from 'src/utils';

export const initialState: types.State = {
  loading: {
    superAgent: false,
    superAgents: false,
    superAgentActiveUsers: false,
  },
  error: {
    superAgent: false,
    superAgents: false,
    superAgentActiveUsers: false,
  },
  success: {
    superAgent: false,
    superAgents: false,
    superAgentActiveUsers: false,
  },
  superAgent: null,
  superAgents: null,
  superAgentActiveUsers: null
};

export default (state = initialState, action: types.IActionSuperAgents) => {
  switch (action.type) {
    case types.ATypes.DESTROY: {
      return {
        ...state,
        loading: {
          superAgent: false,
          superAgents: false,
          superAgentActiveUsers: false,
        },
        error: {
          superAgent: false,
          superAgents: false,
          superAgentActiveUsers: false,
        },
        success: {
          superAgent: false,
          superAgents: false,
          superAgentActiveUsers: false,
        },
      };
    }
    case types.ATypes.REQ_CREATE_SUPER_AGENT: {
      return {
        ...state,
        ...reqState(state, 'superAgent'),
      };
    }
    case types.ATypes.RES_CREATE_SUPER_AGENT: {
      return {
        ...state,
        ...resState(state, 'superAgent'),
        superAgent: action.payload
      };
    }
    case types.ATypes.ERR_CREATE_SUPER_AGENT: {
      return {
        ...state,
        ...errState(state, 'superAgent'),
      };
    }
    case types.ATypes.REQ_GET_SUPER_AGENTS: {
      return {
        ...state,
        ...reqState(state, 'superAgents'),
      };
    }
    case types.ATypes.RES_GET_SUPER_AGENTS: {
      return {
        ...state,
        ...resState(state, 'superAgents'),
        superAgents: action.payload
      };
    }
    case types.ATypes.ERR_GET_SUPER_AGENTS: {
      return {
        ...state,
        ...errState(state, 'superAgents'),
      };
    }
    case types.ATypes.REQ_GET_SUPER_AGENT_ACTIVE_USERS: {
      return {
        ...state,
        ...reqState(state, 'superAgentActiveUsers'),
      };
    }
    case types.ATypes.RES_GET_SUPER_AGENT_ACTIVE_USERS: {
      return {
        ...state,
        ...resState(state, 'superAgentActiveUsers'),
        superAgentActiveUsers: action.payload
      };
    }
    case types.ATypes.ERR_GET_SUPER_AGENT_ACTIVE_USERS: {
      return {
        ...state,
        ...errState(state, 'superAgentActiveUsers'),
      };
    }

    default:
      return state;
  }
};
