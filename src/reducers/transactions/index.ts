import * as types from 'src/actions/transactions/types';
import * as Auth from 'src/actions/auth/types';
import { reqState, errState, resState } from 'src/utils';

export const initialState: types.State = {
  loading: {
    transactions: false,
  },
  error: {
    transactions: false,
  },
  success: {
    transactions: false,
  },
  banks: null,
  transactions: null,
  transaction: null,
};

export default (state = initialState, action: types.IActionTransaction) => {
  switch (action.type) {
    case Auth.ATypes.AUTH_LOGOUT: {
      return {
        ...state,
        banks: null,
        transactions: null,
        transaction: null,
      }
    }
    case types.ATypes.DESTROY: {
      return {
        ...state,
        loading: {
          transactions: false,
        },
        error: {
          transactions: false,
        },
        success: {
          transactions: false,
        },
      };
    }
    case types.ATypes.REQ_REFRESH_TRANSACTIONS: {
      return {
        ...state,
        ...reqState(state, 'refresh'),
      };
    }
    case types.ATypes.ERR_REFRESH_TRANSACTIONS: {
      return {
        ...state,
        ...errState(state, 'refresh'),
      };
    }
    case types.ATypes.RES_TRANSACTIONS_FILTER:
    case types.ATypes.RES_TRANSACTIONS: {
      return {
        ...state,
        transactions: action.payload,
        ...resState(state, 'transactions'),
      };
    }
    case types.ATypes.REQ_TRANSACTIONS:
    case types.ATypes.REQ_TRANSACTIONS_FILTER: {
      return {
        ...state,
        ...reqState(state, 'transactions'),
      };
    }
    case types.ATypes.ERR_TRANSACTIONS:
    case types.ATypes.ERR_TRANSACTIONS_FILTER: {
      return {
        ...state,
        ...errState(state, 'transactions')
      };
    }
    default:
      return state;
  }
};
