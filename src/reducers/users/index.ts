import * as types from 'src/actions/users/types';
import * as Auth from 'src/actions/auth/types';
import { reqState, errState, resState } from 'src/utils';

export const initialState: types.State = {
  loading: {
    users: false,
  },
  error: {
    users: false,
  },
  success: {
    users: false,
  },
  users: null,
};

export default (state = initialState, action: types.IActionUsers) => {
  switch (action.type) {
    case Auth.ATypes.AUTH_LOGOUT: {
      return {
        ...state,
        users: null,
      }
    }
    case types.ATypes.DESTROY: {
      return {
        ...state,
        loading: {
          users: false,
        },
        error: {
          users: false,
        },
        success: {
          users: false,
        },
      };
    }
    case types.ATypes.REQ_GET_FILTERED_USERS:
    case types.ATypes.REQ_USERS: {
      return {
        ...state,
        ...reqState(state, 'users'),
      };
    }
    case types.ATypes.ERR_GET_FILTERED_USERS:
    case types.ATypes.ERR_USERS: {
      return {
        ...state,
        ...errState(state, 'users'),
      };
    }
    case types.ATypes.RES_GET_FILTERED_USERS:
    case types.ATypes.RES_USERS: {
      return {
        ...state,
        users: action.payload,
        ...resState(state, 'users'),
      };
    }

    default:
      return state;
  }
};
