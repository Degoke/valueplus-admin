import * as types from 'src/actions/wallet/types';
import { reqState, errState, resState } from 'src/utils';

export const initialState: types.State = {
  loading: {
    wallet: false,
    walletHistory: false,
  },
  error: {
    wallet: false,
    walletHistory: false,
  },
  success: {
    wallet: false,
    walletHistory: false,
  },
  wallet: null,
  walletHistory: null,
};

export default (state = initialState, action: types.IActionWallet) => {
  switch (action.type) {
    case types.ATypes.REQ_ALL_WALLET: {
      return {
        ...state,
        ...reqState(state, 'wallet'),
      };
    }
    case types.ATypes.ERR_ALL_WALLET: {
      return {
        ...state,
        ...errState(state, 'wallet'),
      };
    }
    case types.ATypes.RES_ALL_WALLET: {
      return {
        ...state,
        wallet: action.payload,
        ...resState(state, 'wallet'),
      };
    }
    case types.ATypes.REQ_WALLET_HISTORY_FILTER: {
      return {
        ...state,
        ...reqState(state, 'walletHistory'),
      };
    }
    case types.ATypes.ERR_WALLET_HISTORY_FILTER: {
      return {
        ...state,
        ...errState(state, 'walletHistory'),
      };
    }
    case types.ATypes.RES_WALLET_HISTORY_FILTER: {
      return {
        ...state,
        walletHistory: action.payload,
        ...resState(state, 'walletHistory'),
      };
    }
    default:
      return state;
  }
};
