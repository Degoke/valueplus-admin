export enum Routes {
  EMPTY = '',
  HOME = '/',
  LOGIN = '/login',
  LOGOUT = '/logout',
  DASHBOARD = '/dashboard',
  FORGOT_PASSWORD = '/forgot-password',
  RESET_PASSWORD = '/reset-password/:slug',

  // DASHBOARD ROUTES
  PROFILE = '/profile',
  MANAGE_USERS = '/manage-users',
  WALLETS = '/wallets',
  COMMISSIONS = '/commissions',
  ACTIVITY_LOG = '/activity-log',
  SUPERAGENTS = '/super-agents',
  TRANSACTIONS = '/transactions',
  CREATE_ADMIN = '/create-admin',
  MANAGE_ORDERS = '/manage-orders',
  MANAGE_PRODUCTS = '/manage-products',
  MANAGE_PRODUCTS_CREATE = '/manage-products/create',
}
