import { applyMiddleware, compose, createStore } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import rootEpic from 'src/actions/root-epic';
import Actions from 'src/actions/root-action';
import rootReducer, { RootState } from 'src/reducers';
import { ActionType } from 'typesafe-actions';
import { Epic } from 'redux-observable';
import { createBrowserHistory } from 'history';

export const history = createBrowserHistory();
export type RootEpic = Epic<RootAction, RootAction, RootState>;
export type RootAction = ActionType<typeof Actions>;
const epicMiddleware = createEpicMiddleware<RootAction, RootAction, RootState>(
  {},
);

const middlewares = [epicMiddleware];
const initialState = {};
const composeEnhancers =
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const enhancers = composeEnhancers(applyMiddleware(...middlewares));
const store = createStore(rootReducer(history), initialState, enhancers);

epicMiddleware.run(rootEpic);

export default store;
