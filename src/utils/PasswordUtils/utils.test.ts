import {
  validateLength,
  validateCase,
  validateDigit,
  hasLowerCase,
  hasUpperCase,
  hasSpecChar,
} from './utils';

describe('validateLength', () => {
  it('should be greater or equal 8 symb length', () => {
    expect(validateLength('wrong')).toBe(false);
    expect(validateLength('legitlength')).toBe(true);
  });
});
describe('validateCase', () => {
  const testDataList: [string, boolean][] = [
    ['wrong', false],
    ['UPPER', false],
    ['SemiCase', true],
  ];
  testDataList.forEach((testData) => {
    it(`'${testData[0]}' should be ${testData[1] ? 'valid' : 'invalid'}`, () => {
      expect(validateCase(testData[0])).toBe(testData[1]);
    });
  });
});
describe('validateDigit', () => {
  it('should contain digit', () => {
    expect(validateDigit('weq')).toBe(false);
    expect(validateDigit('we2sd')).toBe(true);
  });
});
describe('hasLowerCase', () => {
  it('should return if string has lower case symbols', () => {
    expect(hasLowerCase('aW')).toBe(true);
    expect(hasLowerCase('NO')).toBe(false);
    expect(hasLowerCase('')).toBe(false);
  });
});
describe('hasUpperCase', () => {
  it('should return if string has upper case symbols', () => {
    expect(hasUpperCase('aa')).toBe(false);
    expect(hasUpperCase('YES')).toBe(true);
    expect(hasUpperCase('')).toBe(false);
  });
});
describe('hasSpecChar', () => {
  it('should return if string has special character symbols', () => {
    expect(hasSpecChar('asd')).toBe(false);
    expect(hasSpecChar('as@')).toBe(true);
    expect(hasSpecChar('')).toBe(false);
  });
});
