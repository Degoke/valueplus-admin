import { PasswordRule } from 'src/components/PasswordRules/PasswordRules';

export const validateLength = (value: string, l = 8): boolean => value.length >= l;
export const hasLowerCase = (value: string): boolean => /[a-z]/.test(value);
export const hasUpperCase = (value: string): boolean => /[A-Z]/.test(value);
export const validateCase = (value: string): boolean => hasLowerCase(value) && hasUpperCase(value);
export const validateDigit = (value: string): boolean => /\d/.test(value);
export const hasSpecChar = (value: string): boolean => /[!@#$%&*]/.test(value);

export const ruleList: PasswordRule[] = [{
  message: '8 or more characters',
  validation: validateLength,
}, {
  message: 'Upper & lowercase letters',
  validation: validateCase,
}, {
  message: 'At least one digit',
  validation: validateDigit,
}];

export default {
  validateLength,
  hasLowerCase,
  hasUpperCase,
  validateCase,
  validateDigit,
  hasSpecChar,
};
