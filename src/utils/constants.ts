export enum Direction {
  ASC = 'ASC',
  DESC = 'DESC',
}
export default {
  emailRegExp: /^[_a-zA-Z0-9]+((\.[_a-zA-Z0-9]+)|(-[_a-zA-Z0-9]+))*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,6})$/,
  phoneRegExp: /^\+?\d+$/g,
  mobileBreakpoint: 768,
  smallDesktopBreakpoint: 1280,
  tabletBreakPoint: 1024,
  token: 'user_token',
  shortDelay: 100,
  defaultDelay: 500,
  longDelay: 1000,
  TRANSACTION_PAGE_SIZE: 20,
  PRODUCTS_PAGE_SIZE: 8,
  PRODUCTS_ORDERS_SIZE: 20,
  ESC_KEY_CODE: 'Escape',
  ANIMATION_TIME: 0.3,
  PAYSTACK_TEST_SECRET: 'sk_test_ea191037ac7aaac26e69009775b2677de88afeda',
  PAYSTACK_TEST_PUBLIC: 'pk_test_d0cba7966d70b1cfae1fef8ad082ca8e738a7d58',
};
