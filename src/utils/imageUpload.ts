import { message } from 'antd';
export const getBase64 = (
  img: File | Blob,
  callback: (result: string | ArrayBuffer) => void,
) => {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
};

export const beforeUpload = (file: File) => {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 1;
  if (!isLt2M) {
    message.error(`Image must smaller than ${1}MB!`);
  }

  return isJpgOrPng && isLt2M;
};
