import { Status, TransactionStatus } from 'src/utils';
import { PresetStatusColorType } from 'antd/lib/_util/colors';

export const StatusColor = new Map<Status, PresetStatusColorType>([
  [Status.PENDING, 'warning'],
  [Status.COMPLETED, 'success'],
  [Status.FAILED, 'error'],
  [Status.IN_PROGRESS, 'processing'],
  [Status.CANCELLED, 'default'],
]);
// pending - is when transaction is initiated and the user did not activate OTP on his account
// otp - is when transaction is waiting OTP confirmation from the admin
// failed - when transaction actually failed same with error
// success - when transaction is successful
// abandoned - when transaction has stayed too long in the otp state

export const TransactionStatusColor = new Map<TransactionStatus, PresetStatusColorType>([
  [TransactionStatus.ABANDONED, 'default'],
  [TransactionStatus.OTP, 'warning'],
  [TransactionStatus.SUCCESS, 'success'],
  [TransactionStatus.ERROR, 'error'],
  [TransactionStatus.PENDING, 'default'],
  [TransactionStatus.FAILED, 'error'],
]);
