import toInitials from './toInitials';

describe('toInitials', () => {
  it('converts name to initials', () => {
    expect(toInitials('Nikita')).toMatchInlineSnapshot('"NI"');
    expect(toInitials('Nikita Lolovichius')).toMatchInlineSnapshot('"NL"');
    expect(toInitials('123 Lol')).toMatchInlineSnapshot('"1L"');
    expect(toInitials('L o L')).toMatchInlineSnapshot('"LO"');
  });
});
