
// update or merge array
export const unionBy = (a: any[], b: any[], fn: (arg0: any) => unknown) => {
  const s = new Set(b.map(fn));
  return Array.from(new Set([...a.filter(x => !s.has(fn(x))), ...b]));
};
