export enum Status {
  FAILED = 'FAILED',
  PENDING = 'PENDING',
  COMPLETED = 'COMPLETED',
  CANCELLED = 'CANCELLED',
  IN_PROGRESS = 'IN_PROGRESS'
}

export enum TransactionStatus {
  ERROR = 'error',
  FAILED = 'failed',
  OTP = 'otp',
  SUCCESS = 'success',
  PENDING = 'pending',
  ABANDONED = 'abandoned'
}
export type PaginationPageOptions = {
  size?: number,
  page?: number,
  sort?: boolean,
}

export type Sort = {
  empty?: boolean,
  sorted?: boolean,
  unsorted?: boolean
}
export type Pagination = {
  empty?: boolean,
  first?: boolean,
  last?: boolean,
  number?: number,
  numberOfElements?: number,
  size?: number,
  sort?: Sort,
  totalElements?: number,
  totalPages?: number
}

export type DateFilter = {
  startDate: string,
  endDate: string,
};

export interface LoadingState {
  isLoading: boolean;
  isSuccess: boolean;
  isError: boolean;
}

export enum ROLE_TYPE {
  ADMIN = 'ADMIN',
  AGENT = 'AGENT',
  SUPER_AGENT = 'SUPER_AGENT',
}
