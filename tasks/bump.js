'use strict';

const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const semver = require('semver');
const logger = require('log4js').getDefaultLogger();

const version = _.last(process.argv);

// validate version
if (!semver.valid(version)) {
  logger.error(`Version ${version} is not valid!`);
  process.exit(1);
}

const pkgPath = path.resolve(process.cwd(), 'package.json');
const pkg = JSON.parse(fs.readFileSync(pkgPath).toString());

// update version
pkg.version = version;

fs.writeFileSync(pkgPath, Buffer.from(JSON.stringify(pkg, null, 2)));
logger.info('Version was updated!');
