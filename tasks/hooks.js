'use strict';

const fs = require('fs');
const del = require('del');
const path = require('path');
const logger = require('log4js').getLogger();

const hooksPath = path.resolve(process.cwd(), 'hooks');
const gitHooksPath = path.resolve(process.cwd(), '.git/hooks');

del.sync([gitHooksPath]);
fs.symlinkSync(hooksPath, gitHooksPath);
logger.info('Git hooks linked!');
