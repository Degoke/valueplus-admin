const dispatch = jest.fn();
const subscribe = jest.fn();
const getState = jest.fn();

const getStore = () => {
  return {
    dispatch,
    subscribe,
    getState,
  };
};

export default getStore;
