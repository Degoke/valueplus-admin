const argv = process.env.npm_config_argv;
if (argv) {
  // if its passed via npm run script
  module.exports = JSON.parse(argv).original;
} else {
  // if its a direct run
  module.exports = process.argv;
}
