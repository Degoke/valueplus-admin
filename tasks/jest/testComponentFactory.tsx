import { mount, ReactWrapper, shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';

const testComponentFactory = <P extends {}, S extends {} = {},
  C extends React.Component = React.Component<P, S, any>> (
  /** component to mount/shallowing */
  TestComponent: React.ElementType,
  /** prop getter used in component mounting/shallowing */
  getProps: () => P,
) => {
  type DefaultProps = { defaultProps: P; };
  type ShallowReturn = ShallowWrapper<P, S, C> & DefaultProps;
  type MountReturn = ReactWrapper<P, S, C> & DefaultProps;

  function creator(overrideProps?: Partial<P>, type?: 'shallow'): ShallowReturn;
  function creator(overrideProps: Partial<P>, type: 'mount'): MountReturn;
  function creator(overrideProps: Partial<P>, type: 'mount' | 'shallow' = 'shallow')
  : ShallowReturn | MountReturn {
    const props = getProps();
    const component = type === 'shallow'
      ? shallow<C, P, S>(<TestComponent {...props} {...overrideProps || {}} />)
      : mount<C, P, S>(<TestComponent {...props} {...overrideProps || {}} />);

    /**
     * assign defaultProps for accessing props
     * directly instead of wrapper.instance()
     * @example
     * const wrapper = component();
     * expect(wrapper.defaultProps.onClick).toHaveBeenCalled();
     */
    return Object.assign(component, { defaultProps: { ...props, ...overrideProps } });
  }

  return creator;
};

export default testComponentFactory;
