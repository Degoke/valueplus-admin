const _ = require('lodash');
const fs = require('fs');
const git = require('git-rev-sync');
const path = require('path');
const pkg = require('../package.json');
const log = require('log4js').getLogger();

const commit = git.long();
const versionPath = path.resolve(process.cwd(), 'build', 'version.html');
const commitPath = path.resolve(process.cwd(), 'build', 'commit.txt');

fs.writeFile(versionPath, Buffer.from(pkg.version.toString()), _.noop);
fs.writeFile(commitPath, Buffer.from(commit), _.noop);

log.info(`${versionPath} was created`);
log.info(`${commitPath} was created`);
