const log4js = require('log4js');
const fs = require('fs');
const path = require('path');
const nrgv = require('./lib/nrgv');
const pkg = require('../package.json');

const logger = log4js.getLogger();
const version = nrgv.pop();

if (version) {
  pkg.version = version;
  console.log(pkg.version);
  var content = Buffer.from(JSON.stringify(pkg, null, 2));
  fs.writeFile(path.resolve(process.cwd(), 'package.json'), content, function onResult(err) {
    if (err) {
      logger.fatal(err);
    } else {
      logger.info(`Package version was bumped to ${version}!`);
    }
  });
} else {
  logger.fatal('Version was not found! Release branch must follow rules like release/0.0.1');
  process.exit(1);
}
