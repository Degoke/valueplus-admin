const _ = require('lodash');
const path = require('path');
const webpack = require('webpack');
const pkg = require('./package.json');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const SpeedMeasurePlugin = require('speed-measure-webpack-plugin');
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');

const smp = new SpeedMeasurePlugin({
  disable: !process.env.MEASURE,
});
const env = () => process.env.NODE_ENV || 'development';
const isProd = () => env() === 'production';

function fileName(prefix, suffix, dot) {
  suffix = _.isUndefined(suffix) ? '[ext]' : suffix;
  dot = _.isUndefined(dot) ? true : dot;

  const ext = `${dot ? '.' : ''}${suffix}`;
  return `${prefix}__[hash:8].${pkg.version}${ext}`;
}

const buildEnv = {
  NODE_ENV: process.env.NODE_ENV,
  VERSION: pkg.version,
};
// style files regexes
const sassRegex = /\.global\.(scss|sass)$/;
const sassModuleRegex = /\.module\.(scss|sass)$/;

const PATHS = {
  src: path.join(__dirname, 'src'),
  build: path.join(__dirname, 'build'),
};

module.exports = smp.wrap({
  mode: env(),
  entry: path.resolve(pkg.main),
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[name]-[hash].js',
    publicPath: '/',
  },
  stats: 'errors-only',
  watchOptions: {
    aggregateTimeout: 500,
    ignored: ['node_modules'],
  },
  devServer: {
    historyApiFallback: true,
    stats: 'errors-only',
    port: 5000,
    contentBase: PATHS.build,
    disableHostCheck: true,
    progress: true,
    open: true,
  },
  resolve: {
    alias: {
      src: path.join(__dirname, '..', 'src'),
    },
    modules: ['node_modules', path.resolve(__dirname, 'src')],
    extensions: ['.js', '.json', '.jsx', '.ts', '.tsx', '.css', '.scss'],
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
    },
    minimizer: [
      new TerserPlugin({
        cache: true,
        parallel: true,
      }),
      new OptimizeCSSAssetsPlugin({
        cssProcessorPluginOptions: {
          preset: ['default', { calc: false }],
        },
      }),
    ],
  },
  module: {
    rules: [
      {
        test: /\.(tsx|ts)?$/,
        loaders: ['babel-loader', 'awesome-typescript-loader'],
        exclude: /node_modules/,
      },

      {
        test: /\.(mp3|png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)(\?.*)?$/,
        loader: 'file-loader',
      },

      {
        test: /\.css$/,
        exclude: [/\.module\.css$/],
        use: [
          isProd() ? MiniCssExtractPlugin.loader : 'style-loader',
          'cache-loader',
          { loader: 'css-loader', options: { importLoaders: 1 } },
        ],
      },

      {
        test: sassRegex,
        exclude: [sassModuleRegex],
        use: [
          { loader: isProd() ? MiniCssExtractPlugin.loader : 'style-loader' },
          { loader: 'css-loader', options: { modules: true } },
          { loader: 'sass-loader' },
        ],
      },

      {
        test: sassModuleRegex,
        use: [
          { loader: isProd() ? MiniCssExtractPlugin.loader : 'style-loader' },
          { loader: 'css-modules-typescript-loader' },
          { loader: 'css-loader', options: { modules: true } },
          { loader: 'sass-loader' },
        ],
      },
    ],
  },
  plugins: [
    new LodashModuleReplacementPlugin({
      collections: true,
      paths: true,
    }),
    new HtmlWebpackPlugin({ template: 'public/index.html', inject: 'body' }),
    new webpack.DefinePlugin({
      'process.env': _.mapValues(buildEnv, JSON.stringify),
    }),
    new MiniCssExtractPlugin({ filename: fileName('styles', 'css') }),
    new CopyWebpackPlugin({
      patterns: [{ from: 'public' }],
    }),
  ],
});
